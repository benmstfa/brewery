$(document).ready(function () {
    RandomBeer();
});


//get random beer on load
function RandomBeer() {
    $.ajax({
        method: "post",
        url: "/Task/ajax/request.php",
        data: {
            get_random: "get_random"
        },
        success: function (data) {
            data = JSON.parse(data);
            // console.log(data);
            if (data["data"]["description"] == undefined || data["data"]["labels"] == undefined) {
                RandomBeer();
            } else {
                document.getElementById("beer_name").innerText = data["data"].name;
                document.getElementById("beer_description").innerText = data["data"].description;
                document.getElementById("beer_image").setAttribute("src", data["data"]["labels"].medium);
                document.getElementById("brewery_more").value = data["data"]["breweries"][0].id;
                document.getElementById("random_beer").value = data["data"].id;
            }
        }
    });
}

// get random beer on click
document.getElementById("btn1_random_beer").onclick = function () {
    RandomBeer();
    clearResults();
};

// get more beers from brewery on button click
document.getElementById("btn2_more_beers").onclick = function () {
    MoreBeers();
};


function MoreBeers() {
    clearResults();
    var BreweryId = document.getElementById("brewery_more").value;
    // console.log("getting beers");
    $.ajax({
        method: "post",
        url: "/Task/ajax/request.php",
        data: {
            get_more_beers: "true",
            BreweryId: BreweryId
        },
        success: function (data) {
            data = JSON.parse(data);
            // console.log(data);
            var randomBeerId = document.getElementById("random_beer").value
            if (data["data"] == undefined) {
                $("#results").append(
                    "<div class='alert-info'>No Data Found</div>"
                );
            } else {
                for (var i = 0; i < data["data"].length; i++) {
                    if (data["data"][i]["labels"] == undefined || data["data"][i]["description"] == undefined || data["data"][i].id == randomBeerId) {
                        continue;
                    } else {
                        // console.log(i);
                        $("#results").append("" +
                            "<div class='row result'>" +
                            "<div class='col-lg-3 info'>" +
                            "<h6>" + data["data"][i].name + "</h6>" +
                            "<img src='" + data["data"][i]["labels"]["medium"] + "' alt=''>" +
                            "</div>" +
                            "<div class='col-lg-9 description'>" + data["data"][i].description + "</div>" +
                            "</div>" +
                            "<hr>");
                    }
                }
            }
        }
    });
}

// search stuff
// call search function on button click
$('#search_button').on('click', function () {
    Search();
});

function Search() {
    clearResults();
    var choice = null;
    var isBeer = document.getElementById('beer').checked;
    var isBrewery = document.getElementById('brewery').checked;
    var input_value = $('#search_input').val();

    if (!isBeer && !isBrewery || input_value == "") {
        if (input_value == "") {
            return alert("please enter value: ")
        } else {
            return alert("please choose search Option: \n. beer\n. brewery")
        }
    }

    if (isBeer) {
        choice = "beer";
    } else if (isBrewery) {
        choice = "brewery";
    }

    $.ajax({
        method: "post",
        url: '/Task/ajax/request.php',
        data: {
            search: "search",
            input_value: input_value,
            choice: choice
        },
        success: function (data) {
            data = JSON.parse(data);
            // console.log(data);
            if (data["data"] == undefined || (data["data"].length == 1 && (data["data"].description == undefined || data["data"].labels == undefined || data["data"].images == undefined))) {
                $("#results").append(
                    "<div class='alert-info'>No Valid Data Found</div>"
                );
            } else {
                if (choice == "beer") {
                    for (var i = 0; i < data["data"].length; i++) {
                        if (data["data"][i]["labels"] == undefined || data["data"][i]["description"] == undefined) {
                            continue;
                        } else {
                            // console.log(i);
                            $("#results").append("" +
                                "<div class='row result'>" +
                                "<div class='col-lg-3'>" +
                                "<h6>" + data["data"][i].name + "</h6>" +
                                "<div class='image'>" +
                                "<img src='" + data["data"][i]["labels"]["medium"] + "' alt=''>" +
                                "</div>" +
                                "</div>" +
                                "<div class='col-lg-9'>" + data["data"][i].description + "</div>" +
                                "</div>" +
                                "<hr>");
                        }
                    }
                }
                else if (choice == "brewery") {
                    for (var i = 0; i < data["data"].length; i++) {
                        if (data["data"][i]["images"] == undefined || data["data"][i]["description"] == undefined) {
                            continue;
                        } else {
                            $("#results").append("" +
                                "<div class='row result'>" +
                                "<div class='col-lg-3'>" +
                                "<h6>" + data["data"][i].name + "</h6>" +
                                "<img src='" + data["data"][i]["images"]["medium"] + "' alt=''>" +
                                "</div>" +
                                "<div class='col-lg-9'>" + data["data"][i].description + "</div>" +
                                "</div>" +
                                "<hr>");
                        }
                    }
                }
            }
        }
    });
}

// to clear results from results div to bring new results
function clearResults() {
    document.getElementById("results").innerHTML = null;
}

// clear unwanted letters
$("#search_input").keyup(function () {
    var input = $(this);
    var text = input.val().replace(/[^a-zA-Z0-9-_\s,]/g, "");
    input.val(text);
});