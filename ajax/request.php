<?php
ini_set("allow_url_fopen", 1);

// get random beer on load
if (isset($_POST['get_random'])) {
    $url = "http://api.brewerydb.com/v2/beer/random?key=your_key&hasLabels=Y&withBreweries=Y";
    $json = file_get_contents($url);
    print_r($json);
}

// get more beers from brewery owns the beers in random section
if (isset($_POST["get_more_beers"])) {
    $BreweryId = $_POST["BreweryId"];
    $url = sprintf("http://api.brewerydb.com/v2/brewery/%s/beers?key=your_key&hasLabels=Y",
        $BreweryId);
    $json = file_get_contents($url);
    print_r($json);
}

// search post request
if (isset($_POST["search"])) {
    $search_text = $_POST["input_value"];
    $choice = $_POST["choice"];

    //validating search input and get data if valid
    if (!preg_match('/^[a-zA-Z0-9 _,]*$/', $search_text)) {
        echo 0;
    } else {
        $search_text = trim($search_text);
        $search_text = str_replace(" ", "%20", $search_text);
        $url = sprintf("http://api.brewerydb.com/v2/search?q=%s&type=%s&key=your_key",
            $search_text, $choice);
        $json = file_get_contents($url);
        print_r($json);
    }
}

?>
