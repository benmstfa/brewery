function MoreBeersTry() {
    var json = {
        "message": "Request Successful",
        "data": [
            {
                "id": "Hv5qVm",
                "name": "Agave Peach Wheat",
                "nameDisplay": "Agave Peach Wheat",
                "abv": "5",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-21 18:25:51",
                "updateDate": "2015-12-21 18:25:52",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "2PnMVa",
                "name": "Ale La Reverend",
                "nameDisplay": "Ale La Reverend",
                "description": "A distinctively dry, light bodied IPA with an appealing golden hue, thirst-quenching crispness, and laden with pungent hop aromas of citrus and spruce. Semi-sweet malt provides a backdrop for some mild earthy flavors and slight grassy notes. Doubling the dry-hop additions allows for lower alcohol levels, while still providing the entire hop effect expected of an IPA.",
                "abv": "4",
                "ibu": "66",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/2PnMVa/upload_bor5qd-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/2PnMVa/upload_bor5qd-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/2PnMVa/upload_bor5qd-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:39",
                "updateDate": "2015-12-16 13:26:45",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "nWoBcn",
                "name": "Alien Einstein",
                "nameDisplay": "Alien Einstein",
                "description": "Alien Einstein is a light bodied India Pale Lager with a dull golden color and an inviting aroma of tangy grapefruit juice. A bright burst of fruity and earthy hop flavors immediately hits the palate. This beer exemplifies the well-rounded versatility of the American Mosaic hops used exclusively  in this beer.",
                "abv": "5.5",
                "ibu": "75",
                "availableId": 2,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/nWoBcn/upload_NIQ4QD-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/nWoBcn/upload_NIQ4QD-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/nWoBcn/upload_NIQ4QD-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:28:08",
                "updateDate": "2015-12-17 19:25:09",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "TQ6Qdz",
                "name": "Anni Ale 13irteen",
                "nameDisplay": "Anni Ale 13irteen",
                "description": "Anni Ale 13irteen is a dry hopped American Sour Ale with black currants. Deep ruby red in color, this American Sour Ale has aromas of tart fruit and citrus. An initial sour flavor (that’s guaranteed to satisfy any Sour beer fan) is followed by notes of dark berry. Flavors of tart black currant coat the palate before a finish that is very dry with just a touch of citrus and a slight bitterness.",
                "abv": "8.7",
                "ibu": "30",
                "styleId": 40,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/TQ6Qdz/upload_iZ2ZHf-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/TQ6Qdz/upload_iZ2ZHf-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/TQ6Qdz/upload_iZ2ZHf-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-05-16 13:35:32",
                "updateDate": "2017-05-16 13:36:09",
                "style": {
                    "id": 40,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Sour Ale",
                    "shortName": "Sour",
                    "description": "American sour ales can be very light to black or take on the color of added fruits or other ingredients. There is no Brettanomyces character in this style of beer. Wood- and barrel-aged sour ales are classified elsewhere. If acidity is present it is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash or in fermentation by the use of various microorganisms including certain bacteria and yeasts. Acidic character can be a complex balance of several types of acid and characteristics of age. The evolution of natural acidity develops balanced complexity. Residual flavors that come from liquids previously aged in a barrel such as bourbon or sherry should not be present. Wood vessels may be used during the fermentation and aging process, but wood-derived flavors such as vanillin must not be present. In darker versions, roasted malt, caramel-like and chocolate-like characters should be subtle in both flavor and aroma. American sour may have evident full range of hop aroma and hop bitterness with a full range of body. Estery and fruity-ester characters are evident, sometimes moderate and sometimes intense, yet balanced. Diacetyl and sweet corn-like dimethylsulfide (DMS) should not be perceived. Chill haze, bacteria and yeast-induced haze are allowable at low to medium levels at any temperature. Fruited American-Style Sour Ales will exhibit fruit flavors in harmonious balance with other characters.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:32"
                }
            },
            {
                "id": "ejWAf1",
                "name": "Anniversary Ale",
                "nameDisplay": "Anniversary Ale",
                "description": "A high gravity wheat wine made with blood oranges that render an alluring dark purple hue to the color. Flavor abounds from the spice of green peppercorns and tang from blood orange zest. Aggressive dry hop additions amplify the citrus overtones and spiciness found throughout. ‘Anni’ Ale typifies the full magnitude of Short’s Brewing Company by embodying all the complexities and fulfillment necessary to pay tribute to another year of brewing.",
                "abv": "9.3",
                "ibu": "63",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 35,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/ejWAf1/upload_yRiNud-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/ejWAf1/upload_yRiNud-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/ejWAf1/upload_yRiNud-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:42",
                "updateDate": "2016-06-20 18:18:02",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 35,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Wheat Wine Ale",
                    "shortName": "Wheatwine",
                    "description": "American style wheat wines range from gold to deep amber and are brewed with 50% or more wheat malt. They have full body and high residual malty sweetness. Perception of bitterness is moderate to medium -high. Fruity-ester characters are often high and counterbalanced by complexity of alcohols and high alcohol content. Hop aroma and flavor are at low to medium levels. Very low levels of diacetyl may be acceptable. Bready, wheat, honey-like and/or caramel aroma and flavor are often part of the character. Phenolic yeast character, sulfur, and/or sweet corn-like dimethylsulfide (DMS) should not be present. Oxidized, stale and aged characters are not typical of this style. Chill haze is allowable.",
                    "ibuMin": "45",
                    "ibuMax": "85",
                    "abvMin": "8.4",
                    "abvMax": "12",
                    "srmMin": "8",
                    "srmMax": "15",
                    "ogMin": "1.088",
                    "fgMin": "1.024",
                    "fgMax": "1.032",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:17"
                }
            },
            {
                "id": "QY7xBR",
                "name": "Anniversary Ale 2005",
                "nameDisplay": "Anniversary Ale 2005",
                "abv": "9.5",
                "styleId": 31,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-04-30 00:37:52",
                "updateDate": "2018-05-25 19:25:56",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "EaFVV7",
                "name": "Ants on a Lager",
                "nameDisplay": "Ants on a Lager",
                "description": "Ants on a Lager is a Vienna style Lager brewed with peanut butter, raisins, and hand-juiced celery. Copper-colored with a frothy white head, Ants on a Lager has aromas of peanut butter, caramel, and light biscuit. Flavors of peanut butter and slightly sweet raisin are followed by a hint of celery. Ant son a Lager has a silky mouth feel, reminiscent of peanut butter. This experimental brew is truly one of a kind.",
                "abv": "5.9",
                "ibu": "15",
                "styleId": 80,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/EaFVV7/upload_lpC7RW-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/EaFVV7/upload_lpC7RW-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/EaFVV7/upload_lpC7RW-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-20 14:20:20",
                "updateDate": "2017-09-08 16:38:18",
                "style": {
                    "id": 80,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Vienna-Style Lager",
                    "shortName": "Vienna Lager",
                    "description": "Beers in this category are reddish brown or copper colored. They are medium in body. The beer is characterized by malty aroma and slight malt sweetness. The malt aroma and flavor should have a notable degree of toasted and/or slightly roasted malt character. Hop bitterness is clean and crisp. Noble-type hop aromas and flavors should be low or mild. Diacetyl, chill haze and ale-like fruity esters should not be perceived.",
                    "ibuMin": "22",
                    "ibuMax": "28",
                    "abvMin": "4.8",
                    "abvMax": "5.4",
                    "srmMin": "12",
                    "srmMax": "16",
                    "ogMin": "1.046",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:36:42"
                }
            },
            {
                "id": "9bdGbX",
                "name": "Aorta Ale",
                "nameDisplay": "Aorta Ale",
                "description": "Aorta Ale is a Double Red Ale with bold flavors of Cascade hops and sweet malt, and a dark reddish-brown color. Subtle aromas of candy, brown sugar, and toasted malt are released from the depths of this full-bodied beast. Raisins, figs, and burnt caramel are among the sweeter and more forward flavors found in Aorta Ale. This brew finishes with a roasted cocoa like bitterness which is magnified by the high alpha hops.",
                "abv": "9.6",
                "ibu": "82",
                "availableId": 2,
                "styleId": 163,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/9bdGbX/upload_XSUZ2A-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/9bdGbX/upload_XSUZ2A-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/9bdGbX/upload_XSUZ2A-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2013-01-05 19:26:50",
                "updateDate": "2017-08-30 21:48:08",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 163,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Double Red Ale",
                    "shortName": "Double Red",
                    "description": "Double Red Ales are deep amber to dark copper/reddish brown. A small amount of chill haze is allowable at cold temperatures. Fruity-ester aroma is medium. Hop aroma is high, arising from any variety of hops. Medium to medium-high caramel malt character is present. Low to medium biscuit or toasted characters may also be present. Hop flavor is high and balanced with other beer characters. Hop bitterness is high to very high. Alcohol content is medium to high. Complex alcohol flavors may be evident. Fruity-ester flavors are medium. Diacetyl should not be perceived. Body is medium to full.",
                    "createDate": "2015-04-07 17:06:23"
                }
            },
            {
                "id": "78DrbK",
                "name": "Aphasia",
                "nameDisplay": "Aphasia",
                "description": "Short's Old Ale is a medium-bodied, dark brown ale. Predominately sweet, the unique flavors of plum, figs, and brown sugar are evident.",
                "abv": "9",
                "ibu": "28",
                "styleId": 13,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/78DrbK/upload_0DMj2d-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/78DrbK/upload_0DMj2d-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/78DrbK/upload_0DMj2d-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-31 20:02:43",
                "updateDate": "2015-12-17 20:02:51",
                "style": {
                    "id": 13,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Old Ale",
                    "shortName": "Old Ale",
                    "description": "Dark amber to brown in color, old ales are medium to full bodied with a malty sweetness. Hop aroma should be minimal and flavor can vary from none to medium in character intensity. Fruity-ester flavors and aromas can contribute to the character of this ale. Bitterness should be minimal but evident and balanced with malt and/or caramel-like sweetness. Alcohol types can be varied and complex. A distinctive quality of these ales is that they undergo an aging process (often for years) on their yeast either in bulk storage or through conditioning in the bottle, which contributes to a rich, wine-like and often sweet oxidation character. Complex estery characters may also emerge. Some very low diacetyl character may be evident and acceptable. Wood aged characters such as vanillin and other woody characters are acceptable. Horsey, goaty, leathery and phenolic character evolved from Brettanomyces organisms and acidity may be present but should be at low levels and balanced with other flavors Residual flavors that come from liquids previously aged in a barrel such as bourbon or sherry should not be present. Chill haze is acceptable at low temperatures. (This style may often be split into two categories, strong and very strong. Brettanomyces organisms and acidic characters reflect historical character. Competition organizers may choose to distinguish these types of old ale from modern versions.)",
                    "ibuMin": "30",
                    "ibuMax": "65",
                    "abvMin": "6",
                    "abvMax": "9",
                    "srmMin": "12",
                    "srmMax": "30",
                    "ogMin": "1.058",
                    "fgMin": "1.014",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:21:03"
                }
            },
            {
                "id": "igzD4a",
                "name": "Autumn Ale",
                "nameDisplay": "Autumn Ale",
                "description": "Autumn Ale is a true-to-style London Extra Special Bitter (ESB). It has a medium body, amber color, and full flavor. This beer exhibits a wonderful balance of malty sweetness and floral hop bitterness. The result is an ideal bridge between malty and hoppy beer styles. Autumn Ale is a silver medal winner from the 2006 Great American Beer Festival.",
                "abv": "6.5",
                "ibu": "31",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 5,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/igzD4a/upload_DOtK1L-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/igzD4a/upload_DOtK1L-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/igzD4a/upload_DOtK1L-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:43",
                "updateDate": "2015-12-16 12:19:16",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 5,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Extra Special Bitter",
                    "shortName": "ESB",
                    "description": "Extra special bitter possesses medium to strong hop aroma, flavor, and bitterness. The residual malt and defining sweetness of this richly flavored, full-bodied bitter is more pronounced than in other styles of bitter. It is light amber to copper colored with medium to medium-high bitterness. Mild carbonation traditionally characterizes draft-cask versions, but in bottled versions, a slight increase in carbon dioxide content is acceptable. Fruity-ester character is acceptable in aroma and flavor. Diacetyl (butterscotch character) is acceptable and characteristic when at very low levels. The absence of diacetyl is also acceptable. Chill haze is allowable at cold temperatures. English or American hops may be used. (English and American hop character may be specified in subcategories.)",
                    "ibuMin": "30",
                    "ibuMax": "45",
                    "abvMin": "4.8",
                    "abvMax": "5.8",
                    "srmMin": "8",
                    "srmMax": "14",
                    "ogMin": "1.046",
                    "fgMin": "1.01",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:19:20"
                }
            },
            {
                "id": "8lYY6l",
                "name": "Aww Jeah",
                "nameDisplay": "Aww Jeah",
                "description": "Aww Jeah is a Double American India Pale Ale with a dark orange hue and big hop aromas. Sweet malt collides with an array of hop flavors ranging from sticky fruit to pine in this brew. Aww Jeah shifts towards a finish that is sharp and bitter.",
                "abv": "10.4",
                "ibu": "95",
                "styleId": 31,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-10 16:22:49",
                "updateDate": "2017-10-10 16:22:49",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "aW0VED",
                "name": "Basic B",
                "nameDisplay": "Basic B",
                "abv": "6.9",
                "styleId": 121,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 18:24:52",
                "updateDate": "2015-12-18 18:24:52",
                "style": {
                    "id": 121,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Pumpkin Beer",
                    "shortName": "Pumpkin Beer",
                    "description": "Pumpkin beers are any beers using pumpkins (Cucurbito pepo) as an adjunct in either mash, kettle, primary or secondary fermentation, providing obvious (ranging from subtle to intense), yet harmonious, qualities. Pumpkin qualities should not be overpowered by hop character. These may or may not be spiced or flavored with other things. A statement by the brewer explaining the nature of the beer is essential for fair assessment in competitions. If this beer is a classic style with pumpkin, the brewer should also specify the classic style.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:28"
                }
            },
            {
                "id": "P5UOLb",
                "name": "Batch 5000",
                "nameDisplay": "Batch 5000",
                "description": "Batch 5000 is a big bodied Triple American India Pale Ale with a sizable palate coating sweetness. Delicious flavors and aromas of pineapple and orange are complimented by an awesome piney hop bitterness that leads into a warming finish.",
                "abv": "14.6",
                "ibu": "75",
                "styleId": 31,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-05 15:19:19",
                "updateDate": "2017-09-05 15:19:19",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "tMxmxt",
                "name": "Beach Wheat",
                "nameDisplay": "Beach Wheat",
                "description": "A light, clean, easy drinking Hefeweizen. This beer is categorized by a grain bill of 50% malted wheat and a German weizen yeast strain that is directly responsible for the distinctly complex flavors. Immediately evident in the nose, this brew is punctuated by esters of banana and clove. These aromatics reflect similar flavors detected during consumption.",
                "abv": "4.5",
                "ibu": "9",
                "glasswareId": 9,
                "availableId": 2,
                "styleId": 48,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:46",
                "updateDate": "2014-07-01 11:47:17",
                "glass": {
                    "id": 9,
                    "name": "Weizen",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 48,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "South German-Style Hefeweizen / Hefeweissbier",
                    "shortName": "Hefeweizen",
                    "description": "The aroma and flavor of a Weissbier with yeast is decidedly fruity and phenolic. The phenolic characteristics are often described as clove-, nutmeg-like, mildly smoke-like or even vanilla-like. Banana-like esters should be present at low to medium-high levels. These beers are made with at least 50 percent malted wheat, and hop rates are quite low. Hop flavor and aroma are absent or present at very low levels. Weissbier is well attenuated and very highly carbonated and a medium to full bodied beer. The color is very pale to pale amber. Because yeast is present, the beer will have yeast flavor and a characteristically fuller mouthfeel and may be appropriately very cloudy. No diacetyl should be perceived.",
                    "ibuMin": "10",
                    "ibuMax": "15",
                    "abvMin": "4.9",
                    "abvMax": "5.5",
                    "srmMin": "3",
                    "srmMax": "9",
                    "ogMin": "1.047",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:29:27"
                }
            },
            {
                "id": "oURm3F",
                "name": "Beard of Zeus",
                "nameDisplay": "Beard of Zeus",
                "description": "A super hoppy American pale lager. We used the final runnings from the Elk Rapids batch of Peaches and Cream and hopped it heavily with Zeus hops and then lagered it. We also used small amount of bitter orange peel to provide some subtle sweet flavors and enhance the aromas of the Zeus hops",
                "abv": "4.5",
                "ibu": "70",
                "styleId": 93,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/oURm3F/upload_JpLQ5u-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/oURm3F/upload_JpLQ5u-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/oURm3F/upload_JpLQ5u-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-06 05:00:26",
                "updateDate": "2015-12-17 18:57:06",
                "style": {
                    "id": 93,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Lager",
                    "shortName": "American Lager",
                    "description": "Light in body and very light to straw in color, American lagers are very clean and crisp and aggressively carbonated. Flavor components should b e subtle and complex, with no one ingredient dominating the others. Malt sweetness is light to mild. Corn, rice, or other grain or sugar adjuncts are often used. Hop bitterness, flavor and aroma are negligible to very light. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "13",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:26"
                }
            },
            {
                "id": "L0i1E1",
                "name": "Bellaire Brown",
                "nameDisplay": "Bellaire Brown",
                "description": "Bellaire Brown is a full-flavored brown ale that derives sweet caramel and toasted qualities from four different kinds of malt and specialty grains. Select hop varieties create an enticing aroma and dry finish. Bellaire Brown is very dark and rich, which makes it hard to classify as a brown, but it is certainly considered a delicious masterpiece.   Bellaire Brown was originally created to appeal to people who enjoyed drinking coffee, and to beer drinkers who are not interested in hoppy options. When Joe Short was creating the Bellaire Brown recipe, he desired a beer that was meatier and burlier than most browns. Bellaire Brown is a great gate-way beer for drinkers to be introduced to darker beer varieties.",
                "abv": "7",
                "ibu": "14",
                "glasswareId": 5,
                "availableId": 1,
                "styleId": 37,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/L0i1E1/upload_VsAq72-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/L0i1E1/upload_VsAq72-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/L0i1E1/upload_VsAq72-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:47",
                "updateDate": "2015-12-16 12:21:13",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                },
                "style": {
                    "id": 37,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Brown Ale",
                    "shortName": "American Brown",
                    "description": "American brown ales range from deep copper to brown in color. Roasted malt caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma. American brown ales have evident low to medium hop flavor and aroma, medium to high hop bitterness, and a medium body. Estery and fruity-ester characters should be subdued. Diacetyl should not be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "25",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "6.4",
                    "srmMin": "15",
                    "srmMax": "26",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:35"
                }
            },
            {
                "id": "YmeqSb",
                "name": "Bim Bam Boom",
                "nameDisplay": "Bim Bam Boom",
                "abv": "7.4",
                "styleId": 42,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-02-22 16:16:54",
                "updateDate": "2016-02-22 16:16:54",
                "style": {
                    "id": 42,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Stout",
                    "shortName": "American Stout",
                    "description": "Initial low to medium malt sweetness with a degree of caramel, chocolate and/or roasted coffee flavor with a distinctive dryroasted bitterness in the finish. Coffee-like roasted barley and roasted malt aromas are prominent. Some slight roasted malt acidity is permissible and a medium- to full-bodied mouthfeel is appropriate. Hop bitterness may be moderate to high. Hop aroma and flavor is moderate to high, often with American citrus-type and/or resiny hop character. The perception of fruity esters is low. Roasted malt/barley astringency may be low but not excessive. Diacetyl (butterscotch) should be negligible or not perceived. Head retention is excellent.",
                    "ibuMin": "35",
                    "ibuMax": "60",
                    "abvMin": "5.7",
                    "abvMax": "8.8",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.05",
                    "fgMin": "1.01",
                    "fgMax": "1.022",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:43"
                }
            },
            {
                "id": "nI1Ehr",
                "name": "Black Chai",
                "nameDisplay": "Black Chai",
                "description": "Black Chai is a sumptuous black cream ale that immediately entices the senses with aromas of dark specialty malts, cardamon, nutmeg, and ginger. The brew has a delicious creamy mouthfeel. Chocolate and roasted malt characteristics resonate throughout this black ale. The finish is playfully spicy and bitter.",
                "abv": "5",
                "ibu": "50",
                "styleId": 124,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/nI1Ehr/upload_niCdrI-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/nI1Ehr/upload_niCdrI-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/nI1Ehr/upload_niCdrI-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 21:43:16",
                "updateDate": "2015-12-17 19:21:16",
                "style": {
                    "id": 124,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Herb and Spice Beer",
                    "shortName": "Spice Beer",
                    "description": "Herb beers use herbs or spices (derived from roots, seeds, fruits, vegetable, flowers, etc.) other than or in addition to hops to create a distinct (ranging from subtle to intense) character, though individual characters of herbs and/or spices used may not always be identifiable. Under hopping often, but not always, allows the spice or herb to contribute to the flavor profile. Positive evaluations are significantly based on perceived balance of flavors. Note: Chili-flavored beers that emphasize heat rather than chili flavor should be entered as a \"spiced\" beer.  A statement by the brewer explaining what herbs or spices are used is essential in order for fair assessment in competitions. Specifying a style upon which the beer is based may help evaluation. If this beer is a classic style with an herb or spice, the brewer should specify the classic style. If no Chocolate or Coffee category exists in a competition, then chocolate and coffee beers should be entered in this category.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:45"
                }
            },
            {
                "id": "H3EyoS",
                "name": "Black Cherry Porter",
                "nameDisplay": "Black Cherry Porter",
                "description": "This Short’s brew is available seasonally, as soon as the local Northern Michigan sweet black cherries are ripe. Eight different malts, in conjunction with three varieties of hops, provide the deep radiant flavor profile and create the dark opaque color. A slight purple lace from the fruit puree enticingly leads into smooth soft hints of roasted chocolate and pleasurable black cherry flavors. This is quite possibly the most anticipated beer we offer.",
                "abv": "7",
                "ibu": "40",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 18,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/H3EyoS/upload_vsfKt5-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/H3EyoS/upload_vsfKt5-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/H3EyoS/upload_vsfKt5-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:50",
                "updateDate": "2016-11-29 14:17:16",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 18,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Brown Porter",
                    "shortName": "Brown Porter",
                    "description": "Brown porters are mid to dark brown (may have red tint) in color. No roast barley or strong burnt/black malt character should be perceived. Low to medium malt sweetness, caramel and chocolate is acceptable along with medium hop bitterness. This is a lightto medium-bodied beer. Fruity esters are acceptable. Hop flavor and aroma may vary from being negligible to medium in character.",
                    "ibuMin": "20",
                    "ibuMax": "30",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "20",
                    "srmMax": "35",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:21:43"
                }
            },
            {
                "id": "smvhUM",
                "name": "Black Diamond",
                "nameDisplay": "Black Diamond",
                "glasswareId": 5,
                "styleId": 84,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/smvhUM/upload_7BAolg-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/smvhUM/upload_7BAolg-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/smvhUM/upload_7BAolg-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-12-04 12:59:20",
                "updateDate": "2015-12-16 22:17:13",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "style": {
                    "id": 84,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Schwarzbier",
                    "shortName": "Schwarzbier",
                    "description": "These very dark brown to black beers have a mild roasted malt character without the associated bitterness. This is not a fullbodied beer, but rather a moderate body gently enhances malt flavor and aroma with low to moderate levels of sweetness. Hop bitterness is low to medium in character. Noble-type hop flavor and aroma should be low but perceptible. There should be no fruity esters. Diacetyl should not be perceived.",
                    "ibuMin": "22",
                    "ibuMax": "30",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "25",
                    "srmMax": "30",
                    "ogMin": "1.044",
                    "fgMin": "1.01",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:37:12"
                }
            },
            {
                "id": "Vggb7K",
                "name": "Black Licorice Lager",
                "nameDisplay": "Black Licorice Lager",
                "description": "Chocolate and roasted malt characteristics compliment the additions of Madagascar vanilla bean, anise, and fresh chocolate mint. Vanilla aromas greet the nose and are tasted in the initial flavors, followed quickly by the anise. The finish, especially when the beer warms a bit, show cases the crisp, sweet chocolate mint. \r\n\r\nThis well balanced beer is a bronze medal winner at the 2010 WBC.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 103,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:50",
                "updateDate": "2017-01-10 21:38:54",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 103,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Dark Lager",
                    "shortName": "American Dark Lager",
                    "description": "This beer's malt aroma and flavor are low but notable. Its color ranges from a very deep copper to a deep, dark brown. It has a clean, light body with discreet contributions from caramel and roasted malts. Non-malt adjuncts are often used, and hop rates are low. Hop bitterness is clean and has a short duration of impact. Hop flavor, and aroma are low. Carbonation is high. Fruity esters, diacetyl, and chill haze should not be perceived.",
                    "ibuMin": "14",
                    "ibuMax": "20",
                    "abvMin": "4",
                    "abvMax": "5.5",
                    "srmMin": "14",
                    "srmMax": "25",
                    "ogMin": "1.04",
                    "fgMin": "1.008",
                    "fgMax": "1.012",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:41:42"
                }
            },
            {
                "id": "80PPGl",
                "name": "Bloody Beer",
                "nameDisplay": "Bloody Beer",
                "description": "A lighter bodied beer with an appealing ruby red glow and aromas of spicy tomato juice. Fermented with Roma tomatoes and spiced with dill, horseradish, peppercorns, and celery seed lead to an astounding initial tomato flavor, followed by a lingering finish that allows each additional ingredient a chance to resonate on the palate. Decant carefully, and let this 2009 GABF silver medal winner warm slightly in order to appreciate the full magnitude of flavors.",
                "glasswareId": 5,
                "availableId": 2,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:52",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                }
            },
            {
                "id": "kK8DQC",
                "name": "Bludgeon Yer Eye",
                "nameDisplay": "Bludgeon Yer Eye",
                "description": "This India Black Ale is a shining example of stylistic guidelines. Intense fruity and floral aromas abound with a subtle malty fragrance reluctantly piercing through. A deep black color attunes the pallet for rich, dark, roasted malt flavors. The unexpected twist comes when high hop bitterness takes over and resonates profoundly.",
                "abv": "6.2",
                "ibu": "75",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 41,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/kK8DQC/upload_hPLbsQ-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/kK8DQC/upload_hPLbsQ-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/kK8DQC/upload_hPLbsQ-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:52",
                "updateDate": "2015-12-16 13:23:24",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 41,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Black Ale",
                    "shortName": "Black Ale",
                    "description": "American-style Black Ales are very dark to black and perceived to have medium high to high hop bitterness, flavor and aroma with medium-high alcohol content, balanced with a medium body. Fruity, floral and herbal character from hops of all origins may contribute character. The style is further characterized by a balanced and moderate degree of caramel malt and dark roasted malt flavor and aroma. High astringency and high degree of burnt roast malt character should be absent.",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6",
                    "abvMax": "7.5",
                    "srmMin": "35",
                    "srmMax": "35",
                    "ogMin": "1.056",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:36"
                }
            },
            {
                "id": "aAK9I8",
                "name": "Bonafide Legit",
                "nameDisplay": "Bonafide Legit",
                "description": "A light bodied American India Pale Ale brewed entirely with Northern Michigan grown Summit hops. An excellent sharp aroma of strong floral and fruity characteristics stings the senses with hop laced anticipation. A sticky hop filled mouthful of damp earthy pine with notable herbal accents coats the entire mouth, followed by a big resiny bitterness that lingers well into the finish, drying the palate and resonating boldly. Show Less",
                "abv": "6",
                "ibu": "85",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/aAK9I8/upload_toodeQ-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/aAK9I8/upload_toodeQ-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/aAK9I8/upload_toodeQ-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-10 16:49:35",
                "updateDate": "2015-12-17 20:27:18",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "Mdr1Zd",
                "name": "Bourbon Barrel Sustenance",
                "nameDisplay": "Bourbon Barrel Sustenance",
                "description": "Every year we hold back a batch of the Sustenance and age it in “wet” bourbon barrels. We release it the following year with the new batch of Sustenance for each Anniversary party. Available each year at the pub only, sold individually by the bottle.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 132,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:42:53",
                "updateDate": "2017-01-10 21:38:54",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 132,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Wood- and Barrel-Aged Beer",
                    "shortName": "BBL Aged",
                    "description": "A wood- or barrel-aged beer is any lager, ale or hybrid beer, either a traditional style or a unique experimental beer that has been aged for a period of time in a wooden barrel or in contact with wood. This beer is aged with the intention of imparting the particularly unique character of the wood and/or what has previously been in the barrel. New wood character can be characterized as a complex blend of vanillin and/or other unique wood character, but wood aged is not necessarily synonymous with imparting wood-flavors. Used sherry, rum, bourbon, scotch, port, wine and other barrels are often used, imparting complexity and uniqueness to beer. Ultimately a balance of flavor, aroma and mouthfeel are sought with the marriage of new beer with wood and/or barrel flavors. Beers in this style may or may not have Brettanomyces character. Brewers when entering this category should specify type of barrel and/or wood used and any other special treatment or ingredients used. Competition managers may create style subcategories to differentiate between high alcohol and low alcohol beers and very dark and lighter colored beer as well as for fruit beers and non-fruit beers. Competitions may develop guidelines requesting brewers to specify what kind of wood (new or used oak, other wood varieties) and/or barrel (whiskey, port, sherry, wine, etc.) was used in the process. The brewer may be asked to explain the special nature (wood used, base beer style(s) and achieved character) of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:27"
                }
            },
            {
                "id": "VM5dme",
                "name": "Bourbon Black Cherry Porter",
                "nameDisplay": "Bourbon Black Cherry Porter",
                "description": "Bourbon Black Cherry Porter is Short’s Black Cherry Porter aged in bourbon barrels for 9 months. This Porter is brewed with sweet black cherry puree and eight different malts.",
                "abv": "8",
                "ibu": "52",
                "glasswareId": 8,
                "styleId": 158,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/VM5dme/upload_K1pGEj-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/VM5dme/upload_K1pGEj-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/VM5dme/upload_K1pGEj-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-20 19:22:47",
                "updateDate": "2017-08-30 19:24:33",
                "glass": {
                    "id": 8,
                    "name": "Tulip",
                    "createDate": "2012-01-03 02:41:33"
                },
                "style": {
                    "id": 158,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Imperial Porter",
                    "shortName": "American Imperial Porter",
                    "description": "American-style imperial porters are black in color. No roast barley or strong burnt/astringent black malt character should be perceived. Medium malt, caramel and cocoa-like sweetness. Hop bitterness is perceived at a medium-low to medium level. Hop flavor and aroma may vary from being low to medium-high. This is a full bodied beer. Ale-like fruity esters should be evident but not overpowering and compliment hop character and malt derived sweetness. Diacetyl (butterscotch) levels should be absent.",
                    "ibuMin": "35",
                    "ibuMax": "50",
                    "abvMin": "5.5",
                    "abvMax": "9.5",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.08",
                    "ogMax": "1.1",
                    "fgMin": "1.02",
                    "fgMax": "1.03",
                    "createDate": "2013-08-10 12:42:51",
                    "updateDate": "2015-04-07 15:49:32"
                }
            },
            {
                "id": "KzjA1z",
                "name": "Bourbon Carrot Cake",
                "nameDisplay": "Bourbon Carrot Cake",
                "description": "Bourbon Carrot Cake is an experimental dessert ale made with carrots, marshmallow, vanilla, maple syrup, orange zest, walnuts, pecans and spices and then aged in bourbon barrels. The aroma is enticing, emitting fragrances that literally recreate a slice of carrot cake that has been soaked in bourbon, right down to the cream cheese frosting. Incredibly complex, yet surprisingly balanced, the flavor of each specialty ingredient seems to take its turn. Pleasant flavors of spice and cream resonate on the pallet causing an uncontrollable craving to taste more.",
                "abv": "7.5",
                "ibu": "9",
                "styleId": 130,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-08-27 17:46:58",
                "updateDate": "2014-09-11 20:08:33",
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "5wFpK2",
                "name": "Bourbon ControversiALE",
                "nameDisplay": "Bourbon ControversiALE",
                "abv": "6.7",
                "styleId": 30,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-07-31 18:42:11",
                "updateDate": "2015-07-31 18:42:11",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "YRw41w",
                "name": "Bourbon Evil Urges",
                "nameDisplay": "Bourbon Evil Urges",
                "description": "Bourbon Evil Urges is a bourbon barrel aged Belgian dark strong ale. A sharp aroma of chocolate and molasses hits the senses, reminiscent of a rich liqueur. Aided by additions of Belgian amber candy sugar, the initial flavors are sweet and malty, with some unique, dark fruit qualities. This beer is full bodied with an intense warming finish.",
                "abv": "8.5",
                "ibu": "33",
                "styleId": 64,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:43:26",
                "updateDate": "2014-09-12 14:04:45",
                "style": {
                    "id": 64,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Dark Strong Ale",
                    "shortName": "Belgian Dark Strong",
                    "description": "Belgian dark strong ales are amber to dark brown in color. Often, though not always, brewed with dark Belgian \"candy\" sugar, these beers can be well attenuated, ranging from medium to full-bodied. The perception of hop bitterness is low to medium, with hop flavor and aroma also in this range. Fruity complexity along with the soft flavors of roasted malts add distinct character. The alcohol strength of these beers can often be deceiving to the senses. The intensity of malt character can be rich, creamy, and sweet with intensities ranging from medium to high. Very little or no diacetyl is perceived. Herbs and spices are sometimes used to delicately flavor these strong ales. Low levels of phenolic spiciness from yeast byproducts may also be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "20",
                    "ibuMax": "50",
                    "abvMin": "7",
                    "abvMax": "11",
                    "srmMin": "9",
                    "srmMax": "35",
                    "ogMin": "1.064",
                    "fgMin": "1.012",
                    "fgMax": "1.024",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:23"
                }
            },
            {
                "id": "zN2ccz",
                "name": "Bourbon Pig Bird",
                "nameDisplay": "Bourbon Pig Bird",
                "abv": "7.7",
                "styleId": 133,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-01-22 21:16:49",
                "updateDate": "2016-01-22 21:16:49",
                "style": {
                    "id": 133,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Wood- and Barrel-Aged Pale to Amber Beer",
                    "shortName": "BBL Aged Pale",
                    "description": "Any classic style or unique experimental beer that has been aged for a period of time in a wooden barrel or in contact with wood. This beer is aged with the intention of imparting the particularly unique character of the wood and/or what has previously been in the barrel. New wood character can be characterized as a complex blend of vanillin and/or other unique wood character but wood aged is not necessarily synonymous with imparting wood-flavors. Used sherry, rum, bourbon, scotch, port, wine and other barrels are often used, imparting complexity and uniqueness to beer. Ultimately a balance of flavor, aroma and mouthfeel are sought with the marriage of new beer with wood and/or barrel flavors. Primary character of the beer style may or may not be apparent. Sour wood-aged beer of any color is outlined in other categories. Fruited or spiced beer that is wood and barrel aged would also be appropriately entered in this category. Beers in this style may or may not have Brettanomyces character.  The brewer should explain the special nature of the beer to allow for accurate judging. Comments could include: type of wood used (new or old, oak or other wood type), type of barrel used (new, port/ whiskey/ wine/ sherry/ other), base beer style or achieved character. Beer entries not accompanied by this information will be at a disadvantage during judging.",
                    "abvMin": "3.75",
                    "abvMax": "6.5",
                    "srmMin": "4",
                    "srmMax": "18",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:34"
                }
            },
            {
                "id": "1CMgHY",
                "name": "Bucktricutioner",
                "nameDisplay": "Bucktricutioner",
                "description": "Bucktricutioner is a light bodied Berliner Weisse brewed with 50% malted wheat, strawberries, and limes. This beer is pink in color with notable aromas of wheat and yeast esters that combine with some subtle citrus scents, creating a pleasantly refreshing nose. Delicate grain flavors give way to bright, tart lemon – lime qualities and sweet fruit before slightly drying the palate for an over all crisp and clean finish.",
                "abv": "3.5",
                "styleId": 54,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-06-08 19:21:12",
                "updateDate": "2015-06-09 14:50:28",
                "style": {
                    "id": 54,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Bamberg-Style Weiss (Smoke) Rauchbier (Dunkel or Helles)",
                    "shortName": "Rauchbier",
                    "description": "Bamberg-style Weiss Rauchbier should have smoky characters that range from detectable to prevalent in the aroma and flavor. Smoke character is not harshly phenolic, but rather very smooth, almost rendering a perception of mild sweetness to this style of beer. The aroma and flavor of a Weissbier with yeast is decidedly fruity and phenolic. The phenolic characteristics are often described as clove- or nutmeg-like and can be smoky or even vanilla-like. Banana-like esters are often present. These beers are made with at least 50 percent malted wheat, and hop rates are quite low. Hop flavor and aroma are absent. Weissbier is well attenuated and very highly carbonated and a medium- to full-bodied beer. The color is very pale to very dark amber. Darker (dunkel) styles should have a detectable degree of roast malt in the balance without being robust in overall character. Because yeast is present, the beer will have yeast flavor and a characteristically fuller mouthfeel and may be appropriately very cloudy. No diacetyl should be perceived.",
                    "ibuMin": "10",
                    "ibuMax": "15",
                    "abvMin": "4.9",
                    "abvMax": "5.5",
                    "srmMin": "4",
                    "srmMax": "18",
                    "ogMin": "1.047",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:30:29"
                }
            },
            {
                "id": "QiEkgx",
                "name": "Captain Fantasy",
                "nameDisplay": "Captain Fantasy",
                "description": "Brewed in collaboration with Short's Brewing, this ale was constructed using a Saison yeast and pears, then twisted with Sorachi Ace hops from Japan. Another nod to the band Ween.",
                "abv": "7",
                "ibu": "70",
                "availableId": 2,
                "styleId": 72,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/QiEkgx/upload_bXnBZj-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/QiEkgx/upload_bXnBZj-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/QiEkgx/upload_bXnBZj-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-04-16 13:10:17",
                "updateDate": "2017-01-10 21:38:55",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 72,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "French & Belgian-Style Saison",
                    "shortName": "Saison",
                    "description": "Beers in this category are golden to deep amber in color. There may be quite a variety of characters within this style. Generally: They are light to medium in body. Malt aroma is low to medium-low. Esters are medium to high in  aroma, while, complex alcohols, herbs, spices, low Brettanomyces character and even clove and smoke-like phenolics may or may not be evident in the overall balanced beer. Hop aroma and flavor may be at low to medium levels. Malt flavor is low but provides foundation for the overall balance. Hop bitterness is moderate to moderately assertive. Herb and/or spice flavors, including black pepper-like notes, may or may not be evident. Fruitiness from fermentation is generally in character. A balanced small amount of sour or acidic flavors is acceptable when in balance with other components. Earthy, cellar-like, musty aromas are okay. Diacetyl should not be perceived. Chill or slight yeast haze is okay. Often bottle conditioned with some yeast character and high carbonation. French & Belgian-Style Saison may have Brettanomyces characters that are slightly acidity, fruity, horsey, goaty and/or leather-like.",
                    "ibuMin": "20",
                    "ibuMax": "40",
                    "abvMin": "4.5",
                    "abvMax": "8.5",
                    "srmMin": "4",
                    "srmMax": "14",
                    "ogMin": "1.055",
                    "fgMin": "1.004",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:34:55"
                }
            },
            {
                "id": "YtMZJv",
                "name": "Carob Stout",
                "nameDisplay": "Carob Stout",
                "description": "Carob Stout is a big bodied stout that receives its distinguishable flavor characteristics from the substantial use of roasted malt, dark specialty grains and ground carob. Cocoa and chocolate-like aromas penetrate from the black depths of this alluring ale. Robust flavors of roasted black coffee mix with the abundant, rich chocolate qualities. As it warms, an ample bitterness from the dark roasted specialty grains and high alpha hops intensifies. The finish is dry on the palate.",
                "abv": "6.4",
                "ibu": "92",
                "availableId": 2,
                "styleId": 42,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/YtMZJv/upload_zyEGGO-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/YtMZJv/upload_zyEGGO-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/YtMZJv/upload_zyEGGO-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:23:37",
                "updateDate": "2015-12-17 21:10:39",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 42,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Stout",
                    "shortName": "American Stout",
                    "description": "Initial low to medium malt sweetness with a degree of caramel, chocolate and/or roasted coffee flavor with a distinctive dryroasted bitterness in the finish. Coffee-like roasted barley and roasted malt aromas are prominent. Some slight roasted malt acidity is permissible and a medium- to full-bodied mouthfeel is appropriate. Hop bitterness may be moderate to high. Hop aroma and flavor is moderate to high, often with American citrus-type and/or resiny hop character. The perception of fruity esters is low. Roasted malt/barley astringency may be low but not excessive. Diacetyl (butterscotch) should be negligible or not perceived. Head retention is excellent.",
                    "ibuMin": "35",
                    "ibuMax": "60",
                    "abvMin": "5.7",
                    "abvMax": "8.8",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.05",
                    "fgMin": "1.01",
                    "fgMax": "1.022",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:43"
                }
            },
            {
                "id": "LsXbEj",
                "name": "Carrot Cake",
                "nameDisplay": "Carrot Cake",
                "description": "Carrot Cake is an Experimental Strong American Brown Ale developed to taste like a traditional carrot cake. The malty rich base provides the “cake batter” to support the walnuts, pecans, ginger, allspice, cinnamon, maple syrup and carrot puree for additional depth, complexity and flavor. Simcoe hops and orange zest add a slight citrus element coupled with the vanilla and marshmallow fluff to deliver the cream cheese frosting component.",
                "abv": "8",
                "ibu": "9",
                "styleId": 130,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/LsXbEj/upload_HvRpHv-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/LsXbEj/upload_HvRpHv-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/LsXbEj/upload_HvRpHv-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-12 20:47:23",
                "updateDate": "2017-08-30 19:15:20",
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "If8J3l",
                "name": "Cat's Pajamas",
                "nameDisplay": "Cat's Pajamas",
                "abv": "9",
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/If8J3l/upload_cKSz4i-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/If8J3l/upload_cKSz4i-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/If8J3l/upload_cKSz4i-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-01-11 20:16:25",
                "updateDate": "2017-01-03 13:57:29",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "Pbk2rI",
                "name": "Celestial Critter",
                "nameDisplay": "Celestial Critter",
                "description": "Celestial Critter is a deep gold colored Belgian Blond Ale with delicate malted grain aromatics and minimal fruity and spicy yeast phenolics.  The body is relatively light with some sweet sugary and honey like flavors.  A Grassy and some what peppery hop finish leads into a slight bitterness before turning moderately dry and relatively clean.",
                "abv": "6.4",
                "ibu": "35",
                "styleId": 61,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 21:45:41",
                "updateDate": "2014-12-17 21:45:41",
                "style": {
                    "id": 61,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Blonde Ale",
                    "shortName": "Belgian Blonde",
                    "description": "Belgian-style blond ales are characterized by low yet evident hop bitterness, flavor, and sometimes aroma. Light to medium body and low malt aroma with a sweet, spiced and a low to medium fruity-ester character orchestrated in flavor and aroma. Sugar may be used to lighten perceived body. They are blonde to golden in color. Noble-type hops are commonly used. Low levels of phenolic spiciness from yeast byproducts may be perceived. Diacetyl should not be perceived. Acidic character should not be present. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "15",
                    "ibuMax": "30",
                    "abvMin": "6",
                    "abvMax": "7.8",
                    "srmMin": "4",
                    "srmMax": "7",
                    "ogMin": "1.054",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:01"
                }
            },
            {
                "id": "e2Y6n7",
                "name": "Cerveza De Julie",
                "nameDisplay": "Cerveza De Julie",
                "description": "Cerveza De Julie is a pale yellow Mexican Lager brewed with real lime. Delicate yeasty aromas gather quickly from the ample amount of carbonation in this lighter bodied beer style. Soft grain sweetness enhanced by unobtrusive lime qualities bring out a subtle snappy tartness, which turns mildly bitter before a crisp refreshing finish.",
                "abv": "5",
                "ibu": "20",
                "availableId": 2,
                "styleId": 93,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/e2Y6n7/upload_oDTnIS-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/e2Y6n7/upload_oDTnIS-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/e2Y6n7/upload_oDTnIS-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:47:35",
                "updateDate": "2015-12-17 18:07:50",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 93,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Lager",
                    "shortName": "American Lager",
                    "description": "Light in body and very light to straw in color, American lagers are very clean and crisp and aggressively carbonated. Flavor components should b e subtle and complex, with no one ingredient dominating the others. Malt sweetness is light to mild. Corn, rice, or other grain or sugar adjuncts are often used. Hop bitterness, flavor and aroma are negligible to very light. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "13",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:26"
                }
            },
            {
                "id": "6sUcra",
                "name": "Chatterbox",
                "nameDisplay": "Chatterbox",
                "description": "Chatterbox is an American Pale Ale that has inviting hop aromas of sweet orange and tangerine. A balanced presentation of light toasted malt and fruity hop flavors lead into a bold bitterness. Chatterbox’s hoppy characteristics resonate in the finish, creating a very subtle dryness",
                "abv": "6",
                "styleId": 25,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/6sUcra/upload_e2dpw5-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/6sUcra/upload_e2dpw5-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/6sUcra/upload_e2dpw5-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2013-12-05 12:48:59",
                "updateDate": "2015-12-17 09:00:23",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "CCM0Jl",
                "name": "Chief Hopper",
                "nameDisplay": "Chief Hopper",
                "description": "Chief Hopper is a Double IPA hopped with Vic Secret, Simcoe, and Centennial hops inspired by the show “Stranger Things.” Chief Hopper is deep gold in color and has a dense white head. Dank aromas of pine, grapefruit, pineapple, and mango are accompanied by just a hint of malt. Smooth and balanced in flavor, a citrus forefront is matched with equal malt character.",
                "abv": "9.2",
                "ibu": "82",
                "styleId": 31,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-03-13 14:32:47",
                "updateDate": "2017-03-13 14:32:47",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "5s0tI6",
                "name": "Chocolate Wheat",
                "nameDisplay": "Chocolate Wheat",
                "description": "Chocolate Wheat is a Porter brewed with chocolate malt, wheat malt, and well-chosen specialty grains. The combination creates a deep black, full-bodied beer that provides subtle hop flavors and rich malt tones. Chocolate Wheat balances the flavors of roasted caramel, coffee, and chocolate, which ends in a smooth finish.",
                "abv": "6.1",
                "ibu": "30",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 18,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/5s0tI6/upload_zOKUGm-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/5s0tI6/upload_zOKUGm-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/5s0tI6/upload_zOKUGm-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:21",
                "updateDate": "2015-12-16 13:09:55",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 18,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Brown Porter",
                    "shortName": "Brown Porter",
                    "description": "Brown porters are mid to dark brown (may have red tint) in color. No roast barley or strong burnt/black malt character should be perceived. Low to medium malt sweetness, caramel and chocolate is acceptable along with medium hop bitterness. This is a lightto medium-bodied beer. Fruity esters are acceptable. Hop flavor and aroma may vary from being negligible to medium in character.",
                    "ibuMin": "20",
                    "ibuMax": "30",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "20",
                    "srmMax": "35",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:21:43"
                }
            },
            {
                "id": "SrK1Gl",
                "name": "Cinnabilly",
                "nameDisplay": "Cinnabilly",
                "description": "Cinnabilly has an appealing look and aroma. This clear, caramel colored gem emits a soft potpourri of cinnamon sticks, brown sugar, and hints of vanilla. Sweet malt flavors allow for an array of sensations, ranging from toasted almonds to cinnamon graham crackers. The pleasant addition of cinnamon creates wonderful accents throughout the beer that are never overwhelming.",
                "abv": "7",
                "ibu": "30",
                "styleId": 124,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:40:12",
                "updateDate": "2014-09-12 14:13:25",
                "style": {
                    "id": 124,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Herb and Spice Beer",
                    "shortName": "Spice Beer",
                    "description": "Herb beers use herbs or spices (derived from roots, seeds, fruits, vegetable, flowers, etc.) other than or in addition to hops to create a distinct (ranging from subtle to intense) character, though individual characters of herbs and/or spices used may not always be identifiable. Under hopping often, but not always, allows the spice or herb to contribute to the flavor profile. Positive evaluations are significantly based on perceived balance of flavors. Note: Chili-flavored beers that emphasize heat rather than chili flavor should be entered as a \"spiced\" beer.  A statement by the brewer explaining what herbs or spices are used is essential in order for fair assessment in competitions. Specifying a style upon which the beer is based may help evaluation. If this beer is a classic style with an herb or spice, the brewer should specify the classic style. If no Chocolate or Coffee category exists in a competition, then chocolate and coffee beers should be entered in this category.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:45"
                }
            },
            {
                "id": "9JV66F",
                "name": "ControversiALE",
                "nameDisplay": "ControversiALE",
                "description": "Loaded with hops like an IPA, yet drinks like a Pale Ale, we simply decided to call it a Strong Pale Ale. The fragrant, earthy citrus laced nose is instantly detectable. Large amounts of toasted grains and high alpha Simcoe hops form a perfect union that creates the cool sensation of toasted sourdough covered with zesty grapefruit hop marmalade.\r\n\r\nMade exclusively for the City Park Grill.\r\n\r\nControversiALE was one of Shorts first beers and was made and named for the City Park Grill in Petoskey. Here is an excerpt from the Petoskey news explaining why Frank is Hangin:\r\n\r\nIt is said that the ghost of Frank Fochtman, former owner of the City Park Grill in Petoskey, haunts the establishment today. Manager Jason Septic said it is rumored that Fochtman hung himself in the basement, when it was named Grill Cafe. ”I know many people who’ve just had a weird sensation or feeling that there’s someone else down there,” he said, adding that his hair has stood up on the back of his neck on occasion when down in the basement.\r\n\r\nThe ghost has on occasion been blamed for broken glasses, when they would just snap as they sat on the table. ”They seemed to, when just sitting on the table, break where the stem meets the glass,” Septic recalled. However, despite some broken stemware, Fochtman — commonly just called Frank by the staff — does not seem to have a mean or negative presence. ”My personal feeling is that he’d be here, just watching over the place,” Septic said.",
                "abv": "5.5",
                "ibu": "67",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/9JV66F/upload_zchpi6-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/9JV66F/upload_zchpi6-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/9JV66F/upload_zchpi6-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2013-04-20 22:10:46",
                "updateDate": "2015-12-17 02:32:29",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "vWSzEZ",
                "name": "Cozy Blanket",
                "nameDisplay": "Cozy Blanket",
                "description": "Cozy Blanket is a Brown Porter served on Nitro. Dark brown with a thick and creamy mocha colored head, Cozy Blanket has aromas of chocolate and caramel. This Nitro Brown Porter has a velvety, full-bodied mouthfeel with flavors of chocolate, toffee, and caramel. Cozy blanket finishes smooth with a nice nutty flavor.",
                "abv": "5.1",
                "ibu": "31",
                "styleId": 18,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/vWSzEZ/upload_JVYIfp-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/vWSzEZ/upload_JVYIfp-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/vWSzEZ/upload_JVYIfp-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-27 13:33:34",
                "updateDate": "2017-09-27 13:35:04",
                "style": {
                    "id": 18,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Brown Porter",
                    "shortName": "Brown Porter",
                    "description": "Brown porters are mid to dark brown (may have red tint) in color. No roast barley or strong burnt/black malt character should be perceived. Low to medium malt sweetness, caramel and chocolate is acceptable along with medium hop bitterness. This is a lightto medium-bodied beer. Fruity esters are acceptable. Hop flavor and aroma may vary from being negligible to medium in character.",
                    "ibuMin": "20",
                    "ibuMax": "30",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "20",
                    "srmMax": "35",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:21:43"
                }
            },
            {
                "id": "WfgP3S",
                "name": "Critterless",
                "nameDisplay": "Critterless",
                "description": "Critterless is an American Sour Ale brewed with mango and cherry. The beer has a pinkish hue and pours with a small white head. The ale’s initial flavors of mouth puckering tartness fade into a pleasant sweetness that is accompanied by aromas of ripe fruit. Critterless finishes dry with a hint of rye spice.",
                "abv": "8.6",
                "ibu": "5",
                "styleId": 40,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/WfgP3S/upload_y8TUp6-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/WfgP3S/upload_y8TUp6-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/WfgP3S/upload_y8TUp6-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-12-24 15:43:08",
                "updateDate": "2016-12-24 15:44:13",
                "style": {
                    "id": 40,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Sour Ale",
                    "shortName": "Sour",
                    "description": "American sour ales can be very light to black or take on the color of added fruits or other ingredients. There is no Brettanomyces character in this style of beer. Wood- and barrel-aged sour ales are classified elsewhere. If acidity is present it is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash or in fermentation by the use of various microorganisms including certain bacteria and yeasts. Acidic character can be a complex balance of several types of acid and characteristics of age. The evolution of natural acidity develops balanced complexity. Residual flavors that come from liquids previously aged in a barrel such as bourbon or sherry should not be present. Wood vessels may be used during the fermentation and aging process, but wood-derived flavors such as vanillin must not be present. In darker versions, roasted malt, caramel-like and chocolate-like characters should be subtle in both flavor and aroma. American sour may have evident full range of hop aroma and hop bitterness with a full range of body. Estery and fruity-ester characters are evident, sometimes moderate and sometimes intense, yet balanced. Diacetyl and sweet corn-like dimethylsulfide (DMS) should not be perceived. Chill haze, bacteria and yeast-induced haze are allowable at low to medium levels at any temperature. Fruited American-Style Sour Ales will exhibit fruit flavors in harmonious balance with other characters.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:32"
                }
            },
            {
                "id": "0ojte1",
                "name": "Crunchy  Grooves",
                "nameDisplay": "Crunchy  Grooves",
                "abv": "5.25",
                "styleId": 106,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-10-27 16:25:52",
                "updateDate": "2015-10-27 16:25:53",
                "style": {
                    "id": 106,
                    "categoryId": 10,
                    "category": {
                        "id": 10,
                        "name": "International Styles",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "International-Style Pilsener",
                    "shortName": "International Pilsener",
                    "description": "International Pilseners are straw/golden in color and are well attenuated. This medium-bodied beer is often brewed with rice, corn, wheat, or other grain or sugar adjuncts making up part of the mash. Hop bitterness is low to medium. Hop flavor and aroma are low. Residual malt sweetness is low--it does not predominate but may be perceived. Fruity esters and diacetyl should not be perceived. Very low levels of sweet corn-like dimethylsulfide (DMS) character, if perceived, are acceptable. There should be no chill haze.",
                    "ibuMin": "17",
                    "ibuMax": "30",
                    "abvMin": "4.5",
                    "abvMax": "5.3",
                    "srmMin": "3",
                    "srmMax": "4",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:11"
                }
            },
            {
                "id": "yxZpj4",
                "name": "Cup A Joe",
                "nameDisplay": "Cup A Joe",
                "description": "A brew uniquely different from most coffee stouts, we cram Higher Grounds roasted fair trade espresso beans into every facet of the brewing process. Prominent aromatics of malt, espresso, and cocoa are abundant and create a flavor robust with big malt characters fused with cream and coffee. The perfect morning night capper.",
                "abv": "7",
                "ibu": "35",
                "glasswareId": 5,
                "srmId": 40,
                "availableId": 2,
                "styleId": 20,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/yxZpj4/upload_HLVSVv-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/yxZpj4/upload_HLVSVv-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/yxZpj4/upload_HLVSVv-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:02",
                "updateDate": "2015-12-16 13:22:01",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "srm": {
                    "id": 40,
                    "name": "40",
                    "hex": "36080A"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 20,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Sweet or Cream Stout",
                    "shortName": "Sweet Stout",
                    "description": "Sweet stouts, also referred to as cream stouts, have less roasted bitter flavor and a full-bodied mouthfeel. The style can be given more body with milk sugar (lactose) before bottling. Malt sweetness, chocolate, and caramel flavor should dominate the flavor profile and contribute to the aroma. Hops should balance and suppress some of the sweetness without contributing apparent flavor or aroma. The overall impression should be sweet and full-bodied.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "3",
                    "abvMax": "6",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:24:41"
                }
            },
            {
                "id": "V6eVdn",
                "name": "Dan's Pink Skirt",
                "nameDisplay": "Dan's Pink Skirt",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/V6eVdn/upload_9fXES9-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/V6eVdn/upload_9fXES9-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/V6eVdn/upload_9fXES9-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-12-04 13:00:09",
                "updateDate": "2015-12-16 20:40:07",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "gt1Mvh",
                "name": "Das Lokal",
                "nameDisplay": "Das Lokal",
                "description": "Lokale Licht is our flagship Lager, Local’s Light brewed with German Pilsner malt. The beer is crisp and clean and has a subtle malt quality. Flavors of grass and sweet corn are complimented by the scent of mild noble hops. It is the perfect beer for the craft beer enthusiast or the seasoned macro consumer, alike.",
                "abv": "5.2",
                "ibu": "10",
                "styleId": 95,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-27 13:27:03",
                "updateDate": "2017-09-27 13:27:03",
                "style": {
                    "id": 95,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Low-Carbohydrate Light Lager",
                    "shortName": "American Low-Carb Lager",
                    "description": "These beers are extremely light straw to light amber in color, light in body, and high in carbonation. They should have a maximum carbohydrate level of 3.0 gm per 12 oz. (356 ml). These beers are characterized by extremely high degree of attenuation (often final gravity is less than 1.000 (0 ºPlato), but with typical American-style light lager alcohol levels. Corn, rice, or other grain adjuncts are often used. Flavor is very light/mild and very dry. Hop flavor, aroma and bitterness are negligible to very low. Very low yeasty flavors and fruity esters are acceptable in aroma and flavor. Chill haze and diacetyl should not be perceived.",
                    "ibuMin": "3",
                    "ibuMax": "10",
                    "abvMin": "3.5",
                    "abvMax": "4.4",
                    "srmMin": "2",
                    "srmMax": "10",
                    "ogMin": "1.024",
                    "fgMin": "0.992",
                    "fgMax": "1.004",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:47"
                }
            },
            {
                "id": "FNQDn1",
                "name": "Deacon Blues",
                "nameDisplay": "Deacon Blues",
                "description": "Deacon Blues is a hazy yellow Extra Pale Ale brewed with bold American hops, but fermented with a Belgian yeast strain.  Diverse aromas of pronounced citrus fruit with subtle layers of spiciness, are the first indication of the array of flavors stemming from this light bodied ale.  Snappy lemon qualities, along with a fresh ginger-like liveliness, flow appropriately into a grassy bitterness that’s moderately intense, but certainly not overwhelming.",
                "abv": "4.4",
                "ibu": "55",
                "styleId": 125,
                "isOrganic": "Y",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/FNQDn1/upload_WAUNQg-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/FNQDn1/upload_WAUNQg-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/FNQDn1/upload_WAUNQg-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-06-18 14:05:21",
                "updateDate": "2015-12-18 01:34:39",
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "AlesBN",
                "name": "DeclarationALE",
                "nameDisplay": "DeclarationALE",
                "description": "A perfect copper-colored American India Pale Ale abundant with juicy fruit-filled hop aromas. Malt sweetness takes a back seat to a medley of lush hop flavors, ranging from dank wet earthiness to bitter herbs and greens. The finish is punctuated by a clean and dry grassy bitterness.",
                "abv": "6",
                "ibu": "85",
                "styleId": 30,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:44:11",
                "updateDate": "2014-09-12 14:14:26",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "lRktEU",
                "name": "Der Norden",
                "nameDisplay": "Der Norden",
                "description": "Der Norden is a golden blond Pilsner modeled after a traditional version from northern Germany.  Gassy yeast esters join perfectly with delicate aromas of bread and light pilsen malt.  The initial mouthfeel is almost creamy, as soft grain and unique mineral flavors transition quickly toward an expected crispness brought on by ample carbonation.  A heightened bitterness comes through on cue, with “hay-like” Noble hop characteristics that linger on the palate with a perfect dryness to encourage more refreshing tastes.",
                "abv": "5",
                "ibu": "50",
                "styleId": 75,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-10-23 04:11:25",
                "updateDate": "2014-12-17 21:49:49",
                "style": {
                    "id": 75,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Pilsener",
                    "shortName": "German Pilsener",
                    "description": "A classic German Pilsener is very light straw or golden in color and well hopped. Perception of hop bitterness is medium to high. Noble-type hop aroma and flavor are moderate and quite obvious. It is a well-attenuated, medium-light bodied beer, but a malty residual sweetness can be perceived in aroma and flavor. Very low levels of sweet corn-like dimethylsulfide (DMS) character are below most beer drinkers' taste thresholds and are usually not detectable except to the trained or sensitive palate. Other fermentation or hop related sulfur compounds, when perceived at low levels, may be characteristic of this style. Fruity esters and diacetyl should not be perceived. There should be no chill haze. Its head should be dense and rich.",
                    "ibuMin": "25",
                    "ibuMax": "40",
                    "abvMin": "4",
                    "abvMax": "5",
                    "srmMin": "3",
                    "srmMax": "4",
                    "ogMin": "1.044",
                    "fgMin": "1.006",
                    "fgMax": "1.012",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:35:59"
                }
            },
            {
                "id": "eqdpDD",
                "name": "Devils Lettuce",
                "nameDisplay": "Devils Lettuce",
                "description": "A wet hopped lager brewed with fresh hops from Empire Hop Farms in Empire, MI.  The nose is full of hoppy aromatics of citrus zest and subtle spice.  Low malt flavors allow for an array of hoppy attributes ranging from fruity lemon to an earthy grassiness, with an instant bitter impact.  A clean lager finish amplifies the overall bitterness which resonates across the palate.\r\n\r\nDevil’s Lettuce was the first wet hopped Bellaire pub beer. Wet hopped means the hops were picked fresh just hours before the brew.",
                "abv": "5.7",
                "ibu": "80",
                "styleId": 26,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/eqdpDD/upload_3GDoFF-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/eqdpDD/upload_3GDoFF-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/eqdpDD/upload_3GDoFF-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-10-24 03:23:54",
                "updateDate": "2015-12-17 18:12:57",
                "style": {
                    "id": 26,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Fresh \"Wet\" Hop Ale",
                    "shortName": "Wet Hop Ale",
                    "description": "Any style of ale can be made into a fresh hop or wet hop version. These ales are hopped predominantly with fresh (newly harvested and kilned) and/or undried (“wet”) hops. These beers will exhibit especially aromas and flavors of green, almost chlorophyll-like or other fresh hop characters, in harmony with the characters of the base style of the beer. These beers may be aged and enjoyed after the initial “fresh-hop” character diminishes. Unique character from “aged” fresh hop beers may emerge, but they have yet to be defined.",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:25"
                }
            },
            {
                "id": "SF5xs7",
                "name": "Divine Minthe",
                "nameDisplay": "Divine Minthe",
                "description": "Divine Minthe is a Stout brewed with cacao nibs and mint. Pitch black in color, the mocha head holds aromas reminiscent of a thin mint cookie. Divine Minthe in medium bodied and has flavors of roast, caramel, chocolate and mint blending together for a smooth finish.",
                "abv": "6.1",
                "styleId": 130,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-12-20 13:55:56",
                "updateDate": "2017-12-21 18:16:09",
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "wZdlAU",
                "name": "Dolly Dagger",
                "nameDisplay": "Dolly Dagger",
                "description": "Dolly Dagger is a Double Belgian India Red Ale that harmoniously joins big sweet malt aromatics with signature fruit and spice yeast characteristics. Flavors of toasted grain, caramel, dark fruit, and clove provide a perfect precursor to the substantial bitter contributions from the exclusive use of high alpha Bravo hops. The end result is a balanced blend of big grain sweetness, intense hop bitterness, and unmistakable yeast qualities.",
                "abv": "7.4",
                "ibu": "75",
                "availableId": 2,
                "styleId": 33,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/wZdlAU/upload_NeS3Ws-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/wZdlAU/upload_NeS3Ws-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/wZdlAU/upload_NeS3Ws-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:22:22",
                "updateDate": "2015-12-17 21:09:47",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 33,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial Red Ale",
                    "shortName": "Imperial Red",
                    "description": "Imperial Red Ales are deep amber to dark copper/reddish brown. A small amount of chill haze is allowable at cold temperatures. Fruity-ester aroma is medium. Hop aroma is intense, arising from any variety of hops. Medium to high caramel malt character is present. Hop flavor is intense, and balanced with other beer characters. They may use any variety of hops. Hop bitterness is intense. Alcohol content is very high and of notable character. Complex alcohol flavors may be evident. Fruity-ester flavors are medium. Diacetyl should not be perceived. Body is full.",
                    "ibuMin": "55",
                    "ibuMax": "85",
                    "abvMin": "7.9",
                    "abvMax": "10.5",
                    "srmMin": "10",
                    "srmMax": "15",
                    "ogMin": "1.08",
                    "fgMin": "1.02",
                    "fgMax": "1.028",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 17:05:43"
                }
            },
            {
                "id": "ZmenHN",
                "name": "Dr. Thrillride",
                "nameDisplay": "Dr. Thrillride",
                "description": "Dr. Thrillride is a Porter brewed with coffee and vanilla. This beer is a deep brown color and pours with a frothy mocha head. Well-balanced aromas and flavors of fresh coffee, chocolate, and a hint of vanilla abound from this brew. Dr. Thrillride is medium-bodied and easy drinking.",
                "abv": "5",
                "ibu": "35",
                "styleId": 18,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/ZmenHN/upload_njXyGA-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/ZmenHN/upload_njXyGA-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/ZmenHN/upload_njXyGA-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-01 14:13:49",
                "updateDate": "2017-10-01 14:15:08",
                "style": {
                    "id": 18,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Brown Porter",
                    "shortName": "Brown Porter",
                    "description": "Brown porters are mid to dark brown (may have red tint) in color. No roast barley or strong burnt/black malt character should be perceived. Low to medium malt sweetness, caramel and chocolate is acceptable along with medium hop bitterness. This is a lightto medium-bodied beer. Fruity esters are acceptable. Hop flavor and aroma may vary from being negligible to medium in character.",
                    "ibuMin": "20",
                    "ibuMax": "30",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "20",
                    "srmMax": "35",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:21:43"
                }
            },
            {
                "id": "eFBfAA",
                "name": "Dr. Zeus",
                "nameDisplay": "Dr. Zeus",
                "description": "Dr. Zeus is brewed with a combination of English Ale yeast and bold American Zeus hops. This combination creates a unique sharp nose of spicy herbs and tart lemon. Noticeable grassy and straw hop flavors, intermixed with slight peppery notes, outweighs the grain contributions from the smaller malt bill. An intense bitterness takes over, before leading into a dry finish.",
                "abv": "4.5",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/eFBfAA/upload_7XqDUC-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/eFBfAA/upload_7XqDUC-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/eFBfAA/upload_7XqDUC-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 21:52:16",
                "updateDate": "2015-12-17 19:49:58",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "RTAvgW",
                "name": "Earl of Brixom",
                "nameDisplay": "Earl of Brixom",
                "abv": "3.9",
                "styleId": 11,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 19:00:52",
                "updateDate": "2015-12-18 19:00:52",
                "style": {
                    "id": 11,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "English-Style Dark Mild Ale",
                    "shortName": "English Dark Mild",
                    "description": "English dark mild ales range from deep copper to dark brown (often with a red tint) in color. Malt flavor and caramel are part of the flavor and aroma profile while, licorice and roast malt tones may sometimes contribute to the flavor and aroma profile. Body should be low-medium to medium. These beers have very little hop flavor or aroma. Very low diacetyl flavors may be appropriate in this low-alcohol beer. Fruity-ester level is very low.",
                    "ibuMin": "10",
                    "ibuMax": "24",
                    "abvMin": "3.2",
                    "abvMax": "4",
                    "srmMin": "17",
                    "srmMax": "34",
                    "ogMin": "1.03",
                    "fgMin": "1.004",
                    "fgMax": "1.008",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:20:43"
                }
            },
            {
                "id": "g6cXzO",
                "name": "Electric Mullet",
                "nameDisplay": "Electric Mullet",
                "abv": "6.7",
                "styleId": 109,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 19:08:11",
                "updateDate": "2015-12-18 19:08:11",
                "style": {
                    "id": 109,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Cream Ale or Lager",
                    "shortName": "Cream Ale",
                    "description": "Mild, pale, light-bodied ale, made using a warm fermentation (top or bottom) and cold lagering. Hop bitterness and flavor range from very low to low. Hop aroma is often absent. Sometimes referred to as cream ales, these beers are crisp and refreshing. Pale malt character predominates. Caramelized malt character should be absent. A fruity or estery aroma may be perceived. Diacetyl and chill haze should not be perceived. Sulfur character and/or sweet corn-like dimethylsulfide (DMS) should be extremely low or absent from this style of beer.",
                    "ibuMin": "10",
                    "ibuMax": "22",
                    "abvMin": "4.2",
                    "abvMax": "5.6",
                    "srmMin": "2",
                    "srmMax": "5",
                    "ogMin": "1.044",
                    "fgMin": "1.004",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:30"
                }
            },
            {
                "id": "sF30Yi",
                "name": "Empress Catherine",
                "nameDisplay": "Empress Catherine",
                "styleId": 43,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/sF30Yi/upload_FujCKD-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/sF30Yi/upload_FujCKD-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/sF30Yi/upload_FujCKD-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2013-12-05 12:48:16",
                "updateDate": "2015-12-17 08:55:05",
                "style": {
                    "id": 43,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Imperial Stout",
                    "shortName": "American Imperial Stout",
                    "description": "Black in color, American-style imperial stouts typically have a high alcohol content. Generally characterized as very robust. The extremely rich malty flavor and aroma are balanced with assertive hopping and fruity-ester characteristics. Bitterness should be moderately high to very high and balanced with full sweet malt character. Roasted malt astringency and bitterness can be moderately perceived but should not overwhelm the overall character. Hop aroma is usually moderately-high to overwhelmingly hop-floral, -citrus or -herbal. Diacetyl (butterscotch) levels should be absent.",
                    "ibuMin": "50",
                    "ibuMax": "80",
                    "abvMin": "7",
                    "abvMax": "12",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.08",
                    "fgMin": "1.02",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:49"
                }
            },
            {
                "id": "GnhQEX",
                "name": "Ermagerdness",
                "nameDisplay": "Ermagerdness",
                "description": "A light bodied golden colored Pale Ale with zesty tangerine and pineapple aromatics.  A simplified malt bill, along with some signature American hops, allows for an awesome burst of tropical fruit flavors up front.  It then shifts toward an interesting herbal tasting bitterness, which builds and lingers on the back of the palate.\r\n \r\nThis new American India Pale Ale is what Ryan Hale, the head brewer at the Bellaire pub, referred to as his quintessential IPA. Ryan combined all of his favorite hops resulting in what he described as “pure juicy goodness”. This beer is loaded with all of the big citrus and fruity flavors American hops can offer.",
                "abv": "5.5",
                "ibu": "84",
                "availableId": 2,
                "styleId": 25,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/GnhQEX/upload_XZIJrk-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/GnhQEX/upload_XZIJrk-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/GnhQEX/upload_XZIJrk-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:33:19",
                "updateDate": "2015-12-17 20:46:59",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "AmMuT8",
                "name": "Evil Urges",
                "nameDisplay": "Evil Urges",
                "description": "Evil Urges is a Belgian dark strong ale with a deep dark brown color. A sharp aroma of chocolate and molasses hits the senses, reminiscent of a rich liqueur. Aided by additions of Belgian amber candi sugar, the initial flavors are sweet and malty, with some unique, dark fruit qualities. This full bodied beer is defined by its roast malt character and slight black coffee bitterness that lead into an intense warming finish.",
                "abv": "8.4",
                "ibu": "50",
                "styleId": 64,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/AmMuT8/upload_J0fSjL-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/AmMuT8/upload_J0fSjL-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/AmMuT8/upload_J0fSjL-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-23 20:26:07",
                "updateDate": "2015-12-17 19:16:29",
                "style": {
                    "id": 64,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Dark Strong Ale",
                    "shortName": "Belgian Dark Strong",
                    "description": "Belgian dark strong ales are amber to dark brown in color. Often, though not always, brewed with dark Belgian \"candy\" sugar, these beers can be well attenuated, ranging from medium to full-bodied. The perception of hop bitterness is low to medium, with hop flavor and aroma also in this range. Fruity complexity along with the soft flavors of roasted malts add distinct character. The alcohol strength of these beers can often be deceiving to the senses. The intensity of malt character can be rich, creamy, and sweet with intensities ranging from medium to high. Very little or no diacetyl is perceived. Herbs and spices are sometimes used to delicately flavor these strong ales. Low levels of phenolic spiciness from yeast byproducts may also be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "20",
                    "ibuMax": "50",
                    "abvMin": "7",
                    "abvMax": "11",
                    "srmMin": "9",
                    "srmMax": "35",
                    "ogMin": "1.064",
                    "fgMin": "1.012",
                    "fgMax": "1.024",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:23"
                }
            },
            {
                "id": "HoO7eS",
                "name": "Exeter",
                "nameDisplay": "Exeter",
                "description": "Exeter is an American Sour Ale fermented with strawberry and lemon. Ruby red in color with an initial head that dissipates quickly, Exeter has a big sour fruit aroma with just a hint of lemon. A mouth puckering tart strawberry and sour lemon flavor is complemented by a growing sweetness. The finish is very dry.",
                "abv": "6.5",
                "availableId": 2,
                "styleId": 170,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-10 16:04:05",
                "updateDate": "2017-10-10 16:04:06",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 170,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Wild Beer",
                    "shortName": "Wild Beer",
                    "description": "Wild Beers are any range of color. These beers may be clear or hazy due to yeast, chill haze or hop haze. Aromas may vary tremendously due to fermentation characters contributed by various known and unknown microorganisms. The overall balance should be complex and balanced. Hop aroma very low to high. Usually because of a high degree of attenuation in these beers, malt character is very low to low. If there are exceptions that are malty, the overall balance of complexity of other characters should be in harmony. Hop flavor very low to high. Hop bitterness is perceived at varying levels depending on the overall balance, but usually perceived as very low to low. Wild beers are \"spontaneously\" fermented with microorganisms that the brewer has introduced from the ambient air/environment in the vicinity of the brewery in which the beer is brewed. Wild beers may not be fermented with any cultured strains of yeast or bacteria. Wild beer may or may not be perceived as acidic. It may include a wildly variable spectrum of flavors and aromas derived from the wild microorganisms with which it was fermented. The overall balance of flavors, aromas, appearance and body is an important factor in assessing these beers. Body is very low to medium. Spontaneously fermented beers with fruit, spice or other ingredients would be appropriately entered as Wild Beer. For purposes of competition, entries which could be appropriately entered in an existing classic or traditional category such as Belgian-Style Lambic, Gueuze, Fruit Lambic, etc. should be entered in that category and not entered as a Wild Beer.",
                    "createDate": "2015-04-07 17:21:44"
                }
            },
            {
                "id": "Lcdy1m",
                "name": "Freedom Of '78",
                "nameDisplay": "Freedom Of '78",
                "description": "Brewed in collaboration with Short's Brewing, featuring Jonathan Cutler of Piece. This beer was made in honor of the band Ween. We loaded up a Citra Hop forward IPA with some wheat and 1000 lbs of Guava Fruit from Ecuador.",
                "abv": "7",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/Lcdy1m/upload_2BTnmf-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/Lcdy1m/upload_2BTnmf-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/Lcdy1m/upload_2BTnmf-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:14",
                "updateDate": "2017-01-10 21:38:55",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "oQ6PaF",
                "name": "Fun Tina",
                "nameDisplay": "Fun Tina",
                "description": "Fun Tina is an American Wheat Ale brewed with orange zest, lemon zest, and coriander. Golden in color and medium-bodied, Fun Tina has light and inviting aromas of wheat, coriander, and citrus. The subtle flavors of citrus and coriander combine for a refreshing beer with a clean finish.",
                "abv": "5",
                "ibu": "20",
                "styleId": 65,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/oQ6PaF/upload_re2mZO-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/oQ6PaF/upload_re2mZO-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/oQ6PaF/upload_re2mZO-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-01 14:28:19",
                "updateDate": "2017-10-01 14:29:08",
                "style": {
                    "id": 65,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style White (or Wit) / Belgian-Style Wheat",
                    "shortName": "Witbier",
                    "description": "Belgian white ales are very pale in color and are brewed using unmalted wheat and malted barley and are spiced with coriander and orange peel. Coriander and light orange peel aroma should be perceived as such or as an unidentified spiciness. Phenolic spiciness and yeast flavors may be evident at mild levels. These beers are traditionally bottle conditioned and served cloudy. An unfiltered starch and yeast haze should be part of the appearance. The low to medium body should have some degree of creaminess from wheat starch. The style is further characterized by the use of noble-type hops to achieve low hop bitterness and little to no apparent hop flavor. This beer has no diacetyl and a low to medium fruity-ester level. Mild acidity is appropriate.",
                    "ibuMin": "10",
                    "ibuMax": "17",
                    "abvMin": "4.8",
                    "abvMax": "5.2",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.044",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:30"
                }
            },
            {
                "id": "bHIdsV",
                "name": "Funkin Punkin",
                "nameDisplay": "Funkin Punkin",
                "description": "A seasonal blend of pumpkin, spices, and carefully formulated ale. Notably aromatic of pumpkin pie, and the color of a light pastel brown, this brew is especially true of ale and comfortably consumable.  Brewed with real pumpkin and real spice, it’s still very much a beer, but beautifully engineered with spice, pumpkin and beer balance.",
                "abv": "5.5",
                "ibu": "11",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 124,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/bHIdsV/upload_hHHmg2-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/bHIdsV/upload_hHHmg2-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/bHIdsV/upload_hHHmg2-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:15",
                "updateDate": "2015-12-16 13:27:30",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 124,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Herb and Spice Beer",
                    "shortName": "Spice Beer",
                    "description": "Herb beers use herbs or spices (derived from roots, seeds, fruits, vegetable, flowers, etc.) other than or in addition to hops to create a distinct (ranging from subtle to intense) character, though individual characters of herbs and/or spices used may not always be identifiable. Under hopping often, but not always, allows the spice or herb to contribute to the flavor profile. Positive evaluations are significantly based on perceived balance of flavors. Note: Chili-flavored beers that emphasize heat rather than chili flavor should be entered as a \"spiced\" beer.  A statement by the brewer explaining what herbs or spices are used is essential in order for fair assessment in competitions. Specifying a style upon which the beer is based may help evaluation. If this beer is a classic style with an herb or spice, the brewer should specify the classic style. If no Chocolate or Coffee category exists in a competition, then chocolate and coffee beers should be entered in this category.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:45"
                }
            },
            {
                "id": "DEmAah",
                "name": "Funky Is As Funky Does",
                "nameDisplay": "Funky Is As Funky Does",
                "description": "Funky is as Funky Does is a wild India Pale Ale fermented with 100% Brettanomyces yeast. Funky barnyard aromas are complimented by bold citrus hop scents, along with some subtle malty notes.  Flavors of fruity bubblegum and sweet tangerine are followed by a sizable bitterness, yet the finish is lightly dry and somewhat clean.",
                "abv": "6.8",
                "ibu": "76",
                "styleId": 30,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-02-22 19:46:04",
                "updateDate": "2016-02-22 19:46:04",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "GYuTJb",
                "name": "Giggle Bush",
                "nameDisplay": "Giggle Bush",
                "description": "A pale ale brewed entirely with freshly picked Michigan Copper hops.  The overall experience of this beer seems slightly subdued from the use of brewing with wet hops.  Faint fruity aromas give way to subtle malt flavors and an interesting peppery hop spiciness.  A light body allows for a moderate bitterness in the finish, that lingers as the palate drys, leaving a mild resiny hoppiness.",
                "abv": "5.9",
                "ibu": "60",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 21:54:17",
                "updateDate": "2014-12-17 21:54:18",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "cGWhLg",
                "name": "Gitchy Gitchy Ooh La La",
                "nameDisplay": "Gitchy Gitchy Ooh La La",
                "description": "Gitchy Gitchy Ooh La La is an Experimental Kolsch style Ale brewed with lemon juice, cucumber and lilac flower. An equal presentation of all three ingredients show up in the nose, as well as a prominent distinction of each in the flavor profile. Pleasant grain qualities help achieve a balance between refreshment and bold ingredients, with a light bitterness to round out the finish.",
                "abv": "5.5",
                "ibu": "35",
                "styleId": 130,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/cGWhLg/upload_2zGQK2-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/cGWhLg/upload_2zGQK2-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/cGWhLg/upload_2zGQK2-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-20 14:16:43",
                "updateDate": "2017-08-30 19:25:54",
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "ZdAmKr",
                "name": "Golden Eagle",
                "nameDisplay": "Golden Eagle",
                "description": "Golden Eagle is a Belgian Golden Ale brewed with blonde candy sugar, and re-fermented with Brettanomyces yeast in Cabernet Franc/Merlot and Blaufrankish oak barrels from Left Foot Charlie.  Despite it’s stylistic origins, the color is closer to a burnt orange, due to the extensive time spent aging in the wine barrels.  Impressive sweet candied apple and cautionary alcohol aromas prepare the senses for the array of flavors within.  Initial tastes of brown sugar and toffee, flow into apple and pear wine-like qualities, before its slightly tart and peppery finish. The heightened alcohol content cannot be ignored, as it’s warming presence encompasses the entire mouthfeel, lasting well into the finish.",
                "abv": "11",
                "ibu": "30",
                "availableId": 2,
                "styleId": 36,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-08-27 17:49:57",
                "updateDate": "2014-08-28 11:46:19",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 36,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Golden or Blonde Ale",
                    "shortName": "Blonde",
                    "description": "Golden or Blonde ales are straw to golden blonde in color. They have a crisp, dry palate, light to medium body, and light malt sweetness. Low to medium hop aroma may be present but does not dominate. Bitterness is low to medium. Fruity esters may be perceived but do not predominate. Diacetyl should not be perceived. Chill haze should be absent.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "4",
                    "abvMax": "5",
                    "srmMin": "3",
                    "srmMax": "7",
                    "ogMin": "1.045",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:26"
                }
            },
            {
                "id": "05CVXM",
                "name": "Gone Commando",
                "nameDisplay": "Gone Commando",
                "description": "Vienna Lager",
                "abv": "4.5",
                "styleId": 80,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-10-23 04:11:26",
                "updateDate": "2014-10-27 20:21:14",
                "style": {
                    "id": 80,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Vienna-Style Lager",
                    "shortName": "Vienna Lager",
                    "description": "Beers in this category are reddish brown or copper colored. They are medium in body. The beer is characterized by malty aroma and slight malt sweetness. The malt aroma and flavor should have a notable degree of toasted and/or slightly roasted malt character. Hop bitterness is clean and crisp. Noble-type hop aromas and flavors should be low or mild. Diacetyl, chill haze and ale-like fruity esters should not be perceived.",
                    "ibuMin": "22",
                    "ibuMax": "28",
                    "abvMin": "4.8",
                    "abvMax": "5.4",
                    "srmMin": "12",
                    "srmMax": "16",
                    "ogMin": "1.046",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:36:42"
                }
            },
            {
                "id": "BScw3H",
                "name": "Good Humans",
                "nameDisplay": "Good Humans",
                "description": "This Double Brown Ale, made with Carabrown Malt, was created to showcase one of Briess Malting Company’s new products. Although it was thought to be a onetime brewing event, this beer is now one of our most recent successes. Sweet malty aromas mingle with prominent toasted caramel and toffee flavors. This beer finishes pleasingly dry, with a bouquet of hops that have slight fruity qualities.",
                "abv": "8",
                "ibu": "19",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 37,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/BScw3H/upload_nOTdDc-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/BScw3H/upload_nOTdDc-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/BScw3H/upload_nOTdDc-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:17",
                "updateDate": "2015-12-16 13:40:24",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 37,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Brown Ale",
                    "shortName": "American Brown",
                    "description": "American brown ales range from deep copper to brown in color. Roasted malt caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma. American brown ales have evident low to medium hop flavor and aroma, medium to high hop bitterness, and a medium body. Estery and fruity-ester characters should be subdued. Diacetyl should not be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "25",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "6.4",
                    "srmMin": "15",
                    "srmMax": "26",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:35"
                }
            },
            {
                "id": "Db1S1w",
                "name": "Good Samaritan",
                "nameDisplay": "Good Samaritan",
                "description": "Brewed with local cider. Sweet apple aromas and spicy esters of clove, nutmeg, and allspice entice the nose. A pleasant crimson amber tone and slight haze give this beer an appearance similar to the cider it was made with, while malty hints of raisin and molasses balance the complex spiciness found throughout the profile. The finish is surprisingly clean with no particular flavor component resonating on the palette.",
                "glasswareId": 5,
                "availableId": 2,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:17",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                }
            },
            {
                "id": "JvQQ7R",
                "name": "Goodnight Bodacious",
                "nameDisplay": "Goodnight Bodacious",
                "description": "Goodnight Bodacious has the maltiness of a barleywine and the hoppiness of an India Pale Ale. A rich, hoppiness dominates the aroma, with earthy and loamy scents. A medium body presents big, bitter black barley and roasted coffee flavors, with subtle compliments of dark fruit and green hops. Strong alcohol flavors and pleasant warming qualities become more noticeable as the beer approaches room temperature.",
                "abv": "9",
                "ibu": "90",
                "styleId": 41,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/JvQQ7R/upload_ysnsUS-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/JvQQ7R/upload_ysnsUS-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/JvQQ7R/upload_ysnsUS-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-12-04 12:46:49",
                "updateDate": "2015-12-16 20:51:04",
                "style": {
                    "id": 41,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Black Ale",
                    "shortName": "Black Ale",
                    "description": "American-style Black Ales are very dark to black and perceived to have medium high to high hop bitterness, flavor and aroma with medium-high alcohol content, balanced with a medium body. Fruity, floral and herbal character from hops of all origins may contribute character. The style is further characterized by a balanced and moderate degree of caramel malt and dark roasted malt flavor and aroma. High astringency and high degree of burnt roast malt character should be absent.",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6",
                    "abvMax": "7.5",
                    "srmMin": "35",
                    "srmMax": "35",
                    "ogMin": "1.056",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:36"
                }
            },
            {
                "id": "nmLkCi",
                "name": "Hangin Frank",
                "nameDisplay": "Hangin Frank",
                "description": "Loaded with hops like an IPA, yet drinks like a Pale Ale, we simply decided to call it a Strong Pale Ale. The fragrant, earthy citrus laced nose is instantly detectable. Large amounts of toasted grains and high alpha Simcoe hops form a perfect union that creates the cool sensation of toasted sourdough covered with zesty grapefruit hop marmalade.\r\n\r\nMade exclusively for the City Park Grill. \r\nNow called ControversiALE!",
                "abv": "5.5",
                "ibu": "67",
                "glasswareId": 5,
                "availableId": 3,
                "styleId": 25,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/nmLkCi/upload_AQMQyv-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/nmLkCi/upload_AQMQyv-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/nmLkCi/upload_AQMQyv-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:19",
                "updateDate": "2015-12-16 14:03:00",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 3,
                    "name": "Not Available",
                    "description": "Beer is not available."
                },
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "PeaC0K",
                "name": "Happy Jack",
                "nameDisplay": "Happy Jack",
                "abv": "5.5",
                "styleId": 9,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-09-24 18:54:00",
                "updateDate": "2015-09-24 18:54:00",
                "style": {
                    "id": 9,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Scottish-Style Export Ale",
                    "shortName": "Scottish Export",
                    "description": "The overriding character of Scottish export ale is sweet, caramel-like, and malty. Its bitterness is perceived as low to medium. Hop flavor or aroma should not be perceived. It has medium body. Fruity-ester character may be apparent. Yeast characters such as diacetyl (butterscotch) and sulfuriness are acceptable at very low levels. The color will range from golden amber to deep brown. Bottled versions of this traditional draft beer may contain higher amounts of carbon dioxide than is typical for mildly carbonated draft versions. Chill haze is acceptable at low temperatures. Though there is little evidence suggesting that traditionally made Scottish-style export ales exhibited peat smoke character, the current marketplace offers many Scottish-style export ales with peat or smoke character present at low to medium levels. Thus a peaty/smoky character may be evident at low to medium levels (ales with medium-high or higher smoke character would be considered a smoke flavored beer and considered in another category). Scottish-style export ales may be split into two subcategories: Traditional (no smoke character) and Peated (low level of peat smoke character).",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "4",
                    "abvMax": "5.3",
                    "srmMin": "10",
                    "srmMax": "19",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:19:56"
                }
            },
            {
                "id": "2qQglM",
                "name": "Hazy ControversiALE",
                "nameDisplay": "Hazy ControversiALE",
                "description": "Hazy ControversiALE is an American India Pale Ale brewed with Simcoe hops and dry hopped with Simcoe powder. Moderately hazy and copper-colored, this beer has a slightly off-white head with aromas of citrus, caramel, and toasted grain. This variation on Short’s classic, ControversiALE, has more prominent citrus notes and a juicier mouth feel in comparison to the original. A strong malt backbone allows the flavor of Simcoe hops to shine before a slightly bitter finish.",
                "abv": "6",
                "ibu": "50",
                "styleId": 125,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/2qQglM/upload_P7Ui4e-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/2qQglM/upload_P7Ui4e-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/2qQglM/upload_P7Ui4e-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-07 13:49:39",
                "updateDate": "2017-09-07 13:51:09",
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "LejVVp",
                "name": "Heavy in the Future",
                "nameDisplay": "Heavy in the Future",
                "abv": "3",
                "styleId": 46,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-07-28 14:20:21",
                "updateDate": "2015-07-28 14:20:21",
                "style": {
                    "id": 46,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Berliner-Style Weisse (Wheat)",
                    "shortName": "Berlinerweisse",
                    "description": "This is very pale in color and the lightest of all the German wheat beers. The unique combination of yeast and lactic acid bacteria fermentation yields a beer that is acidic, highly attenuated, and very light bodied. The carbonation of a Berliner Weisse is high, and hop rates are very low. Clarity may be hazy or cloudy from yeast or chill haze. Hop character should not be perceived. Fruity esters will be evident. No diacetyl should be perceived.",
                    "ibuMin": "3",
                    "ibuMax": "6",
                    "abvMin": "2.8",
                    "abvMax": "3.4",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.028",
                    "fgMin": "1.004",
                    "fgMax": "1.006",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:29:14"
                }
            },
            {
                "id": "NlThNv",
                "name": "Hellacious Rock",
                "nameDisplay": "Hellacious Rock",
                "description": "An American Double India Pale Ale with bold floral hop aromas of citrus peel and pine.  A sizable malt sweetness allows for a balanced presentation of abundant hop flavors, most notably those of sharp grapefruit and fruity berries.  The finish is mostly clean with a perfect resiny bitterness, that lingers, but doesn’t overwhelm the palate.",
                "abv": "8.2",
                "ibu": "83",
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/NlThNv/upload_r5IQoB-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/NlThNv/upload_r5IQoB-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/NlThNv/upload_r5IQoB-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-10-23 04:11:25",
                "updateDate": "2018-06-04 16:42:32",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "uk4AIG",
                "name": "Herbst Hefe",
                "nameDisplay": "Herbst Hefe",
                "abv": "8.1",
                "styleId": 48,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 19:16:07",
                "updateDate": "2015-12-18 19:16:07",
                "style": {
                    "id": 48,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "South German-Style Hefeweizen / Hefeweissbier",
                    "shortName": "Hefeweizen",
                    "description": "The aroma and flavor of a Weissbier with yeast is decidedly fruity and phenolic. The phenolic characteristics are often described as clove-, nutmeg-like, mildly smoke-like or even vanilla-like. Banana-like esters should be present at low to medium-high levels. These beers are made with at least 50 percent malted wheat, and hop rates are quite low. Hop flavor and aroma are absent or present at very low levels. Weissbier is well attenuated and very highly carbonated and a medium to full bodied beer. The color is very pale to pale amber. Because yeast is present, the beer will have yeast flavor and a characteristically fuller mouthfeel and may be appropriately very cloudy. No diacetyl should be perceived.",
                    "ibuMin": "10",
                    "ibuMax": "15",
                    "abvMin": "4.9",
                    "abvMax": "5.5",
                    "srmMin": "3",
                    "srmMax": "9",
                    "ogMin": "1.047",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:29:27"
                }
            },
            {
                "id": "JCkB58",
                "name": "Hoppy 5th",
                "nameDisplay": "Hoppy 5th",
                "description": "Short’s Hoppy 5th is a hoppy Pale Ale with grapefruit zest brewed in honor of The Buck’s 5th anniversary. Straw-colored with aromas of grapefruit, citrus hops, and wheat, Short’s Hoppy 5th is a well balanced Pale Ale. Juicy and medium-bodied, flavors of grapefruit are complemented by Amarillo, Citra, and Mosaic hops. The finish is dry and slightly bitter. It’s clear that the hops, malt, and zest used in this brew work in unison to create a perfectly easy drinking Pale Ale.",
                "abv": "5.1",
                "ibu": "66",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-27 13:29:45",
                "updateDate": "2017-09-27 13:29:45",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "2AFnvf",
                "name": "Hopstache",
                "nameDisplay": "Hopstache",
                "description": "Hopstache typifies the boldness that comes from American hop varieties. The aroma is dominated by fruity ale yeast esters and fragrant grapefruit tones. Initial malt sweetness is low, and citrus fruit and floral hop qualities overpower any other perceived flavors. Aided by the addition of grapefruit zest, the finish is a sharp hop shock that electrifies the mouth, but is not overly bitter.",
                "abv": "5.7",
                "ibu": "85",
                "availableId": 2,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/2AFnvf/upload_NW40nk-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/2AFnvf/upload_NW40nk-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/2AFnvf/upload_NW40nk-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:29:52",
                "updateDate": "2015-12-17 21:03:45",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "qKQ0vj",
                "name": "Hot Loins",
                "nameDisplay": "Hot Loins",
                "description": "Hot Loins is an aphrodisiac-filled Experimental Brown Ale with honey, rose hips, and cinnamon. Light brown with an off-white head and ample lacing, this Experimental Brown Ale smells of cinnamon and toasted malts with floral notes. Flavorful and balanced, Hot Loins has a slightly sweet taste provided by the use of honey before turning slightly dry and floral in the finish.",
                "abv": "6.2",
                "ibu": "11",
                "styleId": 37,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/qKQ0vj/upload_zQgzBB-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/qKQ0vj/upload_zQgzBB-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/qKQ0vj/upload_zQgzBB-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-02-15 19:29:39",
                "updateDate": "2018-02-15 19:31:03",
                "style": {
                    "id": 37,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Brown Ale",
                    "shortName": "American Brown",
                    "description": "American brown ales range from deep copper to brown in color. Roasted malt caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma. American brown ales have evident low to medium hop flavor and aroma, medium to high hop bitterness, and a medium body. Estery and fruity-ester characters should be subdued. Diacetyl should not be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "25",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "6.4",
                    "srmMin": "15",
                    "srmMax": "26",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:35"
                }
            },
            {
                "id": "agyRo5",
                "name": "Huma Lupa Berning Love",
                "nameDisplay": "Huma Lupa Berning Love",
                "description": "HopCat exclusive in celebration of owner Mark & Carrie's Wedding. Special brew of half Huma Lupa Licious and half Soft Parade.",
                "abv": "7.8",
                "ibu": "96",
                "glasswareId": 8,
                "styleId": 30,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-07-05 14:00:16",
                "updateDate": "2016-07-05 14:00:16",
                "glass": {
                    "id": 8,
                    "name": "Tulip",
                    "createDate": "2012-01-03 02:41:33"
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "xKQpNS",
                "name": "Huma-Lupa-Licious",
                "nameDisplay": "Huma-Lupa-Licious",
                "description": "Huma Lupa Licious’ name derives from the hop plant, Humulus lupulus. It is Short’s best-selling India Pale Ale. Huma Lupa Licious is brewed with five different hop varieties, providing a punch to the palate. Between the beer’s abundance of hops, and the type of hops used in its creation, it has a delicious bitter taste and enticing citrus aroma. The hearty malt bill melds well with the hops for a perfect balance.   Huma Lupa Licious is the product of \r\nJoe Short wanting to create the ultimate IPA experience. Originally, Huma Lupa Licious was named Humulus Lupulus Maximus, and went through several recipe changes before settling on the recipe we know and love today.",
                "abv": "7.7",
                "ibu": "96",
                "glasswareId": 5,
                "srmId": 13,
                "availableId": 1,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/xKQpNS/upload_wrJWR4-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/xKQpNS/upload_wrJWR4-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/xKQpNS/upload_wrJWR4-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:27",
                "updateDate": "2018-06-04 19:07:27",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "srm": {
                    "id": 13,
                    "name": "13",
                    "hex": "CB6200"
                },
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "C6NbQo",
                "name": "Hyperion Bourbon Bellaire Brown",
                "nameDisplay": "Hyperion Bourbon Bellaire Brown",
                "description": "Hyperion Bourbon Bellaire Brown with coffee is Short’s Flagship Brown Ale, Bellaire Brown, fermented with Ethiopian Hambela Estate coffee from Hyperion Coffee Co. in Ypsilanti, MI and aged for 15 months in Old Forester bourbon barrels. Hyperion Bourbon Bellaire Brown pours a dark brown color with a thick, off-white head and has aromas and flavors of vanilla, oak, chocolate, bourbon, and a touch of coffee. A silky mouth feel and smooth finish is complemented by a slight warming of the tongue.",
                "abv": "9.3",
                "ibu": "20",
                "styleId": 37,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-01 14:17:41",
                "updateDate": "2017-10-01 14:17:41",
                "style": {
                    "id": 37,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Brown Ale",
                    "shortName": "American Brown",
                    "description": "American brown ales range from deep copper to brown in color. Roasted malt caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma. American brown ales have evident low to medium hop flavor and aroma, medium to high hop bitterness, and a medium body. Estery and fruity-ester characters should be subdued. Diacetyl should not be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "25",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "6.4",
                    "srmMin": "15",
                    "srmMax": "26",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:35"
                }
            },
            {
                "id": "qG8VWZ",
                "name": "I Got 5 On It",
                "nameDisplay": "I Got 5 On It",
                "description": "I Got 5 On It is a medium-bodied American India Pale Ale brewed with five different hop varieties.  A blend of earthy and sweet citrus hop aromas softy rise from this hop forward brew.  Bready malt and fruity tangerine flavors lead into a prominent bitterness that resonates pleasantly and finishes dry.",
                "abv": "7.6",
                "ibu": "85",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/qG8VWZ/upload_Butx2y-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/qG8VWZ/upload_Butx2y-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/qG8VWZ/upload_Butx2y-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-06-14 20:31:57",
                "updateDate": "2017-06-14 20:32:10",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "E6v8su",
                "name": "Imperial Spruce Pilsner",
                "nameDisplay": "Imperial Spruce Pilsner",
                "description": "India Spruce Pilsner is an Imperial Pilsner, fermented with local, hand-picked blue spruce tips. The spruce presence, rooted in historical brewing practices, is enormous and gives the beer a refreshing gin quality. This beer is impressively light bodied, considering the immense spruce flavors and the prodigious additions of hops.",
                "abv": "10.4",
                "ibu": "85",
                "styleId": 98,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/E6v8su/upload_0L3q51-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/E6v8su/upload_0L3q51-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/E6v8su/upload_0L3q51-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-08-09 15:10:35",
                "updateDate": "2016-08-09 15:22:56",
                "style": {
                    "id": 98,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Pilsener",
                    "shortName": "American Pilsener",
                    "description": "This classic and unique pre-Prohibition American-style Pilsener is straw to deep gold in color. Hop bitterness, flavor and aroma are medium to high, and use of noble-type hops for flavor and aroma is preferred. Up to 25 percent corn and/or rice in the grist should be used. Malt flavor and aroma are medium. This is a light-medium to medium-bodied beer. Sweet corn-like dimethylsulfide (DMS), fruity esters and American hop-derived citrus flavors or aromas should not be perceived. Diacetyl is not acceptable. There should be no chill haze. Competition organizers may wish to subcategorize this style into rice and corn subcategories.",
                    "ibuMin": "25",
                    "ibuMax": "40",
                    "abvMin": "5",
                    "abvMax": "6",
                    "srmMin": "3",
                    "srmMax": "6",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:40:08"
                }
            },
            {
                "id": "SET4Ko",
                "name": "Irish Red Ale",
                "nameDisplay": "Irish Red Ale",
                "description": "A Short’s version of a classic Irish-Style Red Ale. This deep ruby red colored gem offers a wonderful sweet soft malty nose w/ delicate toasted qualities and slight creamy notes. A medium bodied ale that focuses on flavorful toasted malt characteristics lending subtle caramel-like sweetness, along w/ an enjoyable effervescence, allowing for a clean and dry finish.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 22,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:30",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 22,
                    "categoryId": 2,
                    "category": {
                        "id": 2,
                        "name": "Irish Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Irish-Style Red Ale",
                    "shortName": "Irish Red",
                    "description": "Irish-style red ales range from light red-amber-copper to light brown in color. These ales have a medium hop bitterness and flavor. They often don't have hop aroma. Irish-style red ales have low to medium candy-like caramel malt sweetness and may have a balanced subtle degree of roast barley or roast malt character and complexity.  Irish-style Red Ales have a medium body. The style may have low levels of fruity-ester flavor and aroma. Diacetyl should be absent or at very low levels. Chill haze is allowable at cold temperatures. Slight yeast haze is acceptable for bottle-conditioned products.",
                    "ibuMin": "20",
                    "ibuMax": "28",
                    "abvMin": "4",
                    "abvMax": "4.5",
                    "srmMin": "11",
                    "srmMax": "18",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:23:38"
                }
            },
            {
                "id": "qnYhO7",
                "name": "Is This Love",
                "nameDisplay": "Is This Love",
                "abv": "4.8",
                "styleId": 127,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-08-02 18:35:37",
                "updateDate": "2015-08-03 15:45:19",
                "style": {
                    "id": 127,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Gluten-Free Beer",
                    "shortName": "Gluten-Free Beer",
                    "description": "A beer (lager, ale or other) that is made from fermentable sugars, grains and converted carbohydrates. Ingredients do not contain gluten, in other words zero gluten (No barley, wheat, spelt, oats, rye, etc). May or may not contain malted grains that do not contain gluten. Brewers may or may not design and identify these beers along other style guidelines with regard to flavor, aroma and appearance profile. The beer's overall balance and character should be based on its own merits and not necessarily compared with traditional styles of beer. In competitions, brewers identify ingredients and fermentation type. NOTE: These guidelines do not supersede any government regulations. Wine, mead, flavored malt beverages or beverages other than beer as defined by the TTB (U.S. Trade and Tax Bureau) are not considered \"gluten-free beer\" under these guidelines.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:45:08"
                }
            },
            {
                "id": "w8DFsV",
                "name": "It’s About Thyme",
                "nameDisplay": "It’s About Thyme",
                "description": "It’s About Thyme is a golden orange colored American India Pale Ale brewed with orange blossom honey and fresh thyme. Pungent dank hop aromas blend perfectly with some herbal, bready and light sweet citrus complements. Definitive thyme and fruity apricot-like hops flavors, lead into a sticky hop finish, with an enhanced dryness brought on by the honey’s higher degree of fermentability. The overall bitterness builds as it lingers on the palate. Like many other beers at Shorts, this beer was brewed in honor of commemorating a significant moment through its symbolic existence. It’s About Thyme celebrates the formation of Fermenta: A trade group initiated by Michigan women, committed to diversity, camaraderie, networking and education within the craft beverage industry. Six Short’s ladies brewed this beer along with our brewer.",
                "abv": "7.5",
                "ibu": "60",
                "styleId": 30,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:42:39",
                "updateDate": "2014-09-12 14:15:55",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "VR92XP",
                "name": "Juicy Tree",
                "nameDisplay": "Juicy Tree",
                "description": "Juicy Tree is an experimental India Pale Ale made with blue spruce tips, juniper berries, and cranberries. Big aromatics of piney evergreen and sweet berries tingle the senses. After some slightly tart and tangy flavors up front, a harmonious balance between pleasant fruit sweetness and bitter pine is reached. A sizable malt bill, ample hop additions, and the floral qualities of American Ale yeast, all contribute to symmetry of this beer.",
                "abv": "7.5",
                "ibu": "65",
                "availableId": 2,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/VR92XP/upload_op40uY-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/VR92XP/upload_op40uY-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/VR92XP/upload_op40uY-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:36:56",
                "updateDate": "2015-12-17 19:41:12",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "4YBsUF",
                "name": "Key Lime Pie",
                "nameDisplay": "Key Lime Pie",
                "description": "Key Lime Pie is made with fresh limes, milk sugar, graham cracker, and marshmallow fluff. The prominent flavors are immensely sweet, then tart, with subtle hints of graham cracker coming through in the nose and remain on the back of the palate. Key Lime Pie is a Gold Medal Winner at the 2010 Great American Beer Festival.",
                "abv": "5.5",
                "ibu": "21",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 130,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/4YBsUF/upload_sl2BrJ-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/4YBsUF/upload_sl2BrJ-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/4YBsUF/upload_sl2BrJ-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:33",
                "updateDate": "2015-12-16 14:01:47",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "grugdg",
                "name": "Kind Ale",
                "nameDisplay": "Kind Ale",
                "description": "Short’s Harvest Ale, a seasonal beer made each fall to celebrate a successful growing season. True to tradition, we commemorate the earth’s agricultural environment by using freshly picked hops to “wet hop” this brew. Straight from the fields on Old Mission Peninsula to our kettle, local hops impart a mellow earthiness to this ale that lead to moderate bitter tones and a subtle sweetness in the finish.",
                "abv": "5.5",
                "ibu": "56",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 26,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:33",
                "updateDate": "2017-01-10 21:38:56",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 26,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Fresh \"Wet\" Hop Ale",
                    "shortName": "Wet Hop Ale",
                    "description": "Any style of ale can be made into a fresh hop or wet hop version. These ales are hopped predominantly with fresh (newly harvested and kilned) and/or undried (“wet”) hops. These beers will exhibit especially aromas and flavors of green, almost chlorophyll-like or other fresh hop characters, in harmony with the characters of the base style of the beer. These beers may be aged and enjoyed after the initial “fresh-hop” character diminishes. Unique character from “aged” fresh hop beers may emerge, but they have yet to be defined.",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:25"
                }
            },
            {
                "id": "X3myRh",
                "name": "Kolsch 45",
                "nameDisplay": "Kolsch 45",
                "abv": "4.8",
                "styleId": 45,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 19:22:23",
                "updateDate": "2015-12-18 19:22:24",
                "style": {
                    "id": 45,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Kölsch / Köln-Style Kölsch",
                    "shortName": "Kölsch",
                    "description": "Kölsch is warm fermented and aged at cold temperatures (German ale or alt-style beer). Kölsch is characterized by a golden to straw color and a slightly dry, subtly sweet softness on the palate, yet crisp. Good, dense head retention is desirable. Light pearapple-Riesling wine-like fruitiness may be apparent, but is not necessary for this style. Caramel character should not be evident. The body is light to medium-light. This beer has low hop flavor and aroma with medium bitterness. Wheat can be used in brewing this beer. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold conditioning process. Fruity esters should be minimally perceived, if at all. Chill haze should be absent.",
                    "ibuMin": "18",
                    "ibuMax": "25",
                    "abvMin": "4.8",
                    "abvMax": "5.3",
                    "srmMin": "4",
                    "srmMax": "6",
                    "ogMin": "1.042",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:29:04"
                }
            },
            {
                "id": "rUSuCB",
                "name": "Kwikriek",
                "nameDisplay": "Kwikriek",
                "description": "Kwikriek is an American Sour Ale brewed with Northern Michigan cherries and Lactobacillus. This beer is bright pink in color and smells of fresh cherry. The beer is tart with a strong cherry flavor and finishes quite dry. Light in body with an appealing fruit flavor, Kwikriek is a very approachable sour beer.",
                "abv": "4.3",
                "ibu": "5",
                "styleId": 40,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-11 18:44:56",
                "updateDate": "2017-10-11 18:44:56",
                "style": {
                    "id": 40,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Sour Ale",
                    "shortName": "Sour",
                    "description": "American sour ales can be very light to black or take on the color of added fruits or other ingredients. There is no Brettanomyces character in this style of beer. Wood- and barrel-aged sour ales are classified elsewhere. If acidity is present it is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash or in fermentation by the use of various microorganisms including certain bacteria and yeasts. Acidic character can be a complex balance of several types of acid and characteristics of age. The evolution of natural acidity develops balanced complexity. Residual flavors that come from liquids previously aged in a barrel such as bourbon or sherry should not be present. Wood vessels may be used during the fermentation and aging process, but wood-derived flavors such as vanillin must not be present. In darker versions, roasted malt, caramel-like and chocolate-like characters should be subtle in both flavor and aroma. American sour may have evident full range of hop aroma and hop bitterness with a full range of body. Estery and fruity-ester characters are evident, sometimes moderate and sometimes intense, yet balanced. Diacetyl and sweet corn-like dimethylsulfide (DMS) should not be perceived. Chill haze, bacteria and yeast-induced haze are allowable at low to medium levels at any temperature. Fruited American-Style Sour Ales will exhibit fruit flavors in harmonious balance with other characters.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:32"
                }
            },
            {
                "id": "uEoefD",
                "name": "L-I-V-I-N",
                "nameDisplay": "L-I-V-I-N",
                "abv": "8.5",
                "styleId": 72,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-21 18:27:32",
                "updateDate": "2015-12-21 18:27:32",
                "style": {
                    "id": 72,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "French & Belgian-Style Saison",
                    "shortName": "Saison",
                    "description": "Beers in this category are golden to deep amber in color. There may be quite a variety of characters within this style. Generally: They are light to medium in body. Malt aroma is low to medium-low. Esters are medium to high in  aroma, while, complex alcohols, herbs, spices, low Brettanomyces character and even clove and smoke-like phenolics may or may not be evident in the overall balanced beer. Hop aroma and flavor may be at low to medium levels. Malt flavor is low but provides foundation for the overall balance. Hop bitterness is moderate to moderately assertive. Herb and/or spice flavors, including black pepper-like notes, may or may not be evident. Fruitiness from fermentation is generally in character. A balanced small amount of sour or acidic flavors is acceptable when in balance with other components. Earthy, cellar-like, musty aromas are okay. Diacetyl should not be perceived. Chill or slight yeast haze is okay. Often bottle conditioned with some yeast character and high carbonation. French & Belgian-Style Saison may have Brettanomyces characters that are slightly acidity, fruity, horsey, goaty and/or leather-like.",
                    "ibuMin": "20",
                    "ibuMax": "40",
                    "abvMin": "4.5",
                    "abvMax": "8.5",
                    "srmMin": "4",
                    "srmMax": "14",
                    "ogMin": "1.055",
                    "fgMin": "1.004",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:34:55"
                }
            },
            {
                "id": "VCM5cT",
                "name": "Lectronic Lager",
                "nameDisplay": "Lectronic Lager",
                "description": "Lectronic Lager is a light and refreshing beer brewed with traditional Bohemian lager yeast.  The beer pours as a pale yellow color and the soft aromas that accompany it are of doughy bread and a faint pear sweetness.  Flavors of hearty grain and yeast are followed by a medium bitterness before a relatively crisp clean finish.",
                "abv": "5",
                "ibu": "40",
                "styleId": 76,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:04:37",
                "updateDate": "2014-12-17 22:04:37",
                "style": {
                    "id": 76,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Bohemian-Style Pilsener",
                    "shortName": "Bohemian Pilsener",
                    "description": "Traditional Bohemian Pilseners are medium bodied, and they can be as dark as a light amber color. This style balances moderate bitterness and noble-type hop aroma and flavor with a malty, slightly sweet, medium body. Extremely low levels of diacetyl and low levels of sweet corn-like dimethylsulfide (DMS) character, if perceived, are characteristic of this style and both may accent malt aroma. A toasted-, biscuit-like, bready malt character along with low levels of sulfur compounds may be evident. There should be no chill haze. Its head should be dense and rich.",
                    "ibuMin": "30",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "5",
                    "srmMin": "3",
                    "srmMax": "7",
                    "ogMin": "1.044",
                    "fgMin": "1.014",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:36:03"
                }
            },
            {
                "id": "dgIpLs",
                "name": "Lemon Hop Stand",
                "nameDisplay": "Lemon Hop Stand",
                "description": "A light bodied bright yellow India Cream Ale with bready yeast aromas and subtle citrus scents. Flavors of sweet grain and soft lemon spice flow pleasantly into a decent sized grassy bitterness that lingers sufficiently on the pallet.",
                "abv": "7.8",
                "ibu": "70",
                "styleId": 109,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-03-16 17:48:01",
                "updateDate": "2018-03-21 18:16:49",
                "style": {
                    "id": 109,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Cream Ale or Lager",
                    "shortName": "Cream Ale",
                    "description": "Mild, pale, light-bodied ale, made using a warm fermentation (top or bottom) and cold lagering. Hop bitterness and flavor range from very low to low. Hop aroma is often absent. Sometimes referred to as cream ales, these beers are crisp and refreshing. Pale malt character predominates. Caramelized malt character should be absent. A fruity or estery aroma may be perceived. Diacetyl and chill haze should not be perceived. Sulfur character and/or sweet corn-like dimethylsulfide (DMS) should be extremely low or absent from this style of beer.",
                    "ibuMin": "10",
                    "ibuMax": "22",
                    "abvMin": "4.2",
                    "abvMax": "5.6",
                    "srmMin": "2",
                    "srmMax": "5",
                    "ogMin": "1.044",
                    "fgMin": "1.004",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:30"
                }
            },
            {
                "id": "64rPVa",
                "name": "Lil Wheezy",
                "nameDisplay": "Lil Wheezy",
                "description": "A light bodied, heavily hopped, amber colored ale with predominant hop aromas of tropical fruit with compliments of sweet malt. Falconer's Flight is comprised of many unique hop varieties including Citra®, Simcoe®, and Sorachi Ace along with experimental hops and numerous other varieties. There characteristics arr of dank earth, grassy, piney, and gentle citrus flavors are balanced slightly by some full grain attributes before a big bitter shock strikes the back of the pallet and resonates sharply.",
                "abv": "7",
                "styleId": 32,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/64rPVa/upload_OHsRO4-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/64rPVa/upload_OHsRO4-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/64rPVa/upload_OHsRO4-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-08 17:03:31",
                "updateDate": "2015-12-17 19:09:59",
                "style": {
                    "id": 32,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Amber/Red Ale",
                    "shortName": "Amber",
                    "description": "American amber/red ales range from light copper to light brown in color. They are characterized by American-variety hops used to produce the perception of medium hop bitterness, flavor, and medium aroma. Amber ales have medium-high to high maltiness with medium to low caramel character. They should have medium to medium-high body. The style may have low levels of fruityester flavor and aroma. Diacetyl can be either absent or barely perceived at very low levels. Chill haze is allowable at cold temperatures. Slight yeast haze is acceptable for bottle-conditioned products.",
                    "ibuMin": "30",
                    "ibuMax": "40",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "11",
                    "srmMax": "18",
                    "ogMin": "1.048",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:52"
                }
            },
            {
                "id": "iBjhRX",
                "name": "Local's",
                "nameDisplay": "Local's",
                "description": "Local’s is a light, yet very tasty lager. The light pilsen malt creates a soft and subtle flavor profile that finishes crisp and clean on your palate.\r\nLocal’s is a perfect beer for the seasoned craft brew enthusiast and someone new to microbrews. When Short’s first opened, there was not a great deal of craft beer available in the area. Joe Short wanted to create a beer that would appeal to new craft beer drinkers who lived in Michigan, and introduce them to the complex flavors of other beer styles.",
                "abv": "4",
                "ibu": "6",
                "glasswareId": 5,
                "availableId": 1,
                "styleId": 93,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/iBjhRX/upload_bFIV7C-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/iBjhRX/upload_bFIV7C-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/iBjhRX/upload_bFIV7C-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2015-12-16 13:20:14",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                },
                "style": {
                    "id": 93,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Lager",
                    "shortName": "American Lager",
                    "description": "Light in body and very light to straw in color, American lagers are very clean and crisp and aggressively carbonated. Flavor components should b e subtle and complex, with no one ingredient dominating the others. Malt sweetness is light to mild. Corn, rice, or other grain or sugar adjuncts are often used. Hop bitterness, flavor and aroma are negligible to very light. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "13",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:26"
                }
            },
            {
                "id": "7F5yvr",
                "name": "Local's Light",
                "nameDisplay": "Local's Light",
                "description": "Local’s Light is bright and golden American Style Lager. This beer is lightly hopped with low malt sweetness that finishes clean and crisp.",
                "abv": "5",
                "ibu": "11",
                "styleId": 93,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/7F5yvr/upload_frXEah-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/7F5yvr/upload_frXEah-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/7F5yvr/upload_frXEah-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-12 20:42:59",
                "updateDate": "2017-08-30 18:57:09",
                "style": {
                    "id": 93,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Lager",
                    "shortName": "American Lager",
                    "description": "Light in body and very light to straw in color, American lagers are very clean and crisp and aggressively carbonated. Flavor components should b e subtle and complex, with no one ingredient dominating the others. Malt sweetness is light to mild. Corn, rice, or other grain or sugar adjuncts are often used. Hop bitterness, flavor and aroma are negligible to very light. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "13",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:26"
                }
            },
            {
                "id": "KZhRcu",
                "name": "Local's Light",
                "nameDisplay": "Local's Light",
                "abv": "3.6",
                "styleId": 94,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-07-31 18:43:51",
                "updateDate": "2015-07-31 18:43:51",
                "style": {
                    "id": 94,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Light (Low Calorie) Lager",
                    "shortName": "American Light Lager",
                    "description": "These beers are extremely light colored, light in body, and high in carbonation. Calorie level should not exceed 125 per 12 ounce serving. Corn, rice, or other grain or sugar adjuncts are often used. Flavor is mild and hop bitterness and aroma is negligible to very low. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "10",
                    "abvMin": "3.5",
                    "abvMax": "4.4",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.024",
                    "fgMin": "1.002",
                    "fgMax": "1.008",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:35"
                }
            },
            {
                "id": "3Z4MQR",
                "name": "London Fog",
                "nameDisplay": "London Fog",
                "description": "Brewed with Earl Grey Tea.",
                "abv": "5.3",
                "styleId": 12,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-07-28 14:52:26",
                "updateDate": "2015-07-28 14:52:26",
                "style": {
                    "id": 12,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "English-Style Brown Ale",
                    "shortName": "English Brown",
                    "description": "English brown ales range from copper to brown in color. They have medium body and range from dry to sweet maltiness with very little hop flavor or aroma. Roast malt tones may sometimes contribute to the flavor and aroma profile. Low to medium-low levels of fruity-ester flavors are appropriate. Diacetyl should be very low, if evident. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "4",
                    "abvMax": "5.5",
                    "srmMin": "13",
                    "srmMax": "25",
                    "ogMin": "1.04",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:20:51"
                }
            },
            {
                "id": "vDe488",
                "name": "Love Knife",
                "nameDisplay": "Love Knife",
                "description": "Love Knife is a brownish red Belgian Amber Ale that explodes with aromas of strawberry, banana, and nectarine. Malt sweetness is quite low, allowing for the fruit flavors to be apparent throughout the beer.\r\n \r\nOne day while looking over popular Belgian beer styles, Tony noted that there were not a lot of Belgian Amber Ales. Therefore, he decided to create a recipe with a malt bill that loosely resembled a typical Amber Ale found in the states. By adding Belgian yeast and American hop varieties, Tony created a uniquely new and different hybridized style.",
                "abv": "6.7",
                "ibu": "45",
                "styleId": 32,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/vDe488/upload_nkdk2q-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/vDe488/upload_nkdk2q-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/vDe488/upload_nkdk2q-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-02-04 14:46:54",
                "updateDate": "2016-02-04 14:47:19",
                "style": {
                    "id": 32,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Amber/Red Ale",
                    "shortName": "Amber",
                    "description": "American amber/red ales range from light copper to light brown in color. They are characterized by American-variety hops used to produce the perception of medium hop bitterness, flavor, and medium aroma. Amber ales have medium-high to high maltiness with medium to low caramel character. They should have medium to medium-high body. The style may have low levels of fruityester flavor and aroma. Diacetyl can be either absent or barely perceived at very low levels. Chill haze is allowable at cold temperatures. Slight yeast haze is acceptable for bottle-conditioned products.",
                    "ibuMin": "30",
                    "ibuMax": "40",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "11",
                    "srmMax": "18",
                    "ogMin": "1.048",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:52"
                }
            },
            {
                "id": "rDWYjM",
                "name": "Machu Beechu",
                "nameDisplay": "Machu Beechu",
                "description": "Machu Beechu is a Double India Pale Ale brewed with passion fruit and honey in collaboration with Rare Bird Brewpub. At first sniff this beer is an explosion of tropical fruit scents from mango to pineapple. Brewed with Vic Secret, El Dorado, and Mandarina hops, this beer is super dank and juicy. Initial tart and fruity flavors are followed by a dry and slightly bitter finish. With final flavors of mouth-coating honey, this IPA is a must taste.",
                "abv": "8",
                "ibu": "60",
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/rDWYjM/upload_bwuBPX-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/rDWYjM/upload_bwuBPX-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/rDWYjM/upload_bwuBPX-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-02-01 16:46:34",
                "updateDate": "2018-02-01 16:48:05",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "W0CGou",
                "name": "Melt My Brain",
                "nameDisplay": "Melt My Brain",
                "description": "Melt My Brain is an experimental Golden Ale brewed with coriander, juniper berries, limes and blended with Short’s made tonic water to create the tasting experience of drinking a gin and tonic cocktail.  Delicate aromas of citrus and pine flow into bold bright flavors of sweet lime with a slightly sticky bitterness.  Refreshing and clean, a tasty tonic effervescence provokes the palate for more.",
                "abv": "6.1",
                "availableId": 2,
                "styleId": 130,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/W0CGou/upload_0fMEgB-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/W0CGou/upload_0fMEgB-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/W0CGou/upload_0fMEgB-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-06-30 16:55:48",
                "updateDate": "2016-06-01 18:35:44",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "4ZIYmG",
                "name": "Michigantuan",
                "nameDisplay": "Michigantuan",
                "description": "Michigantuan is a Double India Pale Ale brewed with Michigan grown hops and barley, along with cherry concentrate from King Orchards. This beer has an intriguing burgundy color with an abundance of dark fruit and green hop aromatics. Bold flavors of tart fruit, citrus rind and floral hops transition to a sizable bitterness that lingers well into the finish, drying the palate.",
                "abv": "8.8",
                "ibu": "80",
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/4ZIYmG/upload_wU6Hdh-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/4ZIYmG/upload_wU6Hdh-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/4ZIYmG/upload_wU6Hdh-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-27 13:24:04",
                "updateDate": "2017-09-27 13:25:05",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "F3LcaH",
                "name": "Midnight at the Dog Farm",
                "nameDisplay": "Midnight at the Dog Farm",
                "abv": "7.3",
                "styleId": 132,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-02 20:39:59",
                "updateDate": "2017-10-04 18:38:12",
                "style": {
                    "id": 132,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Wood- and Barrel-Aged Beer",
                    "shortName": "BBL Aged",
                    "description": "A wood- or barrel-aged beer is any lager, ale or hybrid beer, either a traditional style or a unique experimental beer that has been aged for a period of time in a wooden barrel or in contact with wood. This beer is aged with the intention of imparting the particularly unique character of the wood and/or what has previously been in the barrel. New wood character can be characterized as a complex blend of vanillin and/or other unique wood character, but wood aged is not necessarily synonymous with imparting wood-flavors. Used sherry, rum, bourbon, scotch, port, wine and other barrels are often used, imparting complexity and uniqueness to beer. Ultimately a balance of flavor, aroma and mouthfeel are sought with the marriage of new beer with wood and/or barrel flavors. Beers in this style may or may not have Brettanomyces character. Brewers when entering this category should specify type of barrel and/or wood used and any other special treatment or ingredients used. Competition managers may create style subcategories to differentiate between high alcohol and low alcohol beers and very dark and lighter colored beer as well as for fruit beers and non-fruit beers. Competitions may develop guidelines requesting brewers to specify what kind of wood (new or used oak, other wood varieties) and/or barrel (whiskey, port, sherry, wine, etc.) was used in the process. The brewer may be asked to explain the special nature (wood used, base beer style(s) and achieved character) of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:27"
                }
            },
            {
                "id": "sndjm7",
                "name": "Mmmkay",
                "nameDisplay": "Mmmkay",
                "abv": "5",
                "ibu": "85",
                "styleId": 93,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/sndjm7/upload_CEfcO5-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/sndjm7/upload_CEfcO5-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/sndjm7/upload_CEfcO5-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2013-08-02 23:19:52",
                "updateDate": "2015-12-17 04:09:00",
                "style": {
                    "id": 93,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Lager",
                    "shortName": "American Lager",
                    "description": "Light in body and very light to straw in color, American lagers are very clean and crisp and aggressively carbonated. Flavor components should b e subtle and complex, with no one ingredient dominating the others. Malt sweetness is light to mild. Corn, rice, or other grain or sugar adjuncts are often used. Hop bitterness, flavor and aroma are negligible to very light. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "13",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:26"
                }
            },
            {
                "id": "BD5NTM",
                "name": "Moher Stout",
                "nameDisplay": "Moher Stout",
                "description": "Moher Export Irish Imperial Stout is a medium bodied double Irish Stout, black in color with a rich brown lace.  Light roast malt and bitter cocoa aromas lead into a big creamy mocha flavor.  Aided by the addition of flaked oatmeal, the finish is exceptionally smooth, with only a slight dryness and moderate roasted bitterness.",
                "abv": "7",
                "ibu": "30",
                "availableId": 2,
                "styleId": 24,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/BD5NTM/upload_jcsecv-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/BD5NTM/upload_jcsecv-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/BD5NTM/upload_jcsecv-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:25:00",
                "updateDate": "2015-12-17 21:10:20",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 24,
                    "categoryId": 2,
                    "category": {
                        "id": 2,
                        "name": "Irish Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Foreign (Export)-Style Stout",
                    "shortName": "Export Stout",
                    "description": "As with classic dry stouts, foreign-style stouts have an initial malt sweetness and caramel flavor with a distinctive dry-roasted bitterness in the finish. Coffee-like roasted barley and roasted malt aromas are prominent. Some slight acidity is permissible and a medium- to full-bodied mouthfeel is appropriate. Bitterness may be high but the perception is often compromised by malt sweetness. Hop aroma and flavor should not be perceived. The perception of fruity esters is low. Diacetyl (butterscotch) should be negligible or not perceived. Head retention is excellent.",
                    "ibuMin": "30",
                    "ibuMax": "60",
                    "abvMin": "5.7",
                    "abvMax": "9.3",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.052",
                    "fgMin": "1.008",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:23:58"
                }
            },
            {
                "id": "yEfL9i",
                "name": "Mr. Fusion",
                "nameDisplay": "Mr. Fusion",
                "description": "Mr. Fusion is a green tea and mint infused India Pale Ale with light, sweet lemon-lime aromas.  Golden in color, this beer has bright green and grassy hop flavors followed by pleasant hints of mint.  A good sized bitterness builds and lingers, before ultimately finishing clean.",
                "abv": "5.6",
                "ibu": "71",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/yEfL9i/upload_qpvR6Q-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/yEfL9i/upload_qpvR6Q-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/yEfL9i/upload_qpvR6Q-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-05-09 15:27:05",
                "updateDate": "2016-05-09 15:28:07",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "sbeMcz",
                "name": "Mule Beer",
                "nameDisplay": "Mule Beer",
                "description": "Mule Beer is an Experimental American Style Cream Ale employing spice and fruit to emulate the classic cocktail known as The Moscow Mule. Brewed with lime and ginger root, this beer has a firm but refreshing spice quality from the ginger that works in harmony with the with the tart, yet slightly sweet citrus elements of lime.",
                "abv": "5.5",
                "ibu": "20",
                "styleId": 109,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/sbeMcz/upload_gtf7pb-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/sbeMcz/upload_gtf7pb-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/sbeMcz/upload_gtf7pb-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-12 20:40:33",
                "updateDate": "2017-08-30 19:18:16",
                "style": {
                    "id": 109,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Cream Ale or Lager",
                    "shortName": "Cream Ale",
                    "description": "Mild, pale, light-bodied ale, made using a warm fermentation (top or bottom) and cold lagering. Hop bitterness and flavor range from very low to low. Hop aroma is often absent. Sometimes referred to as cream ales, these beers are crisp and refreshing. Pale malt character predominates. Caramelized malt character should be absent. A fruity or estery aroma may be perceived. Diacetyl and chill haze should not be perceived. Sulfur character and/or sweet corn-like dimethylsulfide (DMS) should be extremely low or absent from this style of beer.",
                    "ibuMin": "10",
                    "ibuMax": "22",
                    "abvMin": "4.2",
                    "abvMax": "5.6",
                    "srmMin": "2",
                    "srmMax": "5",
                    "ogMin": "1.044",
                    "fgMin": "1.004",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:30"
                }
            },
            {
                "id": "JBdUn7",
                "name": "Mystery Stout",
                "nameDisplay": "Mystery Stout",
                "description": "An Imperial Oatmeal Stout brewed with cocoa and molasses. A slight bitterness from the cocoa is noticeable, but yields to the overall sweet profile of this beer. Allow it to warm, and the full spectrum of flavor is revealed. The mystery is how we managed to pack so many flavors into one beer!",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 43,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:45",
                "updateDate": "2017-01-10 21:39:07",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 43,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Imperial Stout",
                    "shortName": "American Imperial Stout",
                    "description": "Black in color, American-style imperial stouts typically have a high alcohol content. Generally characterized as very robust. The extremely rich malty flavor and aroma are balanced with assertive hopping and fruity-ester characteristics. Bitterness should be moderately high to very high and balanced with full sweet malt character. Roasted malt astringency and bitterness can be moderately perceived but should not overwhelm the overall character. Hop aroma is usually moderately-high to overwhelmingly hop-floral, -citrus or -herbal. Diacetyl (butterscotch) levels should be absent.",
                    "ibuMin": "50",
                    "ibuMax": "80",
                    "abvMin": "7",
                    "abvMax": "12",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.08",
                    "fgMin": "1.02",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:49"
                }
            },
            {
                "id": "lhEakZ",
                "name": "Nectar De La Vida",
                "nameDisplay": "Nectar De La Vida",
                "abv": "6.2",
                "styleId": 105,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 19:28:56",
                "updateDate": "2015-12-18 19:28:56",
                "style": {
                    "id": 105,
                    "categoryId": 9,
                    "category": {
                        "id": 9,
                        "name": "Other Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Australasian, Latin American or Tropical-Style Light Lager",
                    "shortName": "Tropical Light Lager",
                    "description": "Australasian or Tropical light lagers are very light in color and light bodied. They have no hop flavor or aroma, and hop bitterness is negligibly to moderately perceived. Sugar adjuncts are often used to lighten the body and flavor, sometimes contributing to a slight apple-like-like fruity ester. Sugar, corn, rice, and other cereal grains are used as an adjunct. Chill haze and diacetyl should be absent. Fruity esters should be very low.",
                    "ibuMin": "9",
                    "ibuMax": "18",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "5",
                    "ogMin": "1.038",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:08"
                }
            },
            {
                "id": "Q64YM9",
                "name": "Nicie Spicie",
                "nameDisplay": "Nicie Spicie",
                "description": "A Northern Michigan spiced wheat ale made with a 50/50 blend of malted barley and malted white wheat. Packed with fresh citrus zest, then spiced with coriander and a three peppercorn blend, this light bodied ale is complex yet scrumptious. Exemplified by its gorgeous golden color, this beer is crisp and refreshing.",
                "abv": "5",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 112,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/Q64YM9/upload_hiCA4P-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/Q64YM9/upload_hiCA4P-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/Q64YM9/upload_hiCA4P-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:46",
                "updateDate": "2017-01-10 21:39:15",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 112,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Light American Wheat Ale or Lager with Yeast",
                    "shortName": "Wheat Ale",
                    "description": "This beer can be made using either ale or lager yeast. It can be brewed with 30 to 75 percent wheat malt, and hop rates may be low to medium. Hop characters may be light to moderate in bitterness, flavor and aroma. Fruity-estery aroma and flavor are typical but at low levels however, phenolic, clove-like characteristics should not be perceived. Color is usually straw to light amber, and the body should be light to medium in character. Diacetyl should not be perceived. Because this style is served with yeast the character should portray a full yeasty mouthfeel and appear hazy to very cloudy. Chill haze is also acceptable. Yeast flavor and aroma should be low to medium but not overpowering the balance and character of malt and hops. These beers are typically served with the yeast in the bottle, and are cloudy when served.",
                    "ibuMin": "10",
                    "ibuMax": "35",
                    "abvMin": "3.5",
                    "abvMax": "5.5",
                    "srmMin": "4",
                    "srmMax": "10",
                    "ogMin": "1.036",
                    "fgMin": "1.006",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:48"
                }
            },
            {
                "id": "hUldZd",
                "name": "Night Wheeler",
                "nameDisplay": "Night Wheeler",
                "description": "A dark black lager with bold roasted malt and faint citrus hop aromas. Robust flavors of burnt cocoa and roasted espresso barely give way to some mild orange and grapefruit hoppiness. The light body and crisp nature of this unique lager allows for a showcased bitterness that seems delayed at first, but builds to a powerful finish.",
                "abv": "4",
                "ibu": "60",
                "styleId": 84,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/hUldZd/upload_SnGTQ3-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/hUldZd/upload_SnGTQ3-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/hUldZd/upload_SnGTQ3-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:41:23",
                "updateDate": "2015-12-17 16:52:58",
                "style": {
                    "id": 84,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Schwarzbier",
                    "shortName": "Schwarzbier",
                    "description": "These very dark brown to black beers have a mild roasted malt character without the associated bitterness. This is not a fullbodied beer, but rather a moderate body gently enhances malt flavor and aroma with low to moderate levels of sweetness. Hop bitterness is low to medium in character. Noble-type hop flavor and aroma should be low but perceptible. There should be no fruity esters. Diacetyl should not be perceived.",
                    "ibuMin": "22",
                    "ibuMax": "30",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "25",
                    "srmMax": "30",
                    "ogMin": "1.044",
                    "fgMin": "1.01",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:37:12"
                }
            },
            {
                "id": "vVMIwY",
                "name": "Noble Chaos",
                "nameDisplay": "Noble Chaos",
                "description": "A German style Marzen (Oktoberfest) that is brewed in March, but released in mid September to allow this amber lager to mature fully for optimal drinking enjoyment. A subtle hop bouquet and toasted caramel malt flavors create a well balanced beer that finishes fresh and clean. With a pleasant nose and medium body, this brew is a taste of the season.",
                "abv": "5.75",
                "ibu": "40",
                "glasswareId": 3,
                "availableId": 4,
                "styleId": 81,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/vVMIwY/upload_YeBw70-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/vVMIwY/upload_YeBw70-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/vVMIwY/upload_YeBw70-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:46",
                "updateDate": "2015-12-16 13:20:31",
                "glass": {
                    "id": 3,
                    "name": "Mug",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 81,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Märzen",
                    "shortName": "Märzen",
                    "description": "Märzens are characterized by a medium body and broad range of color. They can range from golden to reddish orange. Sweet maltiness should dominate slightly over a clean hop bitterness. Malt character should be light-toasted rather than strongly caramel (though a low level of light caramel character is acceptable). Bread or biscuit-like malt character is acceptable in aroma and flavor. Hop aroma and flavor should be low but notable. Ale-like fruity esters should not be perceived. Diacetyl and chill haze should not be perceived.",
                    "ibuMin": "18",
                    "ibuMax": "25",
                    "abvMin": "5.3",
                    "abvMax": "5.9",
                    "srmMin": "4",
                    "srmMax": "15",
                    "ogMin": "1.05",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:36:50"
                }
            },
            {
                "id": "oiSpca",
                "name": "Not Unofficial",
                "nameDisplay": "Not Unofficial",
                "description": "Not Unofficial is a dry-hopped India Pale Ale brewed with mango and guava in collaboration with Lagunitas Brewing Company. Golden in color and slightly hazy, this beer has huge aromas of juicy tropical fruit. Medium bodied this beer leads with flavors of mango and guava before finishing with dank hoppiness.",
                "abv": "6.1",
                "ibu": "65",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/oiSpca/upload_RXiaKf-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/oiSpca/upload_RXiaKf-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/oiSpca/upload_RXiaKf-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-04-12 18:21:44",
                "updateDate": "2018-04-12 18:23:03",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "3LuRvS",
                "name": "Obliviate",
                "nameDisplay": "Obliviate",
                "description": "A caramel brown colored Belgian Quadrupel with fruity aromatics of black cherry and apple, alongside the looming presence of notable alcohol vapors. Sweet brown sugar and hard candy flavors transition quickly to a warming, almost hot mouthfeel. The lingering effect of the high alcoholic strength creates a slight dryness, as if it evaporates from the palate.",
                "abv": "12.3",
                "ibu": "40",
                "styleId": 60,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/3LuRvS/upload_kC88WX-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/3LuRvS/upload_kC88WX-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/3LuRvS/upload_kC88WX-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-04-12 18:26:32",
                "updateDate": "2018-04-12 18:27:04",
                "style": {
                    "id": 60,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Quadrupel",
                    "shortName": "Belgian Quad",
                    "description": "Quadrupels or \"Quads\" are characterized by the immense presence of alcohol and balanced flavor, bitterness and aromas. Its color is deep amber to rich chestnut/garnet brown. Often characterized by a mousse-like dense, sometimes amber head will top off a properly poured and served quad. Complex fruity aroma and flavor emerge reminiscent of raisins, dates, figs, grapes, plums often accompanied with a hint of winy character. Caramel, dark sugar and malty sweet flavors and aromas can be intense, not cloying, while complementing fruitiness. Though well attenuated it usually has a full, creamy body. Hop characters do not dominate; low to low-medium bitterness is perceived. Perception of alcohol can be extreme. Clove-like phenolic flavor and aroma should not be evident. Chill haze is acceptable at low serving temperatures. Diacetyl and DMS should not be perceived. Well balanced with savoring/sipping drinkability. Oxidative character if evident in aged Quads should be mild and pleasant.",
                    "ibuMin": "25",
                    "ibuMax": "50",
                    "abvMin": "9",
                    "abvMax": "14",
                    "srmMin": "8",
                    "srmMax": "20",
                    "ogMin": "1.084",
                    "fgMin": "1.014",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:33:33"
                }
            },
            {
                "id": "gVBcRj",
                "name": "Ocho De Mayo",
                "nameDisplay": "Ocho De Mayo",
                "abv": "4",
                "styleId": 47,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-05-31 13:00:39",
                "updateDate": "2016-05-31 13:00:39",
                "style": {
                    "id": 47,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Leipzig-Style Gose",
                    "shortName": "Leipzig Gose",
                    "description": "Traditional examples of Gose are spontaneously fermented, similarly to Belgian-style gueuze/lambic beers, and should exhibit complexity of acidic, flavor and aroma contributed by introduction of wild yeast and bacteria into the fermentation. A primary difference between Belgian Gueuze and German Gose is that Gose is served at a much younger age. Gose is typically pale gold to pale amber in color and typically contains malted barley, unmalted wheat with some traditional varieties containing oats. Hop character and malt flavors and aromas are negligible. Lemony or other citrus-like qualities are often present in aroma and on the palate. Some versions may have the spicy character of added coriander in aroma and on the palate at low to medium levels. Salt (table salt) character is also traditional in low amounts. Horsey, leathery, earthy aroma and flavors contributed by Brettanomyces yeasts may be evident but have a very low profile, as this beer is not excessively aged. Modern German Gose breweries typically introduce only pure beer yeast strains for fermentation. Low to medium lactic acid character is evident in all examples as sharp, refreshing sourness. Gose is typically enjoyed fresh, carbonated, and cloudy/hazy with yeast character, and may have evidence of continued fermentation activity. Overall complexity of flavors and aromas are sought while maintaining an ideal balance between acidity, yeast-enhanced spice and refreshment is ideal.",
                    "ibuMin": "10",
                    "ibuMax": "15",
                    "abvMin": "4.4",
                    "abvMax": "5.4",
                    "srmMin": "3",
                    "srmMax": "9",
                    "ogMin": "1.036",
                    "fgMin": "1.008",
                    "fgMax": "1.012",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 17:09:35"
                }
            },
            {
                "id": "7W4tlu",
                "name": "Old Baldy",
                "nameDisplay": "Old Baldy",
                "description": "Old Baldy is an Experimental Belgian Golden Ale with a hazy yellow glow and aromatic complexities of fruity bubblegum, spice, and banana taffy.  A snappy ginger bite complement the fresh citrus flavors of tangerine. The beer also has a wonderful cleansing effervescence.  The finish contains a notable bitterness that’s slightly peppery and dry.",
                "abv": "6",
                "ibu": "40",
                "styleId": 61,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/7W4tlu/upload_EjafFN-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/7W4tlu/upload_EjafFN-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/7W4tlu/upload_EjafFN-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:17:31",
                "updateDate": "2015-12-17 20:39:18",
                "style": {
                    "id": 61,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Blonde Ale",
                    "shortName": "Belgian Blonde",
                    "description": "Belgian-style blond ales are characterized by low yet evident hop bitterness, flavor, and sometimes aroma. Light to medium body and low malt aroma with a sweet, spiced and a low to medium fruity-ester character orchestrated in flavor and aroma. Sugar may be used to lighten perceived body. They are blonde to golden in color. Noble-type hops are commonly used. Low levels of phenolic spiciness from yeast byproducts may be perceived. Diacetyl should not be perceived. Acidic character should not be present. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "15",
                    "ibuMax": "30",
                    "abvMin": "6",
                    "abvMax": "7.8",
                    "srmMin": "4",
                    "srmMax": "7",
                    "ogMin": "1.054",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:01"
                }
            },
            {
                "id": "QTNwiB",
                "name": "Old Reliable",
                "nameDisplay": "Old Reliable",
                "abv": "4.9",
                "styleId": 98,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-06-14 16:47:10",
                "updateDate": "2016-06-14 16:47:10",
                "style": {
                    "id": 98,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Pilsener",
                    "shortName": "American Pilsener",
                    "description": "This classic and unique pre-Prohibition American-style Pilsener is straw to deep gold in color. Hop bitterness, flavor and aroma are medium to high, and use of noble-type hops for flavor and aroma is preferred. Up to 25 percent corn and/or rice in the grist should be used. Malt flavor and aroma are medium. This is a light-medium to medium-bodied beer. Sweet corn-like dimethylsulfide (DMS), fruity esters and American hop-derived citrus flavors or aromas should not be perceived. Diacetyl is not acceptable. There should be no chill haze. Competition organizers may wish to subcategorize this style into rice and corn subcategories.",
                    "ibuMin": "25",
                    "ibuMax": "40",
                    "abvMin": "5",
                    "abvMax": "6",
                    "srmMin": "3",
                    "srmMax": "6",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:40:08"
                }
            },
            {
                "id": "QaLCnA",
                "name": "Pan Galactic Gargle Blaster",
                "nameDisplay": "Pan Galactic Gargle Blaster",
                "description": "Pan Galactic Gargle Blaster is a Double Belgian IPA brewed exclusively with Galaxy hops. The nose is an impressive blend of citrus aromas reminiscent of guava and lemons. It also has subtle Belgian yeast esters. An intense, clean bitterness dominates the flavor profile, with little malt character to compete with. A fruity yeast sweetness is noticeable in the finish, alongside a resounding bitterness.",
                "abv": "8.4",
                "ibu": "90",
                "availableId": 2,
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/QaLCnA/upload_5w29pf-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/QaLCnA/upload_5w29pf-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/QaLCnA/upload_5w29pf-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-08-27 18:08:45",
                "updateDate": "2015-12-17 16:25:16",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "rEZGzE",
                "name": "Pandemonium Pale Ale",
                "nameDisplay": "Pandemonium Pale Ale",
                "description": "Pandemonium Pale Ale is a coppered-colored American Pale Ale that lends its hue to hearty two-row malts and hand selected specialty grains. Glacier hops balance the malt character, resulting in bountiful hoppy flavors and earthy aromas. Behold the bitter hysteria! - See more at: http://www.shortsbrewing.com/beers/pandemonium-pale-ale/#sthash.dgfpA8a7.dpuf",
                "abv": "6.4",
                "ibu": "36",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 25,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/rEZGzE/upload_BJulY0-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/rEZGzE/upload_BJulY0-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/rEZGzE/upload_BJulY0-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:55",
                "updateDate": "2015-12-16 12:57:17",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "jpnhvL",
                "name": "Papa Burgundy",
                "nameDisplay": "Papa Burgundy",
                "description": "Papa Burgundy is a caramel colored German Dunkel-Weise with delectable aromatics of banana and warm toffee. Big flavors of nutmeg and clove fuse with a malty sweetness to create a tasting experience reminiscent of homemade banana bread, with a candied fruit, spice, and nuttiness throughout.",
                "abv": "5.5",
                "styleId": 52,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:20:41",
                "updateDate": "2014-12-17 22:20:42",
                "style": {
                    "id": 52,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "South German-Style Dunkel Weizen / Dunkel Weissbier",
                    "shortName": "Dunkelweizen",
                    "description": "This beer style is characterized by a distinct sweet maltiness and a chocolate-like character from roasted malt. Estery and phenolic elements of this Weissbier should be evident but subdued. Color can range from copper-brown to dark brown. Dunkel Weissbier is well attenuated and very highly carbonated, and hop bitterness is low. Hop flavor and aroma are absent. Usually dark barley malts are used in conjunction with dark cara or color malts, and the percentage of wheat malt is at least 50 percent. If this is served with yeast, the beer may be appropriately very cloudy. No diacetyl should be perceived.",
                    "ibuMin": "10",
                    "ibuMax": "15",
                    "abvMin": "4.8",
                    "abvMax": "5.4",
                    "srmMin": "10",
                    "srmMax": "19",
                    "ogMin": "1.048",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:30:09"
                }
            },
            {
                "id": "HJ8c1P",
                "name": "Passport Pale Ale",
                "nameDisplay": "Passport Pale Ale",
                "description": "Passport Pale Ale is brewed exclusively for Magnum Hospitality with Magnum hops. Passport Pale Ale has a light copper hue and is slightly hazy. The aromas and flavors of this Ale are an enticing blend of toasted grains and citrus. Passport Pale Ale is easy drinking and well balanced.",
                "abv": "4",
                "ibu": "75",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-11 20:45:25",
                "updateDate": "2017-10-11 20:45:25",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "cOvJgn",
                "name": "Paterfamilias",
                "nameDisplay": "Paterfamilias",
                "description": "Belgian Session Ale blended with hard cider",
                "abv": "3.5",
                "styleId": 61,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-10-23 04:11:26",
                "updateDate": "2014-10-30 18:11:21",
                "style": {
                    "id": 61,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Blonde Ale",
                    "shortName": "Belgian Blonde",
                    "description": "Belgian-style blond ales are characterized by low yet evident hop bitterness, flavor, and sometimes aroma. Light to medium body and low malt aroma with a sweet, spiced and a low to medium fruity-ester character orchestrated in flavor and aroma. Sugar may be used to lighten perceived body. They are blonde to golden in color. Noble-type hops are commonly used. Low levels of phenolic spiciness from yeast byproducts may be perceived. Diacetyl should not be perceived. Acidic character should not be present. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "15",
                    "ibuMax": "30",
                    "abvMin": "6",
                    "abvMax": "7.8",
                    "srmMin": "4",
                    "srmMax": "7",
                    "ogMin": "1.054",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:01"
                }
            },
            {
                "id": "Xp575z",
                "name": "PB & J Stout",
                "nameDisplay": "PB & J Stout",
                "description": "Available any time Über Goober is on tap. Take our popular Peanut Oatmeal Stout and blend it with our famous flagship fruit infused rye ale, The Soft Parade, and viola! The result, a liquid version of a peanut butter and jelly sandwich. This customer created concoction can also be found in bottles as a limited specialty release.",
                "abv": "8",
                "ibu": "30",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 125,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:55",
                "updateDate": "2018-05-17 15:40:12",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "3Ru8V1",
                "name": "Peachy Pom Pom",
                "nameDisplay": "Peachy Pom Pom",
                "description": "Peachy Pom Pom is a rosy colored American Sour Ale brewed with peaches and pomegranate. Tart berry aromatics accentuate the sweet and sour fruit flavors within this beer.  A huge tangy mouth feel with some initial acidic citrus qualities shocks the palate before a clean and lightly dry finish.",
                "abv": "7",
                "ibu": "2",
                "styleId": 40,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/3Ru8V1/upload_n5zKCg-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/3Ru8V1/upload_n5zKCg-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/3Ru8V1/upload_n5zKCg-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-02-01 14:33:34",
                "updateDate": "2016-02-01 14:37:08",
                "style": {
                    "id": 40,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Sour Ale",
                    "shortName": "Sour",
                    "description": "American sour ales can be very light to black or take on the color of added fruits or other ingredients. There is no Brettanomyces character in this style of beer. Wood- and barrel-aged sour ales are classified elsewhere. If acidity is present it is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash or in fermentation by the use of various microorganisms including certain bacteria and yeasts. Acidic character can be a complex balance of several types of acid and characteristics of age. The evolution of natural acidity develops balanced complexity. Residual flavors that come from liquids previously aged in a barrel such as bourbon or sherry should not be present. Wood vessels may be used during the fermentation and aging process, but wood-derived flavors such as vanillin must not be present. In darker versions, roasted malt, caramel-like and chocolate-like characters should be subtle in both flavor and aroma. American sour may have evident full range of hop aroma and hop bitterness with a full range of body. Estery and fruity-ester characters are evident, sometimes moderate and sometimes intense, yet balanced. Diacetyl and sweet corn-like dimethylsulfide (DMS) should not be perceived. Chill haze, bacteria and yeast-induced haze are allowable at low to medium levels at any temperature. Fruited American-Style Sour Ales will exhibit fruit flavors in harmonious balance with other characters.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:32"
                }
            },
            {
                "id": "zOERfS",
                "name": "Pekko",
                "nameDisplay": "Pekko",
                "description": "Pekko is a single malt pale ale brewed exclusively with Pekko hops, that provide some lemon and lime citrus scents.  Very light bodied, with low malt sweetness, herbal hop flavors and a pleasant bitterness are prominent in this beer.",
                "abv": "5.7",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-04-15 15:35:08",
                "updateDate": "2016-04-15 15:35:08",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "9JToPd",
                "name": "Pig Bird",
                "nameDisplay": "Pig Bird",
                "description": "Pig Bird is a Belgian IPA with delicate esters of banana and spice. An initial caramel malty sweetness is matched nicely by fruity American hop flavors, leading into a prominent resiny bitterness.",
                "abv": "7.7",
                "styleId": 63,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/9JToPd/upload_0HOyMD-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/9JToPd/upload_0HOyMD-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/9JToPd/upload_0HOyMD-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-20 20:07:04",
                "updateDate": "2015-12-17 20:24:04",
                "style": {
                    "id": 63,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Pale Strong Ale",
                    "shortName": "Belgian Pale Strong",
                    "description": "Belgian pale strong ales are pale to golden in color with relatively light body for a beer of its alcoholic strength. Often brewed with light colored Belgian \"candy\" sugar, these beers are well attenuated. The perception of hop bitterness is medium-low to medium -high, with hop flavor and aroma also in this range. These beers are highly attenuated and have a perceptively deceiving high alcoholic character-being light to medium bodied rather than full bodied. The intensity of malt character should be low to medium, often surviving along with a complex fruitiness. Very little or no diacetyl is perceived. Herbs and spices are sometimes used to delicately flavor these strong ales. Low levels of phenolic spiciness from yeast byproducts may also be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "20",
                    "ibuMax": "50",
                    "abvMin": "7",
                    "abvMax": "11",
                    "srmMin": "4",
                    "srmMax": "10",
                    "ogMin": "1.064",
                    "fgMin": "1.012",
                    "fgMax": "1.024",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:16"
                }
            },
            {
                "id": "A8nvFQ",
                "name": "Pistachio Cream Ale",
                "nameDisplay": "Pistachio Cream Ale",
                "description": "Copious amounts of California pistachios added to this beer create a wonderful combination of flavors, and give the expression “slightly nutty” a whole new meaning. The aroma of pistachios blended with sweet malty fragrance is intoxicating and exciting. The sweet smooth whipped cream flavors seem to coat the toasted pistachio goodness, lending a sense of fullness uncommon to lighter golden ales.",
                "abv": "5",
                "ibu": "10",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 125,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:58",
                "updateDate": "2015-07-01 14:05:44",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "mSWjOr",
                "name": "Plum Rye Bock",
                "nameDisplay": "Plum Rye Bock",
                "description": "A medium bodied amber lager with a light mahogany color and mild toasted malt character. Fresh local plums and 20% malted rye yeild sweet toffee flavors initially, followed by subtle tart plum qualities that lead to a slightly bitter and dry finish.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 88,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:58",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 88,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Traditional German-Style Bock",
                    "shortName": "Bock",
                    "description": "Traditional bocks are made with all malt and are strong, malty, medium- to full-bodied, bottom-fermented beers with moderate hop bitterness that should increase proportionately with the starting gravity. Malt character should be a balance of sweetness and toasted/nut-like malt; not caramel. Hop flavor should be low and hop aroma should be very low. Bocks can range in color from deep copper to dark brown. Fruity esters should be minimal. Diacetyl should be absent.",
                    "ibuMin": "20",
                    "ibuMax": "30",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "20",
                    "srmMax": "30",
                    "ogMin": "1.066",
                    "fgMin": "1.018",
                    "fgMax": "1.024",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:38:54"
                }
            },
            {
                "id": "TYI58f",
                "name": "Pontius Road Pilsner",
                "nameDisplay": "Pontius Road Pilsner",
                "description": "Pontius Road Pilsner is a true American-style Pilsner. The beer contains malted barley blended with flaked maize (corn) to produce a deliciously distinct flavor. It may be light in body and color, but it has a full flavor that gives way to a refreshing crispness. Handfuls of hops provide pleasant aromas and a wonderful dry finish.\r\n \r\nPontius Road Pilsner was designed to be like a colonial Pilsner. In colonial times, ingredients were often rationed so people used corn to supplement some of the barley in their beer recipes. Their Pilsners were also slightly hopped to add preservation. The name, Pontius Road Pilsner, derives from the road name that The Woodmaster, Bill Sohn, lives on. Bill encouraged Joe Short to move forward with his brewery business plan, and is a big reason why Short’s exists today!",
                "abv": "5.2",
                "ibu": "18",
                "glasswareId": 4,
                "availableId": 2,
                "styleId": 98,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/TYI58f/upload_rqvwDm-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/TYI58f/upload_rqvwDm-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/TYI58f/upload_rqvwDm-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:43:59",
                "updateDate": "2015-12-16 13:21:15",
                "glass": {
                    "id": 4,
                    "name": "Pilsner",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 98,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Pilsener",
                    "shortName": "American Pilsener",
                    "description": "This classic and unique pre-Prohibition American-style Pilsener is straw to deep gold in color. Hop bitterness, flavor and aroma are medium to high, and use of noble-type hops for flavor and aroma is preferred. Up to 25 percent corn and/or rice in the grist should be used. Malt flavor and aroma are medium. This is a light-medium to medium-bodied beer. Sweet corn-like dimethylsulfide (DMS), fruity esters and American hop-derived citrus flavors or aromas should not be perceived. Diacetyl is not acceptable. There should be no chill haze. Competition organizers may wish to subcategorize this style into rice and corn subcategories.",
                    "ibuMin": "25",
                    "ibuMax": "40",
                    "abvMin": "5",
                    "abvMax": "6",
                    "srmMin": "3",
                    "srmMax": "6",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:40:08"
                }
            },
            {
                "id": "9H4se5",
                "name": "Power of Love",
                "nameDisplay": "Power of Love",
                "description": "Power of love is a truly unique pink colored Shandy created by blending Northwoods Gourmet Lemonade and a Wheat Ale brewed with raspberry and rosemary.  Enticing sugary aromas intermix with appealing citrus and herb fragrances.  Very sweet lemon flavors give way to a shock of tart raspberry before turning sharply dry, as if eating a piece of grapefruit.  The finish is somewhat bitter and spicy, but quite satisfying.",
                "styleId": 119,
                "isOrganic": "Y",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/9H4se5/upload_hCCMv7-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/9H4se5/upload_hCCMv7-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/9H4se5/upload_hCCMv7-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-07-29 18:54:20",
                "updateDate": "2018-06-04 19:01:02",
                "style": {
                    "id": 119,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Fruit Beer",
                    "shortName": "Fruit Beer",
                    "description": "Fruit beers are any beers using fruit or fruit extracts as an adjunct in either, the mash, kettle, primary or secondary fermentation providing obvious (ranging from subtle to intense), yet harmonious, fruit qualities. Fruit qualities should not be overpowered by hop character. If a fruit (such as juniper berry) has an herbal or spice quality, it is more appropriate to consider it in the herb and spice beers category. Acidic bacterial (not wild yeast) fermentation characters may be evident (but not necessary) they would contribute to acidity and enhance fruity balance. Clear or hazy beer is acceptable in appearance. A statement by the brewer explaining what fruits are used is essential in order for fair assessment in competitions. If this beer is a classic style with fruit, the brewer should also specify the classic style.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:21"
                }
            },
            {
                "id": "M9WW4m",
                "name": "Pretty Vacant",
                "nameDisplay": "Pretty Vacant",
                "description": "Pretty Vacant is a very light bodied Extra Pale Ale, yellow in color, with a quick burst of bitter hop fragrances.  Malt flavor and sweetness is perceivably low, while some grassy and peppery hop flavors stand out. The finish is relatively clean except for a dandelion-like bitterness that lingers into the finish.",
                "abv": "4.5",
                "styleId": 29,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:23:13",
                "updateDate": "2014-12-17 22:23:13",
                "style": {
                    "id": 29,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Strong Pale Ale",
                    "shortName": "American Strong Pale",
                    "description": "American strong pale ales range from deep golden to copper in color. The style is characterized by floral and citrus-like American-variety hops used to produce high hop bitterness, flavor, and aroma. Note that floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character is the perceived end, but may be a result of the skillful use of hops of other national origins. American strong pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruityester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "40",
                    "ibuMax": "50",
                    "abvMin": "5.5",
                    "abvMax": "6.3",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.05",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:32"
                }
            },
            {
                "id": "qXbD7K",
                "name": "Prolonged Enjoyment",
                "nameDisplay": "Prolonged Enjoyment",
                "description": "Prolonged Enjoyment has a huge earthy hop fragrance of green grass, pine, and citrus fruit. The sweet, malty flavors are met quickly by zesty American hop characteristics. This India Pale Ale has a light body but is packed with extraordinary bitterness. Bitter qualities linger briefly, before the mildly dry finish takes over.",
                "abv": "3.5",
                "ibu": "47",
                "availableId": 1,
                "styleId": 108,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/qXbD7K/upload_dCNrzj-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/qXbD7K/upload_dCNrzj-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/qXbD7K/upload_dCNrzj-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:39:36",
                "updateDate": "2015-12-17 18:01:06",
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                },
                "style": {
                    "id": 108,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Session Beer",
                    "shortName": "Session",
                    "description": "Any style of beer can be made lower in strength than described in the classic style guidelines. The goal should be to reach a balance between the style's character and the lower alcohol content. Drinkability is a character in the overall balance of these beers. Beers in this category must not exceed 4.1% alcohol by weight (5.1% alcohol by volume).",
                    "ibuMin": "10",
                    "ibuMax": "30",
                    "abvMin": "4",
                    "abvMax": "5.1",
                    "srmMin": "2",
                    "srmMax": "2",
                    "ogMin": "1.034",
                    "fgMin": "1.004",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:20"
                }
            },
            {
                "id": "J72b6S",
                "name": "Psychedelic Cat Grass",
                "nameDisplay": "Psychedelic Cat Grass",
                "abv": "9",
                "styleId": 31,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-04-20 18:31:04",
                "updateDate": "2016-04-20 18:31:04",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "j60mb1",
                "name": "Publican Porter",
                "nameDisplay": "Publican Porter",
                "description": "A full flavored Imperial porter with a black opaque color that leaves a pleasing dark brown lace. Rich decadent aromas of espresso entice the senses, eventually giving way to some subtle biscuit like tones. The use of heavily roasted malt imparts tasty flavors of burnt coffee and dark chocolate accompanied by a suitable and pleasant bitterness.",
                "abv": "9.15",
                "ibu": "77",
                "styleId": 158,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/j60mb1/upload_mO3mRK-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/j60mb1/upload_mO3mRK-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/j60mb1/upload_mO3mRK-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-03-13 01:31:30",
                "updateDate": "2015-12-17 10:53:03",
                "style": {
                    "id": 158,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Imperial Porter",
                    "shortName": "American Imperial Porter",
                    "description": "American-style imperial porters are black in color. No roast barley or strong burnt/astringent black malt character should be perceived. Medium malt, caramel and cocoa-like sweetness. Hop bitterness is perceived at a medium-low to medium level. Hop flavor and aroma may vary from being low to medium-high. This is a full bodied beer. Ale-like fruity esters should be evident but not overpowering and compliment hop character and malt derived sweetness. Diacetyl (butterscotch) levels should be absent.",
                    "ibuMin": "35",
                    "ibuMax": "50",
                    "abvMin": "5.5",
                    "abvMax": "9.5",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.08",
                    "ogMax": "1.1",
                    "fgMin": "1.02",
                    "fgMax": "1.03",
                    "createDate": "2013-08-10 12:42:51",
                    "updateDate": "2015-04-07 15:49:32"
                }
            },
            {
                "id": "6YQ1eq",
                "name": "Pumpkincrusha",
                "nameDisplay": "Pumpkincrusha",
                "description": "PUMPKINCRUSHA is a seasonal blend of pumpkin, spices, and carefully formulated ale. Brewed with real pumpkin and spice, aromas of pumpkin pie are apparent. The ale is a light pastel brown and comfortably consumable.",
                "abv": "6",
                "ibu": "35",
                "availableId": 4,
                "styleId": 121,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/6YQ1eq/upload_kzvMgw-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/6YQ1eq/upload_kzvMgw-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/6YQ1eq/upload_kzvMgw-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-09-28 14:32:48",
                "updateDate": "2015-12-18 05:47:24",
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 121,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Pumpkin Beer",
                    "shortName": "Pumpkin Beer",
                    "description": "Pumpkin beers are any beers using pumpkins (Cucurbito pepo) as an adjunct in either mash, kettle, primary or secondary fermentation, providing obvious (ranging from subtle to intense), yet harmonious, qualities. Pumpkin qualities should not be overpowered by hop character. These may or may not be spiced or flavored with other things. A statement by the brewer explaining the nature of the beer is essential for fair assessment in competitions. If this beer is a classic style with pumpkin, the brewer should also specify the classic style.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:28"
                }
            },
            {
                "id": "OmFXYI",
                "name": "Purple Rain",
                "nameDisplay": "Purple Rain",
                "description": "A light lager made in the summer time, when adding fruit to our seasonal brews is a common practice. Real blueberries give this beer a radiant purple glow, along with some poignant sweet and tart flavors. A delightful effervescence adds to the cool quenching nature of this beer, making it perfect for those warmer times of the year.",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 114,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/OmFXYI/upload_5CUMOB-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/OmFXYI/upload_5CUMOB-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/OmFXYI/upload_5CUMOB-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:01",
                "updateDate": "2015-12-16 12:29:26",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 114,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Fruit Wheat Ale or Lager with or without Yeast",
                    "shortName": "Fruit Wheat Ale",
                    "description": "This beer can be made using either ale or lager yeast. It can be brewed with 30 to 75 percent malted wheat. Fruit or fruit extracts contribute flavor and/or aroma. Perceived fruit qualities should be authentic and replicate true fruit complexity as much as possible. Color should reflect a degree of fruit's color. Hop rates may be low to medium. Hop characters may be light to moderate in bitterness, flavor and aroma. Fruity-estery aroma and flavor from yeast can be typical but at low levels; however, phenolic, clovelike characteristics should not be perceived. Body should be light to medium in character. Diacetyl should not be perceived. When this style is served with yeast the character should portray a full yeasty mouthfeel and appear hazy to very cloudy. Chill haze is also acceptable. Yeast flavor and aroma should be low to medium but not overpowering the balance and character of malt and hops. Brewer may indicate on the bottle whether the yeast should be intentionally roused or if they prefer that the entry be poured as quietly as possible.",
                    "ibuMin": "10",
                    "ibuMax": "35",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "10",
                    "ogMin": "1.036",
                    "fgMin": "1.004",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:43:55"
                }
            },
            {
                "id": "uUaSmB",
                "name": "Quasar Dank",
                "nameDisplay": "Quasar Dank",
                "description": "Quasar Dank is a medium bodied ruby red ale with bold spicy grain and sweet citrus hop aromas.  Hearty and distinct malted rye dominates the flavor profile, with slight traces of sweetness from other specialty grains.  Heavy hop additions compound with an intense rye dryness, leaving its signature peppery mark on the magnified bitter finish.",
                "abv": "6.5",
                "styleId": 117,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/uUaSmB/upload_lbIIQK-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/uUaSmB/upload_lbIIQK-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/uUaSmB/upload_lbIIQK-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-10-23 04:11:24",
                "updateDate": "2015-12-17 18:19:32",
                "style": {
                    "id": 117,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Rye Ale or Lager with or without Yeast",
                    "shortName": "Rye Ale",
                    "description": "Rye beers can be made using either ale or lager yeast. It should be brewed with at least 20 percent rye malt, and low to medium perception of hop bitterness. Hop aroma and flavor can be low to medium-high. These are often versions of classic styles that contain noticeable rye character in balance with other qualities of the beer. A spicy, fruity-estery aroma and flavor are typical but at low levels; however, phenolic, clove-like characteristics should not be perceived. Color is straw to amber, and the body should be light to medium in character. Diacetyl should not be perceived. If this style is packaged and served without yeast, no yeast characters should be evident in mouthfeel, flavor, or aroma. A low level of tannin derived astringency may be perceived. If this style is served with yeast, the character should portray a full yeasty mouthfeel and appear hazy to very cloudy. Yeast flavor and aroma should be low to medium but not overpowering the balance and character of rye and barley malt and hops. Darker versions of this style will be dark amber to dark brown, and the body should be light to medium in character. Roasted malts are optionally evident in aroma and flavor with a low level of roast malt astringency acceptable when appropriately balanced with malt sweetness. Roast malts may be evident as a cocoa/chocolate or caramel character. Aromatic toffee-like, caramel, or biscuit-like characters may be part of the overall flavor/aroma profile. As in the lighter colored versions, diacetyl should not be perceived. Competition directors may create specific styles of rye beer, such as Rye Pale Ale or Rye Brown Ale. A statement by the brewer indicating if the beer is based on a classic style is essential for accurate judging.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:12"
                }
            },
            {
                "id": "t7QENy",
                "name": "Queen Bee",
                "nameDisplay": "Queen Bee",
                "description": "Queen Bee is an Imperial Amber Ale full of sweet fruity aromas from the skillful blending of malt, hops, and floral northern Michigan Star Thistle honey.  Crystal clear and bright in appearance, this beer is rich and full of flavors of honey and malt, which create a wonderful balance. The balance has similarities of hard candy.  Also, generous hop additions add to a perfectly timed dry and warming finish.",
                "abv": "8",
                "ibu": "30",
                "availableId": 2,
                "styleId": 32,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/t7QENy/upload_3QgeKf-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/t7QENy/upload_3QgeKf-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/t7QENy/upload_3QgeKf-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:35:53",
                "updateDate": "2015-12-17 20:04:55",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 32,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Amber/Red Ale",
                    "shortName": "Amber",
                    "description": "American amber/red ales range from light copper to light brown in color. They are characterized by American-variety hops used to produce the perception of medium hop bitterness, flavor, and medium aroma. Amber ales have medium-high to high maltiness with medium to low caramel character. They should have medium to medium-high body. The style may have low levels of fruityester flavor and aroma. Diacetyl can be either absent or barely perceived at very low levels. Chill haze is allowable at cold temperatures. Slight yeast haze is acceptable for bottle-conditioned products.",
                    "ibuMin": "30",
                    "ibuMax": "40",
                    "abvMin": "4.5",
                    "abvMax": "6",
                    "srmMin": "11",
                    "srmMax": "18",
                    "ogMin": "1.048",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:52"
                }
            },
            {
                "id": "zmJjN5",
                "name": "Richard in the Dirt",
                "nameDisplay": "Richard in the Dirt",
                "description": "Richard in the Dirt is a Belgian Tripel with a bright copper glow and an estery aroma of subtle green apple and a hint of spice. A complex spiciness increases in intensity towards the finish. Malt flavors take a back seat to the sweet sugary additions of Belgian candy sugar. Hop flavors are subtle while spicy yeast qualities provide an offset to sweeter flavors found up front.",
                "abv": "10",
                "ibu": "45",
                "styleId": 59,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/zmJjN5/upload_ZMh37r-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/zmJjN5/upload_ZMh37r-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/zmJjN5/upload_ZMh37r-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-11-17 21:48:49",
                "updateDate": "2016-11-17 21:49:27",
                "style": {
                    "id": 59,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style Tripel",
                    "shortName": "Belgian Tripel",
                    "description": "Tripels are often characterized by a complex, sometimes mild spicy character. Clove-like phenolic flavor and aroma may be evident at extremely low levels. Yeast-generated  fruity esters, including banana, are also common, but not necessary. These pale/light-colored ales may finish sweet, though any sweet finish should be light. The beer is characteristically medium and clean in body with an equalizing hop/malt balance and a perception of medium to medium high hop bitterness. Traditional Belgian Tripels are often well attenuated. Brewing sugar may be used to lighten the perception of body. Its sweetness will come from very pale malts. There should not be character from any roasted or dark malts. Low hop flavor is acceptable. Alcohol strength and flavor should be perceived as evident. Head retention is dense and mousse-like. Chill haze is acceptable at low serving temperatures. Traditional Tripels are bottle conditioned, may exhibit slight yeast haze but the yeast should not be intentionally roused. Oxidative character if evident in aged Tripels should be mild and pleasant.",
                    "ibuMin": "20",
                    "ibuMax": "45",
                    "abvMin": "7",
                    "abvMax": "10",
                    "srmMin": "4",
                    "srmMax": "9",
                    "ogMin": "1.07",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:31:50"
                }
            },
            {
                "id": "15yLhj",
                "name": "Rich’s Rye",
                "nameDisplay": "Rich’s Rye",
                "description": "This super hopped, copper colored, rye beer has distinct flavors coming from highly resinous hops, blended with grain complexities from Munich malt and rye. Lager yeast provides a crisp, clean backdrop for the bitter hop character and remarkably dry nature of the rye to dominate the flavor profile.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 112,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:05",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 112,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Light American Wheat Ale or Lager with Yeast",
                    "shortName": "Wheat Ale",
                    "description": "This beer can be made using either ale or lager yeast. It can be brewed with 30 to 75 percent wheat malt, and hop rates may be low to medium. Hop characters may be light to moderate in bitterness, flavor and aroma. Fruity-estery aroma and flavor are typical but at low levels however, phenolic, clove-like characteristics should not be perceived. Color is usually straw to light amber, and the body should be light to medium in character. Diacetyl should not be perceived. Because this style is served with yeast the character should portray a full yeasty mouthfeel and appear hazy to very cloudy. Chill haze is also acceptable. Yeast flavor and aroma should be low to medium but not overpowering the balance and character of malt and hops. These beers are typically served with the yeast in the bottle, and are cloudy when served.",
                    "ibuMin": "10",
                    "ibuMax": "35",
                    "abvMin": "3.5",
                    "abvMax": "5.5",
                    "srmMin": "4",
                    "srmMax": "10",
                    "ogMin": "1.036",
                    "fgMin": "1.006",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:48"
                }
            },
            {
                "id": "PU3Snw",
                "name": "Rochester Pale Ale",
                "nameDisplay": "Rochester Pale Ale",
                "description": "Rochester Pale Ale is a Pale Ale brewed for Rochester Tap Room in Rochester, MI. Golden in color with a frothy head and ample lacing, this Pale Ale is well balanced with rich malt and citrus hop aromas and flavors. Rochester Pale Ale is a medium-bodied, easy drinking Pale Ale.",
                "abv": "5.5",
                "ibu": "60",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-12-12 18:57:10",
                "updateDate": "2017-12-12 18:57:10",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "wF9EGs",
                "name": "Rye Not?",
                "nameDisplay": "Rye Not?",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/wF9EGs/upload_FjrXwp-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/wF9EGs/upload_FjrXwp-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/wF9EGs/upload_FjrXwp-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-12-04 12:47:17",
                "updateDate": "2015-12-16 22:23:31",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "oGcOdj",
                "name": "S'more Stout",
                "nameDisplay": "S'more Stout",
                "description": "S’more Stout is a complex stout brewed with graham cracker, milk chocolate, marshmallow, and smoked malt. The rich hearty sensations each take their turn, starting with the subtle graham aromas, and leading pleasantly into the sweet flavors of marshmallow cream and milk chocolate. All of these flavors are followed by a slight lingering flavor of smoke. A flaming marshmallow garnish is strongly encouraged.",
                "abv": "8",
                "ibu": "20",
                "availableId": 2,
                "styleId": 44,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/oGcOdj/upload_1EFtEo-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/oGcOdj/upload_1EFtEo-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/oGcOdj/upload_1EFtEo-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-08-27 18:02:58",
                "updateDate": "2018-01-24 20:40:21",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 44,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Specialty Stouts",
                    "shortName": "Stout",
                    "description": "See British Origin American-Style Imperial Porter Imperial porters are very dark brown to black in color. No roast barley or strong burnt/astringent black malt character should be perceived. Medium malt, caramel and cocoa-like sweetness should be in harmony with medium-low to medium hop bitterness. This is a full bodied beer. Ale-like fruity esters should be evident but not overpowering and compliment malt derived sweetness and hop character. Hop flavor and aroma may vary from being low to medium-high. Diacetyl (butterscotch) levels should be absent.",
                    "ibuMin": "35",
                    "ibuMax": "50",
                    "abvMin": "7",
                    "abvMax": "12",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.08",
                    "fgMin": "1.02",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:58"
                }
            },
            {
                "id": "9oV4Vd",
                "name": "Schnozzleberry Griffin",
                "nameDisplay": "Schnozzleberry Griffin",
                "description": "Schnozzleberry Griffin is a magenta colored American Wheat Ale, made with Oolong tea and blackberries. The rich, berry filled aroma overshadows the soft, flour-like contributions of the malted wheat. The crisp, tart, fruity flavors create a pleasantly sour and slightly tangy mouthfeel that leads into a lightly acidic, and semi-dry finish.",
                "abv": "5",
                "ibu": "24",
                "styleId": 35,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/9oV4Vd/upload_PbbVNk-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/9oV4Vd/upload_PbbVNk-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/9oV4Vd/upload_PbbVNk-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-08-27 17:16:54",
                "updateDate": "2015-12-17 16:00:24",
                "style": {
                    "id": 35,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Wheat Wine Ale",
                    "shortName": "Wheatwine",
                    "description": "American style wheat wines range from gold to deep amber and are brewed with 50% or more wheat malt. They have full body and high residual malty sweetness. Perception of bitterness is moderate to medium -high. Fruity-ester characters are often high and counterbalanced by complexity of alcohols and high alcohol content. Hop aroma and flavor are at low to medium levels. Very low levels of diacetyl may be acceptable. Bready, wheat, honey-like and/or caramel aroma and flavor are often part of the character. Phenolic yeast character, sulfur, and/or sweet corn-like dimethylsulfide (DMS) should not be present. Oxidized, stale and aged characters are not typical of this style. Chill haze is allowable.",
                    "ibuMin": "45",
                    "ibuMax": "85",
                    "abvMin": "8.4",
                    "abvMax": "12",
                    "srmMin": "8",
                    "srmMax": "15",
                    "ogMin": "1.088",
                    "fgMin": "1.024",
                    "fgMax": "1.032",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:17"
                }
            },
            {
                "id": "y8wTUQ",
                "name": "SCOREched",
                "nameDisplay": "SCOREched",
                "description": "Oak-smoked lager made exclusively for The Score in Grand Rapids, MI. Lively mouthfeel and a lingering woodiness in the finish.",
                "abv": "7.4",
                "ibu": "20",
                "styleId": 93,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-03-31 15:02:38",
                "updateDate": "2016-03-31 15:02:38",
                "style": {
                    "id": 93,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Lager",
                    "shortName": "American Lager",
                    "description": "Light in body and very light to straw in color, American lagers are very clean and crisp and aggressively carbonated. Flavor components should b e subtle and complex, with no one ingredient dominating the others. Malt sweetness is light to mild. Corn, rice, or other grain or sugar adjuncts are often used. Hop bitterness, flavor and aroma are negligible to very light. Light fruity esters are acceptable. Chill haze and diacetyl should be absent.",
                    "ibuMin": "5",
                    "ibuMax": "13",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.04",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:39:26"
                }
            },
            {
                "id": "yTFKED",
                "name": "Shadow Assasin",
                "nameDisplay": "Shadow Assasin",
                "description": "Shadow Assassin is a medium bodied bronze-colored lager with smokey campfire aromas, indicative of this Rauchbier style. Bold smoke flavors strike the palate creating a surprisingly lively and tingly mouthfeel. The overall impact from start to finish is reminiscent of smoking a cigar, creating an interesting contrast between savory malt flavors and the natural refreshing quality of beer.",
                "abv": "5.3",
                "ibu": "30",
                "styleId": 86,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:26:05",
                "updateDate": "2014-12-17 22:26:05",
                "style": {
                    "id": 86,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Bamberg-Style Helles Rauchbier",
                    "shortName": "Helles Rauchbier",
                    "description": "Helles Rauchbier should have beech wood smoky characters that range from detectable to prevalent in the aroma and flavor. Smoke character is not harshly phenolic, but rather very smooth, almost rendering a perception of mild sweetness to this style of beer. This is a medium-bodied, smoke and malt-emphasized beer, with malt character often balanced with low levels of yeast produced sulfur compounds (character). This beer should be perceived as having low bitterness. Certain renditions of this beer style approach a perceivable level of hop flavor (note: hop flavor does not imply hop bitterness) and character but it is essentially balanced with malt character to retain its style identity. Helles Rauchbier malt character is reminiscent of freshly and very lightly toasted sweet malted barley. There should not be any caramel character. Color is light straw to golden. Noble-type hop flavor is low but may be perceptible. The aroma should strike a balance between malt, hop, and smoke. Fruity esters, diacetyl, and chill haze should not be perceived.",
                    "ibuMin": "18",
                    "ibuMax": "25",
                    "abvMin": "4.5",
                    "abvMax": "5.5",
                    "srmMin": "4",
                    "srmMax": "6",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.012",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:38:36"
                }
            },
            {
                "id": "1ipIdg",
                "name": "Shandy",
                "nameDisplay": "Shandy",
                "description": "Short’s Shandy is a light yellow Wheat Ale that is blended with Northwoods Gourmet Lemonade. Bright lemon and gassy yeast aromas are released by the awesome effervescence of this hybrid brew. The flavors are predominantly sweet sugary lemonade that are slightly tart and dry. The beer seems to aid in the crisp refreshment through its wonderful carbonation, while also contributing a small amount of alcohol that makes drinking a Shandy so enjoyable.",
                "abv": "2.5",
                "styleId": 119,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:46:26",
                "updateDate": "2014-09-12 13:12:05",
                "style": {
                    "id": 119,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Fruit Beer",
                    "shortName": "Fruit Beer",
                    "description": "Fruit beers are any beers using fruit or fruit extracts as an adjunct in either, the mash, kettle, primary or secondary fermentation providing obvious (ranging from subtle to intense), yet harmonious, fruit qualities. Fruit qualities should not be overpowered by hop character. If a fruit (such as juniper berry) has an herbal or spice quality, it is more appropriate to consider it in the herb and spice beers category. Acidic bacterial (not wild yeast) fermentation characters may be evident (but not necessary) they would contribute to acidity and enhance fruity balance. Clear or hazy beer is acceptable in appearance. A statement by the brewer explaining what fruits are used is essential in order for fair assessment in competitions. If this beer is a classic style with fruit, the brewer should also specify the classic style.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:21"
                }
            },
            {
                "id": "yNi8ik",
                "name": "Slum Lord",
                "nameDisplay": "Slum Lord",
                "description": "Slurm Lord is a double New England style India Pale Ale brewed with oats and a blend of Citra, Azacca, Amarillo, Galaxy, and Mosaic hops. We’re excited to say that the brewing of Slurm Lord employs the largest dry hop to date in SBC history! Deep orange in color with a permanent haze and a minimal head, a glass of Slurm Lord resembles a glass of freshly squeezed orange juice. Aromas of dank passion fruit, mango, and pineapple are accompanied by bold flavors of citrus. This medium-bodied India Pale Ale is incredibly juicy from start to finish. A final sip leads into a slight, warming bitterness.",
                "abv": "7.7",
                "ibu": "17",
                "styleId": 125,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/yNi8ik/upload_SJQhYX-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/yNi8ik/upload_SJQhYX-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/yNi8ik/upload_SJQhYX-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-09-08 13:50:09",
                "updateDate": "2017-09-08 13:51:08",
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "P9JOgf",
                "name": "Slurm",
                "nameDisplay": "Slurm",
                "description": "Slurm is a hazy, juicy New England style India Pale Ale. Light orange in color with a prominent haze, Slurm has aromas of pineapple and mango. Big tropical fruit flavors hit you up front and carry through to the finish. This easy drinking, medium-bodied New England style IPA has a slightly bitter, dry finish.",
                "abv": "6.5",
                "ibu": "75",
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/P9JOgf/upload_pSJX3K-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/P9JOgf/upload_pSJX3K-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/P9JOgf/upload_pSJX3K-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-05-15 14:52:57",
                "updateDate": "2017-05-15 14:53:12",
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "teImQy",
                "name": "Small Heath",
                "nameDisplay": "Small Heath",
                "description": "Small Heath is an English Mild Ale inspired by the show “Peaky Blinders.” This ale is copper colored and has a lasting white head. The nose is malt forward with notes of toasted grain and caramel, but balanced with herbal and grassy flavors derived from the use of English Noble hops. The beer is creamy, medium bodied, and finishes smooth.",
                "abv": "3.9",
                "ibu": "22",
                "styleId": 10,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/teImQy/upload_pPraaW-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/teImQy/upload_pPraaW-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/teImQy/upload_pPraaW-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-04-06 14:34:56",
                "updateDate": "2017-04-07 05:00:38",
                "style": {
                    "id": 10,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "English-Style Pale Mild Ale",
                    "shortName": "English Pale Mild",
                    "description": "English pale mild ales range from golden to amber in color. Malt flavor dominates the flavor profile with little hop bitterness or flavor. Hop aroma can be light. Very low diacetyl flavors may be appropriate in this low-alcohol beer. Fruity-ester level is very low. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "10",
                    "ibuMax": "20",
                    "abvMin": "3.2",
                    "abvMax": "4",
                    "srmMin": "8",
                    "srmMax": "17",
                    "ogMin": "1.03",
                    "fgMin": "1.004",
                    "fgMax": "1.008",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:20:35"
                }
            },
            {
                "id": "7NhZ7P",
                "name": "Snake Juice",
                "nameDisplay": "Snake Juice",
                "description": "Snake Juice is a pale orange colored IPA brewed with Sidama and Yirgacheffe coffee beans from Higher Grounds Trading Company.  The aroma is a blend of fruity citrus hops with roasted coffee and subtle chocolate notes.  Sweet flavors of java, berries, and orange lead into a complimentary bitterness that lingers with a pleasant mocha-like finish.",
                "abv": "6.9",
                "ibu": "64",
                "styleId": 125,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/7NhZ7P/upload_WZjtcQ-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/7NhZ7P/upload_WZjtcQ-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/7NhZ7P/upload_WZjtcQ-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-03-21 14:40:21",
                "updateDate": "2016-03-21 14:41:11",
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "amy8Ne",
                "name": "Snow Wheat",
                "nameDisplay": "Snow Wheat",
                "description": "Snow Wheat is a light, clean, easy drinking Hefeweizen. This beer is brewed with a grain bill of 50% malted wheat and a German weizen yeast strain. Immediately evident are the esters and flavors of bananas and cloves. The finish is clean.",
                "abv": "4.5",
                "ibu": "15",
                "styleId": 48,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/amy8Ne/upload_V4lKsa-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/amy8Ne/upload_V4lKsa-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/amy8Ne/upload_V4lKsa-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:28:05",
                "updateDate": "2015-12-17 20:30:51",
                "style": {
                    "id": 48,
                    "categoryId": 4,
                    "category": {
                        "id": 4,
                        "name": "German Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "South German-Style Hefeweizen / Hefeweissbier",
                    "shortName": "Hefeweizen",
                    "description": "The aroma and flavor of a Weissbier with yeast is decidedly fruity and phenolic. The phenolic characteristics are often described as clove-, nutmeg-like, mildly smoke-like or even vanilla-like. Banana-like esters should be present at low to medium-high levels. These beers are made with at least 50 percent malted wheat, and hop rates are quite low. Hop flavor and aroma are absent or present at very low levels. Weissbier is well attenuated and very highly carbonated and a medium to full bodied beer. The color is very pale to pale amber. Because yeast is present, the beer will have yeast flavor and a characteristically fuller mouthfeel and may be appropriately very cloudy. No diacetyl should be perceived.",
                    "ibuMin": "10",
                    "ibuMax": "15",
                    "abvMin": "4.9",
                    "abvMax": "5.5",
                    "srmMin": "3",
                    "srmMax": "9",
                    "ogMin": "1.047",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:29:27"
                }
            },
            {
                "id": "KVSVay",
                "name": "Soft Parade",
                "nameDisplay": "Soft Parade",
                "description": "Soft Parade is a gourmet specialty pour that is brewed with toasted rye flakes and two-row malted barley. Loaded with pureed strawberries, blueberries, raspberries and blackberries, we refer to this fermented potion as a fruit infused rye ale. The result is a refreshing fruit delicacy that is easy to drink, visually appealing, and finishes dry.\r\n \r\nSoft Parade was originally developed because Joe Short wanted to create a beer that incorporated a medley of berries, had a great color, and appealed to wine drinkers. The name Soft Parade stemmed from two moments in Joe Short’s life. First, because Joe worked as a brewer at the Traverse Brewing Company and The Doors album, The Soft Parade, played every day at the end of the brew shift. The second moment occurred when Joe was driving by himself, flipping through CD’s in his car, and came across The Soft Parade album again. At the time, The Soft Parade beer had not been named and it dawned on Joe that the medley of fruit infusing into the golden beer reminded him of a soft parade. That day the beer was named!",
                "abv": "7.5",
                "ibu": "15",
                "glasswareId": 5,
                "availableId": 1,
                "styleId": 119,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/KVSVay/upload_BZeBbt-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/KVSVay/upload_BZeBbt-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/KVSVay/upload_BZeBbt-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2017-01-10 21:39:30",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                },
                "style": {
                    "id": 119,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Fruit Beer",
                    "shortName": "Fruit Beer",
                    "description": "Fruit beers are any beers using fruit or fruit extracts as an adjunct in either, the mash, kettle, primary or secondary fermentation providing obvious (ranging from subtle to intense), yet harmonious, fruit qualities. Fruit qualities should not be overpowered by hop character. If a fruit (such as juniper berry) has an herbal or spice quality, it is more appropriate to consider it in the herb and spice beers category. Acidic bacterial (not wild yeast) fermentation characters may be evident (but not necessary) they would contribute to acidity and enhance fruity balance. Clear or hazy beer is acceptable in appearance. A statement by the brewer explaining what fruits are used is essential in order for fair assessment in competitions. If this beer is a classic style with fruit, the brewer should also specify the classic style.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:21"
                }
            },
            {
                "id": "zwntCF",
                "name": "Soft Parade Shandy",
                "nameDisplay": "Soft Parade Shandy",
                "description": "Soft Parade Shandy is a blend of Short’s flagship Fruit Rye Ale, Soft Parade, and Northwoods Soda & Syrup Co. Gourmet Lemonade. This beer is raspberry in color and smells like fresh berry lemonade. Soft Parade‘s signature berry flavor is enhanced by the addition of citrus. Soft Parade Shandy finishes with the faint taste of fruit candy. It is light in body and very refreshing.”",
                "abv": "4.2",
                "ibu": "12",
                "glasswareId": 5,
                "availableId": 6,
                "styleId": 130,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/zwntCF/upload_ftrQ5f-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/zwntCF/upload_ftrQ5f-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/zwntCF/upload_ftrQ5f-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-20 13:49:31",
                "updateDate": "2017-08-30 19:43:28",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 6,
                    "name": "Summer",
                    "description": "Available during the summer months."
                },
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "KFux13",
                "name": "Space Rock",
                "nameDisplay": "Space Rock",
                "description": "A light bodied American Pale Ale with prominent floral and orange peel-like hop aromas. Only the slightest grainy malt qualities are detectable, as assertive bitter flavors of citrus rind and dandelion leaf take hold.  The finish lingers with a waning bitterness that’s not overly dry.",
                "abv": "5",
                "ibu": "70",
                "availableId": 8,
                "styleId": 25,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/KFux13/upload_Jlkpq7-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/KFux13/upload_Jlkpq7-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/KFux13/upload_Jlkpq7-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:20:38",
                "updateDate": "2016-05-27 20:15:40",
                "available": {
                    "id": 8,
                    "name": "Winter",
                    "description": "Available during the winter months."
                },
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "vB3SCl",
                "name": "Spruce Pilsner",
                "nameDisplay": "Spruce Pilsner",
                "description": "India Spruce Pilsner is an Imperial Pilsner, fermented with local, hand-picked blue spruce tips. The spruce presence, rooted in historical brewing practices, is enormous and gives the beer a refreshing gin quality. This beer is impressively light bodied, considering the immense spruce flavors and the prodigious additions of hops.",
                "abv": "7",
                "ibu": "85",
                "availableId": 2,
                "styleId": 124,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/vB3SCl/upload_J4f1AE-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/vB3SCl/upload_J4f1AE-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/vB3SCl/upload_J4f1AE-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-08-27 17:11:43",
                "updateDate": "2017-01-10 21:43:19",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 124,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Herb and Spice Beer",
                    "shortName": "Spice Beer",
                    "description": "Herb beers use herbs or spices (derived from roots, seeds, fruits, vegetable, flowers, etc.) other than or in addition to hops to create a distinct (ranging from subtle to intense) character, though individual characters of herbs and/or spices used may not always be identifiable. Under hopping often, but not always, allows the spice or herb to contribute to the flavor profile. Positive evaluations are significantly based on perceived balance of flavors. Note: Chili-flavored beers that emphasize heat rather than chili flavor should be entered as a \"spiced\" beer.  A statement by the brewer explaining what herbs or spices are used is essential in order for fair assessment in competitions. Specifying a style upon which the beer is based may help evaluation. If this beer is a classic style with an herb or spice, the brewer should specify the classic style. If no Chocolate or Coffee category exists in a competition, then chocolate and coffee beers should be entered in this category.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:45"
                }
            },
            {
                "id": "JoURj4",
                "name": "Steely Danzig",
                "nameDisplay": "Steely Danzig",
                "description": "A blend of Twist of Cain (Dark American Strong Lager) with Deacon Blues (Belgian Session Pale Ale) forming a Belgian Brown Cream Ale.",
                "abv": "5.5",
                "styleId": 37,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:42:03",
                "updateDate": "2014-09-12 13:10:05",
                "style": {
                    "id": 37,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Brown Ale",
                    "shortName": "American Brown",
                    "description": "American brown ales range from deep copper to brown in color. Roasted malt caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma. American brown ales have evident low to medium hop flavor and aroma, medium to high hop bitterness, and a medium body. Estery and fruity-ester characters should be subdued. Diacetyl should not be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "25",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "6.4",
                    "srmMin": "15",
                    "srmMax": "26",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:35"
                }
            },
            {
                "id": "ZxTd4e",
                "name": "Stellar Ale",
                "nameDisplay": "Stellar Ale",
                "description": "A copper colored strong pale ale brewed exclusively for Trattoria Stella, located in the Village Commons, Traverse City, MI. Sumptuous aromas of sharp citrus blend with toasted caramel malt fragrance. It is a medium bodied, well balanced ale that overflows with snappy grapefruit flavors, a playful spiciness and the perfect bitterness for maximum delight.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:17",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "qyFp3i",
                "name": "Sticky Boots",
                "nameDisplay": "Sticky Boots",
                "description": "Sticky Boots is a triple dry-hopped Double IPA. Copper in color with a substantial white head carrying dank aromas of floral and citrus hops. Leading with strong tropical flavors and a juicy mouthfeel, this beer transitions into highlighting the floral hops. Well balanced and easy to drink, Sticky Boots finishes with huge juicy tropical flavors.",
                "abv": "8",
                "ibu": "75",
                "styleId": 31,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-04-19 21:00:45",
                "updateDate": "2018-04-19 21:00:45",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "eTyc56",
                "name": "Sticky Icky Icky",
                "nameDisplay": "Sticky Icky Icky",
                "description": "Sticky Icky Icky is a radiantly clear, copper-colored American India Pale Ale. It is loaded with citrusy hop aromas of oranges, nectarines, and apricots. This light, yet full bodied ale is perfectly balanced by equal parts sweet malt and juicy hops. A medium bitterness along with a subtle earthiness lingers in the finish.\r\n \r\nThis new American India Pale Ale is what Ryan Hale, the head brewer at the Bellaire pub, referred to as his quintessential IPA. Ryan combined all of his favorite hops resulting in what he described as “pure juicy goodness”. This beer is loaded with all of the big citrus and fruity flavors American hops can offer.",
                "abv": "6.5",
                "ibu": "70",
                "availableId": 2,
                "styleId": 30,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/eTyc56/upload_DmpcIr-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/eTyc56/upload_DmpcIr-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/eTyc56/upload_DmpcIr-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-01-06 17:32:14",
                "updateDate": "2018-06-04 18:46:54",
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 30,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style India Pale Ale",
                    "shortName": "American IPA",
                    "description": "American-style India pale ales are perceived to have medium-high to intense hop bitterness, flavor and aroma with medium-high alcohol content. The style is further characterized by floral, fruity, citrus-like, piney, resinous, or sulfur-like American-variety hop character. Note that one or more of these American-variety hop characters is the perceived end, but the hop characters may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer. This pale gold to deep copper-colored ale has a full, flowery hop aroma and may have a strong hop flavor (in addition to the perception of hop bitterness). India pale ales possess medium maltiness which contributes to a medium body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill and/or hop haze is allowable at cold temperatures. (English and citrus-like American hops are considered enough of a distinction justifying separate American-style IPA and English-style IPA categories or subcategories. Hops of other origins may be used for bitterness or approximating traditional American or English character. See English-style India Pale Ale",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6.3",
                    "abvMax": "7.5",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.06",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:37"
                }
            },
            {
                "id": "4XrCW8",
                "name": "Strawberry Short's Cake",
                "nameDisplay": "Strawberry Short's Cake",
                "abv": "5",
                "styleId": 167,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-05-10 14:14:12",
                "updateDate": "2018-06-06 15:59:10",
                "style": {
                    "id": 167,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-style Fruit Beer",
                    "shortName": "Belgian Fruit",
                    "description": "Belgian-Style Fruit Beers are any range of color from pale to dark depending on underlying Belgian style being fruited. Clear to hazy beer is acceptable in appearance. Fruit aromas ranging from subtle to intense should be evident, and should not be overpowered by hop aromas. Belgian-Style Fruit Beers are fermented with traditional Belgian-, farmhouse-, saison- and/or Brettanomyces-type yeast using fruit or fruit extracts as an adjunct in either the mash, kettle, primary or secondary fermentation providing obvious (ranging from subtle to intense), yet harmonious, fruit qualities. Malt sweetness can vary from not perceived to medium-high levels. Acidic bacterial (not wild yeast) fermentation characters may be evident (but not necessary) and if present contribute to acidity and enhance fruity balance. Body is variable with style. Classifying these beers is complex, with exemplary versions depending on the exhibition of fruit characters more so than the addition of fruit itself, within a Belgian beer style. As an example, a fruited Saison exhibiting some Brett character would be appropriately considered as Belgian Fruit Beer; whereas a fruited Brett Beer might more appropriately be considered as Brett Beer. Lambic-Style fruit beers should be entered in the Belgian-Style Fruit Lambic category. Fruited Belgian style beers brewed with additional unusual fermentables should be entered in this category. Fruit beers fermented using German, British or American ale or lager yeast would be more appropriately categorized as American-Style Fruit Beer or as Fruit Wheat Beer. For purposes of competition coconut is defined as a vegetable; beers exhibiting coconut character would be appropriately entered as Field Beer.",
                    "createDate": "2015-04-07 17:15:52"
                }
            },
            {
                "id": "s38mA2",
                "name": "Strawberry Short’s Cake",
                "nameDisplay": "Strawberry Short’s Cake",
                "description": "Strawberry Short’s Cake is a golden ale. The addition of strawberries and milk sugar transform this beer into a rose colored nectar that has hints of cream and is pleasingly sweet. Biscuit flavors and aromas arise from the great amounts of Victory malt used in the brewing process.",
                "abv": "5",
                "ibu": "2.5",
                "glasswareId": 5,
                "styleId": 36,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/s38mA2/upload_wGnrSW-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/s38mA2/upload_wGnrSW-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/s38mA2/upload_wGnrSW-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:19",
                "updateDate": "2015-12-16 13:21:04",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "style": {
                    "id": 36,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Golden or Blonde Ale",
                    "shortName": "Blonde",
                    "description": "Golden or Blonde ales are straw to golden blonde in color. They have a crisp, dry palate, light to medium body, and light malt sweetness. Low to medium hop aroma may be present but does not dominate. Bitterness is low to medium. Fruity esters may be perceived but do not predominate. Diacetyl should not be perceived. Chill haze should be absent.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "4",
                    "abvMax": "5",
                    "srmMin": "3",
                    "srmMax": "7",
                    "ogMin": "1.045",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:26"
                }
            },
            {
                "id": "n5NgKo",
                "name": "Stroker Ace",
                "nameDisplay": "Stroker Ace",
                "description": "Stroker Ace is a bright, carrot-orange colored ale with fruity hop aromas of citrus zest. This brew is light bodied with low malt sweetness, which allows for an array of noticeable lemony flavors that range from citrus fruit to lemongrass. A proper IPA bitterness resonates in the back of the throat providing a slight lingering bite, and an increasing dryness.",
                "abv": "4",
                "ibu": "80",
                "styleId": 108,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/n5NgKo/upload_hrHiFW-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/n5NgKo/upload_hrHiFW-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/n5NgKo/upload_hrHiFW-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:45:19",
                "updateDate": "2015-12-17 17:46:00",
                "style": {
                    "id": 108,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Session Beer",
                    "shortName": "Session",
                    "description": "Any style of beer can be made lower in strength than described in the classic style guidelines. The goal should be to reach a balance between the style's character and the lower alcohol content. Drinkability is a character in the overall balance of these beers. Beers in this category must not exceed 4.1% alcohol by weight (5.1% alcohol by volume).",
                    "ibuMin": "10",
                    "ibuMax": "30",
                    "abvMin": "4",
                    "abvMax": "5.1",
                    "srmMin": "2",
                    "srmMax": "2",
                    "ogMin": "1.034",
                    "fgMin": "1.004",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:42:20"
                }
            },
            {
                "id": "6mHyRa",
                "name": "Superfluid",
                "nameDisplay": "Superfluid",
                "description": "An American Double IPA with a large but basic malt bill that explodes with a perfume of resiny hop aromas! The lack of sweet specialty grains allows the hops to seize the flavor focus contributing bright tasty qualities of citrus fruit, berries, and even a mild spiciness. The perfect bitterness resonates across palate complimenting the awesome hop intensity within.",
                "abv": "8",
                "ibu": "71",
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/6mHyRa/upload_qgd3js-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/6mHyRa/upload_qgd3js-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/6mHyRa/upload_qgd3js-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-03-13 01:29:20",
                "updateDate": "2018-06-04 19:05:38",
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "FjPy0b",
                "name": "Sustenance Black Beer",
                "nameDisplay": "Sustenance Black Beer",
                "description": "Classically referred to as a Schwartz beer, this dark American lager is brewed in the spring. Soft and sweet on the palate, the roasted and dark caramel grains give the sensation of a satisfying and filling beer, while lager yeast lends refreshing thirst-quenching ability, providing a rich, full flavored, light bodied experience..",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 84,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/FjPy0b/upload_ScD4V3-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/FjPy0b/upload_ScD4V3-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/FjPy0b/upload_ScD4V3-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:20",
                "updateDate": "2015-12-18 16:12:27",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 84,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Schwarzbier",
                    "shortName": "Schwarzbier",
                    "description": "These very dark brown to black beers have a mild roasted malt character without the associated bitterness. This is not a fullbodied beer, but rather a moderate body gently enhances malt flavor and aroma with low to moderate levels of sweetness. Hop bitterness is low to medium in character. Noble-type hop flavor and aroma should be low but perceptible. There should be no fruity esters. Diacetyl should not be perceived.",
                    "ibuMin": "22",
                    "ibuMax": "30",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "25",
                    "srmMax": "30",
                    "ogMin": "1.044",
                    "fgMin": "1.01",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:37:12"
                }
            },
            {
                "id": "L8WQNM",
                "name": "Swirl Stout",
                "nameDisplay": "Swirl Stout",
                "styleId": 20,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/L8WQNM/upload_FXBf9E-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/L8WQNM/upload_FXBf9E-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/L8WQNM/upload_FXBf9E-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-02 21:14:01",
                "updateDate": "2015-12-18 06:19:05",
                "style": {
                    "id": 20,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Sweet or Cream Stout",
                    "shortName": "Sweet Stout",
                    "description": "Sweet stouts, also referred to as cream stouts, have less roasted bitter flavor and a full-bodied mouthfeel. The style can be given more body with milk sugar (lactose) before bottling. Malt sweetness, chocolate, and caramel flavor should dominate the flavor profile and contribute to the aroma. Hops should balance and suppress some of the sweetness without contributing apparent flavor or aroma. The overall impression should be sweet and full-bodied.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "3",
                    "abvMax": "6",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:24:41"
                }
            },
            {
                "id": "mnEE8u",
                "name": "Thai Coconut Oatmeal Stout",
                "nameDisplay": "Thai Coconut Oatmeal Stout",
                "description": "Thai Coconut is an opaque black Oatmeal Stout brewed with toasted coconut and red Thai curry.  A complex array of unusual spice aromatics of coriander, pepper, and cumin prepare the senses for a truly unique tasting experience.  Rich chocolate malt flavors join with tasty coconut to create a pleasant vanilla-like quality.  The combination of sweet and spice form an almost savory sensation that provides a touch of heat leads into a roasty bitter finish.",
                "abv": "7",
                "styleId": 21,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/mnEE8u/upload_n8KVH5-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/mnEE8u/upload_n8KVH5-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/mnEE8u/upload_n8KVH5-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-07-27 20:55:54",
                "updateDate": "2015-12-18 03:54:08",
                "style": {
                    "id": 21,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Oatmeal Stout",
                    "shortName": "Oatmeal Stout",
                    "description": "Oatmeal stouts include oatmeal in their grist, resulting in a pleasant, full flavor and a smooth profile that is rich without being grainy. A roasted malt character which is caramel-like and chocolate-like should be evident - smooth and not bitter. Coffee-like roasted barley and roasted malt aromas (chocolate and nut-like) are prominent. Color is dark brown to black. Bitterness is moderate, not high. Hop flavor and aroma are optional but should not overpower the overall balance if present. This is a medium- to full- bodied beer, with minimal fruity esters. Diacetyl should be absent or at extremely low levels. Original gravity range and alcohol levels are indicative of English tradition of oatmeal stout.",
                    "ibuMin": "20",
                    "ibuMax": "40",
                    "abvMin": "3.8",
                    "abvMax": "6",
                    "srmMin": "20",
                    "srmMax": "20",
                    "ogMin": "1.038",
                    "fgMin": "1.008",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:22:53"
                }
            },
            {
                "id": "hlRA8B",
                "name": "The Barrelman",
                "nameDisplay": "The Barrelman",
                "description": "The Barrelman is a true to style English India Pale Ale with a light brown color from aging in toasted oak barrels for 6 months.  Aromas of sweet tea and wood compliment flavors of honey and green grass from the traditional hops and malts used.  A smokey bitterness in the finish dries and lingers on the palate, increasing in intensity.",
                "abv": "7",
                "ibu": "63",
                "styleId": 2,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/hlRA8B/upload_THy2aq-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/hlRA8B/upload_THy2aq-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/hlRA8B/upload_THy2aq-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-11-13 16:10:19",
                "updateDate": "2015-12-18 06:18:00",
                "style": {
                    "id": 2,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "English-Style India Pale Ale",
                    "shortName": "English IPA",
                    "description": "Most traditional interpretations of English-style India pale ales are characterized by medium-high hop bitterness with a medium to medium-high alcohol content. Hops from a variety of origins may be used to contribute to a high hopping rate. Earthy and herbal English-variety hop character is the perceived end, but may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer, sometimes with subtle and balanced character of sulfur compounds. This pale gold to deep copper-colored ale has a medium to high, flowery hop aroma and may have a medium to strong hop flavor (in addition to the hop bitterness). English-style India pale ales possess medium maltiness and body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill haze is allowable at cold temperatures. Hops of other origins may be used for bitterness or approximating traditional English character.",
                    "ibuMin": "35",
                    "ibuMax": "63",
                    "abvMin": "5",
                    "abvMax": "7",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.05",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:18:33"
                }
            },
            {
                "id": "dPh9rA",
                "name": "The Curl",
                "nameDisplay": "The Curl",
                "description": "One of the first Imperial beers made at Short’s, this American Pilsner has an appealing clear, bright, golden straw color. Faint esters of fresh baked bread and grain aromas precede the flavor resulting from the abundance of flake maize used in this recipe. Hefty doses of hops create a pronounced dryness that seamlessly blends with the crisp, clean finish.",
                "abv": "7",
                "ibu": "50",
                "glasswareId": 4,
                "availableId": 2,
                "styleId": 98,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/dPh9rA/upload_ndJNwC-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/dPh9rA/upload_ndJNwC-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/dPh9rA/upload_ndJNwC-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2015-12-16 13:14:02",
                "glass": {
                    "id": 4,
                    "name": "Pilsner",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 98,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Pilsener",
                    "shortName": "American Pilsener",
                    "description": "This classic and unique pre-Prohibition American-style Pilsener is straw to deep gold in color. Hop bitterness, flavor and aroma are medium to high, and use of noble-type hops for flavor and aroma is preferred. Up to 25 percent corn and/or rice in the grist should be used. Malt flavor and aroma are medium. This is a light-medium to medium-bodied beer. Sweet corn-like dimethylsulfide (DMS), fruity esters and American hop-derived citrus flavors or aromas should not be perceived. Diacetyl is not acceptable. There should be no chill haze. Competition organizers may wish to subcategorize this style into rice and corn subcategories.",
                    "ibuMin": "25",
                    "ibuMax": "40",
                    "abvMin": "5",
                    "abvMax": "6",
                    "srmMin": "3",
                    "srmMax": "6",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:40:08"
                }
            },
            {
                "id": "7C0IZA",
                "name": "The Double Magician",
                "nameDisplay": "The Double Magician",
                "description": "The Double Magician is a Double London-Style Red Ale. Deep mahogany in color with an off white head, The Double Magician has aromas of toffee, light citrus, and just a hint of roast. The Double Magician is medium-bodied with flavors of raisin, toffee, and a slight hop bitterness. The finish is balanced and dry.",
                "abv": "8.1",
                "ibu": "49",
                "styleId": 163,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/7C0IZA/upload_Lj4sty-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/7C0IZA/upload_Lj4sty-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/7C0IZA/upload_Lj4sty-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-11-15 14:45:06",
                "updateDate": "2016-11-15 14:46:14",
                "style": {
                    "id": 163,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Double Red Ale",
                    "shortName": "Double Red",
                    "description": "Double Red Ales are deep amber to dark copper/reddish brown. A small amount of chill haze is allowable at cold temperatures. Fruity-ester aroma is medium. Hop aroma is high, arising from any variety of hops. Medium to medium-high caramel malt character is present. Low to medium biscuit or toasted characters may also be present. Hop flavor is high and balanced with other beer characters. Hop bitterness is high to very high. Alcohol content is medium to high. Complex alcohol flavors may be evident. Fruity-ester flavors are medium. Diacetyl should not be perceived. Body is medium to full.",
                    "createDate": "2015-04-07 17:06:23"
                }
            },
            {
                "id": "xdkovD",
                "name": "The Golden Rule",
                "nameDisplay": "The Golden Rule",
                "description": "An English style India Pale Ale made with 100% organic malt and hops. English ale yeast and a simple grist bill create this light golden colored brew and provide a template for the discernable hop characteristics of earthy, grassy, and even hay-like flavors that are detected before a bitter finale. Brew unto others as you would have brewed unto you.",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 2,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 2,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "English-Style India Pale Ale",
                    "shortName": "English IPA",
                    "description": "Most traditional interpretations of English-style India pale ales are characterized by medium-high hop bitterness with a medium to medium-high alcohol content. Hops from a variety of origins may be used to contribute to a high hopping rate. Earthy and herbal English-variety hop character is the perceived end, but may be a result of the skillful use of hops of other national origins. The use of water with high mineral content results in a crisp, dry beer, sometimes with subtle and balanced character of sulfur compounds. This pale gold to deep copper-colored ale has a medium to high, flowery hop aroma and may have a medium to strong hop flavor (in addition to the hop bitterness). English-style India pale ales possess medium maltiness and body. Fruity-ester flavors and aromas are moderate to very strong. Diacetyl can be absent or may be perceived at very low levels. Chill haze is allowable at cold temperatures. Hops of other origins may be used for bitterness or approximating traditional English character.",
                    "ibuMin": "35",
                    "ibuMax": "63",
                    "abvMin": "5",
                    "abvMax": "7",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.05",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:18:33"
                }
            },
            {
                "id": "LGbeQy",
                "name": "The Landing Summer Ale",
                "nameDisplay": "The Landing Summer Ale",
                "description": "The Landing Summer Ale is a Pale Wheat Ale brewed with 2 row malt grown and malted by Valley View Farms in East Jordan, MI. Bright and light straw-colored, the Landing Summer Ale has a balanced aroma of malt and tropical fruit. Bready malt and citrus flavors make way for a finish that is slightly bitter. This well-balanced Pale Ale has a medium body.",
                "abv": "4",
                "ibu": "55",
                "styleId": 125,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-10 14:10:14",
                "updateDate": "2017-10-10 14:10:14",
                "style": {
                    "id": 125,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Specialty Beer",
                    "shortName": "Specialty",
                    "description": "These beers are brewed using unusual fermentable sugars, grains and starches that contribute to alcohol content other than, or in addition to, malted barley. Nuts generally have some degree of fermentables, thus beers brewed with nuts would appropriately be entered in this category. The distinctive characters of these special ingredients should be evident either in the aroma, flavor or overall balance of the beer, but not necessarily in overpowering quantities. For example, maple syrup or potatoes would be considered unusual. Rice, corn, or wheat are not considered unusual. Special ingredients must be listed when competing. A statement by the brewer explaining the special nature of the beer, ingredient(s) and achieved character is essential in order for fair assessment in competitions. If this beer is a classic style with some specialty ingredient(s), the brewer should also specify the classic style. Guidelines for competing: Spiced beers using unusual fermentables should be entered in the experimental category. Fruit beers using unusual fermentables should be entered in the fruit beer category.",
                    "ibuMax": "100",
                    "abvMin": "2.5",
                    "abvMax": "25",
                    "srmMin": "1",
                    "srmMax": "100",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:53"
                }
            },
            {
                "id": "UMraJT",
                "name": "The Liberator",
                "nameDisplay": "The Liberator",
                "description": "Liberator was made as a 30th birthday gift to Joe Short. As a double IPA, this beast employs a sizable malt bill, but it is the crazy amount of hops added to the boil every 4 minutes, for 120 minutes, that really make this beer special. Fruity, floral, and piney hop flavors penetrate throughout the caramelized malt profile. Liberator is a well-balanced, full bodied brew that has a bitter finish due to the post fermentation addition of lemon and orange zest. The original batch brewed on the Bellaire system had 42lbs of hops in it, compared to Huma, which has around 20lbs of hops per batch. Today’s Elk Rapids batches have up to 138lbs of hops. The name, Liberator, came from Joe’s name for the Short’s employees who carry out our company’s mission statement as “Beer Liberators”, freeing the masses from their misconceptions surrounding beer.",
                "abv": "8",
                "ibu": "116",
                "glasswareId": 5,
                "availableId": 4,
                "styleId": 31,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/UMraJT/upload_wwTOTx-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/UMraJT/upload_wwTOTx-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/UMraJT/upload_wwTOTx-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2015-12-16 13:21:13",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 31,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Imperial or Double India Pale Ale",
                    "shortName": "Imperial IPA",
                    "description": "Imperial or Double India Pale Ales have intense hop bitterness, flavor and aroma. Alcohol content is medium-high to high and notably evident. They range from deep golden to medium copper in color. The style may use any variety of hops. Though the hop character is intense it's balanced with complex alcohol flavors, moderate to high fruity esters and medium to high malt character. Hop character should be fresh and lively and should not be harsh in quality. The use of large amounts of hops may cause a degree of appropriate hop haze. Imperial or Double India Pale Ales have medium-high to full body. Diacetyl should not be perceived. The intention of this style of beer is to exhibit the fresh and bright character of hops. Oxidative character and aged character should not be present.",
                    "ibuMin": "65",
                    "ibuMax": "100",
                    "abvMin": "7.5",
                    "abvMax": "10.5",
                    "srmMin": "5",
                    "srmMax": "13",
                    "ogMin": "1.075",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:26:46"
                }
            },
            {
                "id": "wpBJkP",
                "name": "The Magician",
                "nameDisplay": "The Magician",
                "description": "A lustrous dark red London ale with a rich malt combination that lends complex notes of toasted caramel, raisins, chocolate, and roasted toffee. Very light hop additions let the true malt characters promenade throughout the duration of this pleasurable experience.",
                "abv": "5.3",
                "ibu": "15",
                "glasswareId": 5,
                "availableId": 1,
                "styleId": 97,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/wpBJkP/upload_BzZ847-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/wpBJkP/upload_BzZ847-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/wpBJkP/upload_BzZ847-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2015-12-16 14:00:58",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                },
                "style": {
                    "id": 97,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Premium Lager",
                    "shortName": "American Premium Lager",
                    "description": "This style has low malt (and adjunct) sweetness, is medium bodied, and should contain no or a low percentage (less than 25%) of adjuncts. Color may be light straw to golden. Alcohol content and bitterness may also be greater. Hop aroma and flavor is low or negligible. Light fruity esters are acceptable. Chill haze and diacetyl should be absent. Note: Some beers marketed as \"premium\" (based on price) may not fit this definition.",
                    "ibuMin": "6",
                    "ibuMax": "15",
                    "abvMin": "4.3",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "6",
                    "ogMin": "1.044",
                    "fgMin": "1.01",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:40:04"
                }
            },
            {
                "id": "bHvXPo",
                "name": "The Upside Down",
                "nameDisplay": "The Upside Down",
                "description": "The Upside Down is an Imperial Experimental English Nut Brown Ale brewed with maple syrup and waffles inspired by the show \"Stranger Things.\" Dark brown in color with a think white head and immaculate lacing, The Upside Down smells of maple syrup and nuts. At first sip, the taster is met with a wave of decadent maple syrup flavors that compliment the malty backbone of this brew. A light bitterness leaves ample room for flavors of biscuit, chocolate, and caramel. The beer is medium-bodied with a sweet and clean finish.",
                "abv": "7.4",
                "ibu": "23",
                "styleId": 130,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-12-20 13:53:24",
                "updateDate": "2017-12-21 18:18:06",
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "Z8u7tq",
                "name": "The Village Reserve",
                "nameDisplay": "The Village Reserve",
                "description": "Use of an exclusive yeast strain and a higher fermentation temperature produce Short’s version of a California Common Ale. This unique brew is a beautiful golden color with a body slightly bigger than a lager. The brew is extracted from two-row pale malt, and then aggressively hopped to result in a flavor that is bold, yet light enough to be refreshing.",
                "abv": "4.2",
                "ibu": "40",
                "glasswareId": 5,
                "availableId": 1,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/Z8u7tq/upload_vCAHDd-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/Z8u7tq/upload_vCAHDd-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/Z8u7tq/upload_vCAHDd-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:22",
                "updateDate": "2013-04-21 09:01:31",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 1,
                    "name": "Year Round",
                    "description": "Available year round as a staple beer."
                }
            },
            {
                "id": "L0NLFu",
                "name": "The Wizard",
                "nameDisplay": "The Wizard",
                "description": "The Wizard is Short’s first Barleywine. From its batch number, to its identical boil time of 6 hours and 66 minutes, this deep, dark amber ale holds some epic warming flavor. A monstrous malt bill creates a full bodied beast of a beer with high residual malty sweetness. These colossal flavors are then enhanced by ample additions of raisins during fermentation.",
                "abv": "11",
                "styleId": 34,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2018-05-10 13:46:24",
                "updateDate": "2018-06-06 13:19:30",
                "style": {
                    "id": 34,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Barley Wine Ale",
                    "shortName": "American Barleywine",
                    "description": "American style barley wines range from amber to deep copper-garnet in color and have a full body and high residual malty sweetness. Complexity of alcohols and fruity-ester characters are often high and counterbalanced by assertive bitterness and extraordinary alcohol content. Hop aroma and flavor are at medium to very high levels. American type hops are often used but not necessary for this style. Very low levels of diacetyl may be acceptable. A caramel and/or toffee aroma and flavor are often part of the character. Characters indicating oxidation, such as vinous (sometimes sherry-like) aromas and/or flavors, are not generally acceptable in American-style Barley Wine Ale, however if a low level of age-induced oxidation character harmonizes and enhances the overall experience this can be regarded favorably. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "60",
                    "ibuMax": "100",
                    "abvMin": "8.4",
                    "abvMax": "12",
                    "srmMin": "11",
                    "srmMax": "22",
                    "ogMin": "1.09",
                    "fgMin": "1.024",
                    "fgMax": "1.028",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:08"
                }
            },
            {
                "id": "CHCRE5",
                "name": "Turtle Stout",
                "nameDisplay": "Turtle Stout",
                "description": "The newest in our conceptual stouts series, this sweet fermentation is made with caramel, milk chocolate, and pecans, embodying all of the excellent qualities of the famous candy it was modeled after. These flavors are complimented by a large malt bill, lending similar characteristics of chocolate and rich sweet roasted grains, leading to a slightly nutty finish.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 20,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:25",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 20,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Sweet or Cream Stout",
                    "shortName": "Sweet Stout",
                    "description": "Sweet stouts, also referred to as cream stouts, have less roasted bitter flavor and a full-bodied mouthfeel. The style can be given more body with milk sugar (lactose) before bottling. Malt sweetness, chocolate, and caramel flavor should dominate the flavor profile and contribute to the aroma. Hops should balance and suppress some of the sweetness without contributing apparent flavor or aroma. The overall impression should be sweet and full-bodied.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "3",
                    "abvMax": "6",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.045",
                    "fgMin": "1.012",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:24:41"
                }
            },
            {
                "id": "PP6UUF",
                "name": "Twist of Cain",
                "nameDisplay": "Twist of Cain",
                "abv": "7.5",
                "styleId": 84,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/PP6UUF/upload_3wy4Hr-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/PP6UUF/upload_3wy4Hr-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/PP6UUF/upload_3wy4Hr-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:45:51",
                "updateDate": "2015-12-17 18:01:33",
                "style": {
                    "id": 84,
                    "categoryId": 7,
                    "category": {
                        "id": 7,
                        "name": "European-germanic Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "German-Style Schwarzbier",
                    "shortName": "Schwarzbier",
                    "description": "These very dark brown to black beers have a mild roasted malt character without the associated bitterness. This is not a fullbodied beer, but rather a moderate body gently enhances malt flavor and aroma with low to moderate levels of sweetness. Hop bitterness is low to medium in character. Noble-type hop flavor and aroma should be low but perceptible. There should be no fruity esters. Diacetyl should not be perceived.",
                    "ibuMin": "22",
                    "ibuMax": "30",
                    "abvMin": "3.8",
                    "abvMax": "5",
                    "srmMin": "25",
                    "srmMax": "30",
                    "ogMin": "1.044",
                    "fgMin": "1.01",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:37:12"
                }
            },
            {
                "id": "Bs5ixC",
                "name": "Twisted Cain",
                "nameDisplay": "Twisted Cain",
                "description": "Twisted Cain is an American Black Ale that pours black in color with red hues passing through the edges of the glass, topped with a creamy off-white head. This medium bodied brew leads with a pleasant roasted aroma, before a sweet dark fruitiness, that ends in slightly bitter and dry finish.",
                "abv": "7.75",
                "ibu": "40",
                "srmId": 41,
                "availableId": 4,
                "styleId": 41,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/Bs5ixC/upload_S0Wp0Q-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/Bs5ixC/upload_S0Wp0Q-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/Bs5ixC/upload_S0Wp0Q-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2016-01-22 00:23:11",
                "updateDate": "2016-01-22 00:24:08",
                "srm": {
                    "id": 41,
                    "name": "Over 40",
                    "hex": "000000"
                },
                "available": {
                    "id": 4,
                    "name": "Seasonal",
                    "description": "Available at the same time of year, every year."
                },
                "style": {
                    "id": 41,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Black Ale",
                    "shortName": "Black Ale",
                    "description": "American-style Black Ales are very dark to black and perceived to have medium high to high hop bitterness, flavor and aroma with medium-high alcohol content, balanced with a medium body. Fruity, floral and herbal character from hops of all origins may contribute character. The style is further characterized by a balanced and moderate degree of caramel malt and dark roasted malt flavor and aroma. High astringency and high degree of burnt roast malt character should be absent.",
                    "ibuMin": "50",
                    "ibuMax": "70",
                    "abvMin": "6",
                    "abvMax": "7.5",
                    "srmMin": "35",
                    "srmMax": "35",
                    "ogMin": "1.056",
                    "fgMin": "1.012",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:28:36"
                }
            },
            {
                "id": "oavFNW",
                "name": "Über Goober Oatmeal Stout",
                "nameDisplay": "Über Goober Oatmeal Stout",
                "description": "This highly coveted Oatmeal Stout has the largest grain bill in the entire Short’s beer portfolio. With roughly a handful of peanuts in every glass, rich aromas of chocolate and peanut nuttiness are generated along with a decadent full flavor on the palate. Creamy oatmeal, fused with peanut puree, combines perfectly with the caramel and roasted grain flavors of the malt.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 21,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:33",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 21,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Oatmeal Stout",
                    "shortName": "Oatmeal Stout",
                    "description": "Oatmeal stouts include oatmeal in their grist, resulting in a pleasant, full flavor and a smooth profile that is rich without being grainy. A roasted malt character which is caramel-like and chocolate-like should be evident - smooth and not bitter. Coffee-like roasted barley and roasted malt aromas (chocolate and nut-like) are prominent. Color is dark brown to black. Bitterness is moderate, not high. Hop flavor and aroma are optional but should not overpower the overall balance if present. This is a medium- to full- bodied beer, with minimal fruity esters. Diacetyl should be absent or at extremely low levels. Original gravity range and alcohol levels are indicative of English tradition of oatmeal stout.",
                    "ibuMin": "20",
                    "ibuMax": "40",
                    "abvMin": "3.8",
                    "abvMax": "6",
                    "srmMin": "20",
                    "srmMax": "20",
                    "ogMin": "1.038",
                    "fgMin": "1.008",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:22:53"
                }
            },
            {
                "id": "j0JCcL",
                "name": "Uncle Steve’s Irish Stout",
                "nameDisplay": "Uncle Steve’s Irish Stout",
                "description": "Short’s version of the classic Irish Stout, complete with a dark color, soft head, and smooth full flavor typical of this lighter bodied style. A simple grist formula provides a base of roasted malt flavors while the infusion of nitrogen adds the creamy drinkability everyone loves.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 24,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:26",
                "updateDate": "2017-01-10 21:39:31",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 24,
                    "categoryId": 2,
                    "category": {
                        "id": 2,
                        "name": "Irish Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Foreign (Export)-Style Stout",
                    "shortName": "Export Stout",
                    "description": "As with classic dry stouts, foreign-style stouts have an initial malt sweetness and caramel flavor with a distinctive dry-roasted bitterness in the finish. Coffee-like roasted barley and roasted malt aromas are prominent. Some slight acidity is permissible and a medium- to full-bodied mouthfeel is appropriate. Bitterness may be high but the perception is often compromised by malt sweetness. Hop aroma and flavor should not be perceived. The perception of fruity esters is low. Diacetyl (butterscotch) should be negligible or not perceived. Head retention is excellent.",
                    "ibuMin": "30",
                    "ibuMax": "60",
                    "abvMin": "5.7",
                    "abvMax": "9.3",
                    "srmMin": "40",
                    "srmMax": "40",
                    "ogMin": "1.052",
                    "fgMin": "1.008",
                    "fgMax": "1.02",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:23:58"
                }
            },
            {
                "id": "D6gwX5",
                "name": "Vintage Premium Lager",
                "nameDisplay": "Vintage Premium Lager",
                "description": "Short’s Vintage Premium Lager is the perfect replica of today’s mainstream American Pale Lagers, complete with a wort that’s 50% blended rice syrup and hot water.  A small amount of 2 row malted barley contributes to its bright yellow color and delicate malt flavors.  Crystal clear and boldly effervescent, this beer has yeast aromas that lead into flavors of lightly toasted white bread with a very low residual sweetness.  The super light body aids in the ultimate refreshment providing a thirst quenching finish that’s super crisp and clean.",
                "abv": "4",
                "ibu": "15",
                "styleId": 97,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/D6gwX5/upload_tHWWj9-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/D6gwX5/upload_tHWWj9-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/D6gwX5/upload_tHWWj9-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:34:08",
                "updateDate": "2015-12-17 19:13:54",
                "style": {
                    "id": 97,
                    "categoryId": 8,
                    "category": {
                        "id": 8,
                        "name": "North American Lager",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "American-Style Premium Lager",
                    "shortName": "American Premium Lager",
                    "description": "This style has low malt (and adjunct) sweetness, is medium bodied, and should contain no or a low percentage (less than 25%) of adjuncts. Color may be light straw to golden. Alcohol content and bitterness may also be greater. Hop aroma and flavor is low or negligible. Light fruity esters are acceptable. Chill haze and diacetyl should be absent. Note: Some beers marketed as \"premium\" (based on price) may not fit this definition.",
                    "ibuMin": "6",
                    "ibuMax": "15",
                    "abvMin": "4.3",
                    "abvMax": "5",
                    "srmMin": "2",
                    "srmMax": "6",
                    "ogMin": "1.044",
                    "fgMin": "1.01",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:40:04"
                }
            },
            {
                "id": "oXUt2i",
                "name": "What'cha What'cha What'cha Want",
                "nameDisplay": "What'cha What'cha What'cha Want",
                "description": "What’cha What’cha What’cha Want! was brewed in honor of 7 Monks 4th Anniversary. This American Pale Ale is brewed entirely with Michigan grown and sourced ingredients including malt from Pilot Malt House, Summit hops from New Mission Organics, and Cascade and Chinook hops from Empire Hops Farm.",
                "abv": "6.5",
                "ibu": "90",
                "styleId": 25,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-10-10 14:36:46",
                "updateDate": "2017-10-10 14:36:46",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "va6JPo",
                "name": "Whiskey Sour",
                "nameDisplay": "Whiskey Sour",
                "description": "Whiskey sour is an experimental Belgian ale brewed with key limes and aged in oak bourbon barrels.  Considerable time spent in the barrel turns this beer a deep amber hue and the flavors feature strong notes of oak, limes, whiskey, and finishes with a faint sweetness that doesn't linger on the palate.  The mouthfeel is full, yet slightly dry.",
                "abv": "6.7",
                "ibu": "20",
                "styleId": 70,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/va6JPo/upload_HYJ6TI-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/va6JPo/upload_HYJ6TI-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/va6JPo/upload_HYJ6TI-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-10 17:02:39",
                "updateDate": "2015-12-17 17:51:34",
                "style": {
                    "id": 70,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Other Belgian-Style Ales",
                    "shortName": "Belgian Ale",
                    "description": "Recognizing the uniqueness and traditions of several other styles of Belgian Ales, the beers entered in this category will be assessed on the merits that they do not fit existing style guidelines and information that the brewer provides explaining the history and tradition of the style. Balance of character is a key component when assessing these beers. Barrel or wood-aged entries in competitions may be directed to other categories by competition director. In competitions the brewer must provide the historical or regional tradition of the style, or his interpretation of the style, in order to be assessed properly by the judges.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:33:05"
                }
            },
            {
                "id": "pQPkgW",
                "name": "White Falcon",
                "nameDisplay": "White Falcon",
                "description": "White Falcon is a Belgian White Ale hopped assertively like an American India Pale Ale. Its pale yellow color is lightened further by an appropriate haze from the use of malted white wheat. An intense, fruit-filled nose comprised of notable hop aromas, distinctive yeast esters, and pronounced lemon and orange zest is prominent. Flavors of soft grain and earthy hops mingle with an overall fruitiness, amplified by coriander and other lively spice qualities. An agreeable bitterness heightens in the finish before turning unmistakably dry.",
                "abv": "5.5",
                "ibu": "64",
                "styleId": 65,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/pQPkgW/upload_SV9MEO-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/pQPkgW/upload_SV9MEO-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/pQPkgW/upload_SV9MEO-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-17 22:37:23",
                "updateDate": "2015-12-17 20:25:39",
                "style": {
                    "id": 65,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style White (or Wit) / Belgian-Style Wheat",
                    "shortName": "Witbier",
                    "description": "Belgian white ales are very pale in color and are brewed using unmalted wheat and malted barley and are spiced with coriander and orange peel. Coriander and light orange peel aroma should be perceived as such or as an unidentified spiciness. Phenolic spiciness and yeast flavors may be evident at mild levels. These beers are traditionally bottle conditioned and served cloudy. An unfiltered starch and yeast haze should be part of the appearance. The low to medium body should have some degree of creaminess from wheat starch. The style is further characterized by the use of noble-type hops to achieve low hop bitterness and little to no apparent hop flavor. This beer has no diacetyl and a low to medium fruity-ester level. Mild acidity is appropriate.",
                    "ibuMin": "10",
                    "ibuMax": "17",
                    "abvMin": "4.8",
                    "abvMax": "5.2",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.044",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:30"
                }
            },
            {
                "id": "A6MQ6e",
                "name": "Wishful Sinful",
                "nameDisplay": "Wishful Sinful",
                "description": "Wishful Sinful is an experimental Golden Ale brewed with strawberries, blueberries, blackberries, raspberries, and flaked rye aged in wine barrels for 6 months. A light rosy color and sweet berry aromatics provide the perfect precursor to the bright fruit flavors within.  Boldly tart, yet refreshing and clean, the finish is mildly dry with a lingering tannic aftertaste.",
                "abv": "7.5",
                "styleId": 36,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/A6MQ6e/upload_zZ8EtJ-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/A6MQ6e/upload_zZ8EtJ-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/A6MQ6e/upload_zZ8EtJ-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-11-09 16:00:18",
                "updateDate": "2017-01-03 13:44:36",
                "style": {
                    "id": 36,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "Golden or Blonde Ale",
                    "shortName": "Blonde",
                    "description": "Golden or Blonde ales are straw to golden blonde in color. They have a crisp, dry palate, light to medium body, and light malt sweetness. Low to medium hop aroma may be present but does not dominate. Bitterness is low to medium. Fruity esters may be perceived but do not predominate. Diacetyl should not be perceived. Chill haze should be absent.",
                    "ibuMin": "15",
                    "ibuMax": "25",
                    "abvMin": "4",
                    "abvMax": "5",
                    "srmMin": "3",
                    "srmMax": "7",
                    "ogMin": "1.045",
                    "fgMin": "1.008",
                    "fgMax": "1.016",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:26"
                }
            },
            {
                "id": "5B2pQC",
                "name": "Wit Happens",
                "nameDisplay": "Wit Happens",
                "description": "Wit Happens is a Belgian White beer brewed with coriander, grains of paradise, and orange peel. Delicate aromas of sweet citrus and bready malt rise from this hazy pale yellow ale. Although perceivably low on the nose, the spicy contributions from the Belgian yeast come through instantly upon first taste, offering pleasant clove complements to the additional peppery flavors. Followed by a soft banana-like sweetness and a mildly pleasant tang, the finish is relatively clean marked by a slight spiciness and a touch of bitterness.",
                "abv": "4.9",
                "ibu": "19",
                "styleId": 65,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/5B2pQC/upload_75qggx-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/5B2pQC/upload_75qggx-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/5B2pQC/upload_75qggx-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-09-11 22:40:50",
                "updateDate": "2015-12-17 16:53:24",
                "style": {
                    "id": 65,
                    "categoryId": 5,
                    "category": {
                        "id": 5,
                        "name": "Belgian And French Origin Ales",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Belgian-Style White (or Wit) / Belgian-Style Wheat",
                    "shortName": "Witbier",
                    "description": "Belgian white ales are very pale in color and are brewed using unmalted wheat and malted barley and are spiced with coriander and orange peel. Coriander and light orange peel aroma should be perceived as such or as an unidentified spiciness. Phenolic spiciness and yeast flavors may be evident at mild levels. These beers are traditionally bottle conditioned and served cloudy. An unfiltered starch and yeast haze should be part of the appearance. The low to medium body should have some degree of creaminess from wheat starch. The style is further characterized by the use of noble-type hops to achieve low hop bitterness and little to no apparent hop flavor. This beer has no diacetyl and a low to medium fruity-ester level. Mild acidity is appropriate.",
                    "ibuMin": "10",
                    "ibuMax": "17",
                    "abvMin": "4.8",
                    "abvMax": "5.2",
                    "srmMin": "2",
                    "srmMax": "4",
                    "ogMin": "1.044",
                    "fgMin": "1.006",
                    "fgMax": "1.01",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:32:30"
                }
            },
            {
                "id": "Que3Wx",
                "name": "Wizard",
                "nameDisplay": "Wizard",
                "description": "Short’s first barley wine is a force to be reckoned with! From its batch number, to its identical boil time of 6hrs and 66mins, this deep dark amber ale holds some epic warming flavor. A monstrous malt bill creates a full bodied behemoth with high residual malty sweetness of caramel and toffee. These colossal flavors are then enhanced by ample additions of raisins during fermentation. Every Halloween we release a barrel aged version that takes on favorable vanilla and burnt oak qualities.",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 34,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:32",
                "updateDate": "2012-03-22 13:05:38",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 34,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Barley Wine Ale",
                    "shortName": "American Barleywine",
                    "description": "American style barley wines range from amber to deep copper-garnet in color and have a full body and high residual malty sweetness. Complexity of alcohols and fruity-ester characters are often high and counterbalanced by assertive bitterness and extraordinary alcohol content. Hop aroma and flavor are at medium to very high levels. American type hops are often used but not necessary for this style. Very low levels of diacetyl may be acceptable. A caramel and/or toffee aroma and flavor are often part of the character. Characters indicating oxidation, such as vinous (sometimes sherry-like) aromas and/or flavors, are not generally acceptable in American-style Barley Wine Ale, however if a low level of age-induced oxidation character harmonizes and enhances the overall experience this can be regarded favorably. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "60",
                    "ibuMax": "100",
                    "abvMin": "8.4",
                    "abvMax": "12",
                    "srmMin": "11",
                    "srmMax": "22",
                    "ogMin": "1.09",
                    "fgMin": "1.024",
                    "fgMax": "1.028",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:08"
                }
            },
            {
                "id": "3EMrgO",
                "name": "Woodmaster",
                "nameDisplay": "Woodmaster",
                "description": "Woodmaster is a high gravity, American Brown Ale fermented with Northern Michigan maple syrup and toasted pecans. This dark brown, full bodied ale is rich with sweet malt and syrup flavors. These flavors are complemented by the presence of hop aromas and a faint nuttiness.\r\n \r\nThe very first batch of Woodmaster was brewed in Bellaire on March 20, 2007. It was the 289th batch of beer brewed at Shorts, and is one of the original beers from the Short’s Imperial Beer Series (#7 of 13 to be exact).",
                "abv": "10.5",
                "ibu": "23",
                "glasswareId": 5,
                "availableId": 2,
                "styleId": 37,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/3EMrgO/upload_N09lIg-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/3EMrgO/upload_N09lIg-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/3EMrgO/upload_N09lIg-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2012-01-03 02:44:33",
                "updateDate": "2017-07-20 16:31:09",
                "glass": {
                    "id": 5,
                    "name": "Pint",
                    "createDate": "2012-01-03 02:41:33"
                },
                "available": {
                    "id": 2,
                    "name": "Limited",
                    "description": "Limited availability."
                },
                "style": {
                    "id": 37,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Brown Ale",
                    "shortName": "American Brown",
                    "description": "American brown ales range from deep copper to brown in color. Roasted malt caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma. American brown ales have evident low to medium hop flavor and aroma, medium to high hop bitterness, and a medium body. Estery and fruity-ester characters should be subdued. Diacetyl should not be perceived. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "25",
                    "ibuMax": "45",
                    "abvMin": "4",
                    "abvMax": "6.4",
                    "srmMin": "15",
                    "srmMax": "26",
                    "ogMin": "1.04",
                    "fgMin": "1.01",
                    "fgMax": "1.018",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:27:35"
                }
            },
            {
                "id": "QcF47O",
                "name": "Wowee Zowee",
                "nameDisplay": "Wowee Zowee",
                "description": "A refreshing golden ale infused with fresh mint and mango. An abundance of green mint and fruity aromas pour off of this light bodied ale, opening up the senses with a tingling cool freshness. The first taste offers a minty blast up front followed by some pleasant fruity mango sweetness. The enjoyable balance of both flavors turns slightly tart and bitter as it resonates, but aided by ample carbonation, the overall finish is crisp and clean",
                "abv": "6",
                "ibu": "53",
                "styleId": 119,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/QcF47O/upload_ZkJM7D-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/QcF47O/upload_ZkJM7D-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/QcF47O/upload_ZkJM7D-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2014-12-12 18:53:00",
                "updateDate": "2015-12-17 19:09:48",
                "style": {
                    "id": 119,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Fruit Beer",
                    "shortName": "Fruit Beer",
                    "description": "Fruit beers are any beers using fruit or fruit extracts as an adjunct in either, the mash, kettle, primary or secondary fermentation providing obvious (ranging from subtle to intense), yet harmonious, fruit qualities. Fruit qualities should not be overpowered by hop character. If a fruit (such as juniper berry) has an herbal or spice quality, it is more appropriate to consider it in the herb and spice beers category. Acidic bacterial (not wild yeast) fermentation characters may be evident (but not necessary) they would contribute to acidity and enhance fruity balance. Clear or hazy beer is acceptable in appearance. A statement by the brewer explaining what fruits are used is essential in order for fair assessment in competitions. If this beer is a classic style with fruit, the brewer should also specify the classic style.",
                    "ibuMin": "5",
                    "ibuMax": "70",
                    "abvMin": "2.5",
                    "abvMax": "12",
                    "srmMin": "5",
                    "srmMax": "50",
                    "ogMin": "1.03",
                    "fgMin": "1.006",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:44:21"
                }
            },
            {
                "id": "jYon5I",
                "name": "Woweee Zowee",
                "nameDisplay": "Woweee Zowee",
                "description": "Wowee Zowee is an Experimental American Amber Ale brewed with fresh mint and mango. Slight sweetness with medium to light fruit tones are provided by mango, while fresh mint brings balanced herbal refreshment. The malt and hop character of the amber ale work masterfully in conjunction with the fruit and herbal qualities of mango and mint finishing crisp and clean.",
                "abv": "6",
                "ibu": "36",
                "styleId": 130,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/jYon5I/upload_UTTwtI-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/jYon5I/upload_UTTwtI-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/jYon5I/upload_UTTwtI-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2017-07-12 20:45:41",
                "updateDate": "2017-08-30 19:19:25",
                "style": {
                    "id": 130,
                    "categoryId": 11,
                    "category": {
                        "id": 11,
                        "name": "Hybrid/mixed Beer",
                        "createDate": "2012-03-21 20:06:46"
                    },
                    "name": "Experimental Beer (Lager or Ale)",
                    "shortName": "Experimental Beer",
                    "description": "An experimental beer is any beer (lager, ale or other) that is primarily grain-based and employs unusual techniques and/or ingredients. A minimum 51% of the fermentable carbohydrates must be derived from malted grains. The overall uniqueness of the process, ingredients used and creativity should be considered. Beers such as garden (vegetable), fruit, chocolate, coffee, spice, specialty or other beers that match existing categories should not be entered into this category. Beers not easily matched to existing style categories in a competition would often be entered into this category. Beers that are a combination of other categories (spice, smoke, specialty, porter, etc.) could also be entered into this category. A statement by the brewer explaining the experimental or other nature of the beer is essential in order for fair assessment in competitions. Generally, a 25-word statement would suffice in explaining the experimental nature of the beer.",
                    "createDate": "2012-03-21 20:06:46",
                    "updateDate": "2015-04-07 15:46:02"
                }
            },
            {
                "id": "UYloi9",
                "name": "Yosemite Scooter",
                "nameDisplay": "Yosemite Scooter",
                "styleId": 25,
                "isOrganic": "N",
                "labels": {
                    "icon": "https://s3.amazonaws.com/brewerydbapi/beer/UYloi9/upload_x8piFj-icon.png",
                    "medium": "https://s3.amazonaws.com/brewerydbapi/beer/UYloi9/upload_x8piFj-medium.png",
                    "large": "https://s3.amazonaws.com/brewerydbapi/beer/UYloi9/upload_x8piFj-large.png"
                },
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2013-12-05 12:48:45",
                "updateDate": "2015-12-17 07:37:06",
                "style": {
                    "id": 25,
                    "categoryId": 3,
                    "category": {
                        "id": 3,
                        "name": "North American Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "American-Style Pale Ale",
                    "shortName": "American Pale",
                    "description": "American pale ales range from deep golden to copper in color. The style is characterized by fruity, floral and citrus-like American-variety hop character producing medium to medium-high hop bitterness, flavor, and aroma. Note that the \"traditional\" style of this beer has its origins with certain floral, fruity, citrus-like, piney, resinous, or sulfur-like American hop varietals. One or more of these hop characters is the perceived end, but the perceived hop characters may be a result of the skillful use of hops of other national origins. American pale ales have medium body and low to medium maltiness. Low caramel character is allowable. Fruity-ester flavor and aroma should be moderate to strong. Diacetyl should be absent or present at very low levels. Chill haze is allowable at cold temperatures.",
                    "ibuMin": "30",
                    "ibuMax": "42",
                    "abvMin": "4.5",
                    "abvMax": "5.6",
                    "srmMin": "6",
                    "srmMax": "14",
                    "ogMin": "1.044",
                    "fgMin": "1.008",
                    "fgMax": "1.014",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:25:18"
                }
            },
            {
                "id": "3mtjpx",
                "name": "Zangief's Bear Hug",
                "nameDisplay": "Zangief's Bear Hug",
                "abv": "10.2",
                "glasswareId": 6,
                "styleId": 16,
                "isOrganic": "N",
                "status": "verified",
                "statusDisplay": "Verified",
                "createDate": "2015-12-18 19:03:47",
                "updateDate": "2015-12-18 19:03:47",
                "glass": {
                    "id": 6,
                    "name": "Snifter",
                    "createDate": "2012-01-03 02:41:33"
                },
                "style": {
                    "id": 16,
                    "categoryId": 1,
                    "category": {
                        "id": 1,
                        "name": "British Origin Ales",
                        "createDate": "2012-03-21 20:06:45"
                    },
                    "name": "British-Style Imperial Stout",
                    "shortName": "British Imperial Stout",
                    "description": "Dark copper to very dark brown, British-style imperial stouts typically have high alcohol content. The extremely rich malty flavor (often characterized as toffee-like or caramel-like) and aroma are balanced with medium hopping and high fruity-ester characteristics. Bitterness should be moderate and balanced with sweet malt character. The bitterness may be higher in the darker versions. Roasted malt astringency is very low or absent. Bitterness should not overwhelm the overall character. Hop aroma can be subtle to moderately hop-floral, -citrus or -herbal. Diacetyl (butterscotch) levels should be absent.",
                    "ibuMin": "45",
                    "ibuMax": "65",
                    "abvMin": "7",
                    "abvMax": "12",
                    "srmMin": "20",
                    "srmMax": "35",
                    "ogMin": "1.08",
                    "fgMin": "1.02",
                    "fgMax": "1.03",
                    "createDate": "2012-03-21 20:06:45",
                    "updateDate": "2015-04-07 15:22:41"
                }
            }
        ],
        "status": "success"
    }
    console.log(json["data"].length);

    for (var i = 0; i < json["data"].length; i++) {
        if (json["data"][i]["labels"] == undefined || json["data"][i]["description"] == undefined) {
            continue;
        } else {
            console.log(i)
            $("#results").append("" +
                "<div class='row result'>" +
                "<div class='col-lg-2'>" +
                "<h6>" + json["data"][i].name + "</h6>" +
                "<img src='" + json["data"][i]["labels"]["icon"] + "' alt=''>" +
                "</div>" +
                "<div class='col-lg-10'>" + json["data"][i].description + "</div>" +
                "</div>" +
                "<hr>");
        }
    }

}
function RandomBeerTry() {
    var json = {
        "message": "Request Successful",
        "data": {
            "id": "6lpTNU",
            "name": "Black Yak",
            "nameDisplay": "Black Yak",
            "styleId": 41,
            "isOrganic": "N",
            "labels": {
                "icon": "https://s3.amazonaws.com/brewerydbapi/beer/6lpTNU/upload_vUuX8F-icon.png",
                "medium": "https://s3.amazonaws.com/brewerydbapi/beer/6lpTNU/upload_vUuX8F-medium.png",
                "large": "https://s3.amazonaws.com/brewerydbapi/beer/6lpTNU/upload_vUuX8F-large.png"
            },
            "status": "verified",
            "statusDisplay": "Verified",
            "createDate": "2012-07-11 18:21:40",
            "updateDate": "2015-12-16 16:57:03",
            "style": {
                "id": 41,
                "categoryId": 3,
                "category": {},
                "name": "American-Style Black Ale",
                "shortName": "Black Ale",
                "description": "American-style Black Ales are very dark to black and perceived to have medium high to high hop bitterness, flavor and aroma with medium-high alcohol content, balanced with a medium body. Fruity, floral and herbal character from hops of all origins may contribute character. The style is further characterized by a balanced and moderate degree of caramel malt and dark roasted malt flavor and aroma. High astringency and high degree of burnt roast malt character should be absent.",
                "ibuMin": "50",
                "ibuMax": "70",
                "abvMin": "6",
                "abvMax": "7.5",
                "srmMin": "35",
                "srmMax": "35",
                "ogMin": "1.056",
                "fgMin": "1.012",
                "fgMax": "1.018",
                "createDate": "2012-03-21 20:06:46",
                "updateDate": "2015-04-07 15:28:36"
            },
            "breweries": [
                {
                    "id": "YTASRd",
                    "name": "Three Barrel Brewing Company",
                    "nameShortDisplay": "Three Barrel",
                    "description": "Artisan crafted brews produced from San Luis Valley grown malt, hops, honey and fresh mountain water.\r\n\r\nFlavors may vary, check in to see what's new!",
                    "website": "http://www.threebarrelbrew.com/",
                    "isOrganic": "N",
                    "images": {
                        "icon": "https://s3.amazonaws.com/brewerydbapi/brewery/YTASRd/upload_Zua9MO-icon.png",
                        "medium": "https://s3.amazonaws.com/brewerydbapi/brewery/YTASRd/upload_Zua9MO-medium.png",
                        "large": "https://s3.amazonaws.com/brewerydbapi/brewery/YTASRd/upload_Zua9MO-large.png",
                        "squareMedium": "https://s3.amazonaws.com/brewerydbapi/brewery/YTASRd/upload_Zua9MO-squareMedium.png",
                        "squareLarge": "https://s3.amazonaws.com/brewerydbapi/brewery/YTASRd/upload_Zua9MO-squareLarge.png"
                    },
                    "status": "verified",
                    "statusDisplay": "Verified",
                    "createDate": "2012-01-03 02:42:10",
                    "updateDate": "2015-12-22 15:23:53",
                    "isMassOwned": "N",
                    "brandClassification": "craft",
                    "locations": [
                        {
                            "id": "sNu7xv",
                            "name": "Main Brewery",
                            "streetAddress": "586 Columbia Avenue",
                            "locality": "Del Norte",
                            "region": "Colorado",
                            "postalCode": "81132",
                            "phone": "719-657-0681",
                            "website": "http://www.threebarrelbrew.com/",
                            "hoursOfOperation": "Mon - Fri:\t9:00 am - 5:30 pm",
                            "latitude": 37.678011,
                            "longitude": -106.354316,
                            "isPrimary": "Y",
                            "inPlanning": "N",
                            "isClosed": "N",
                            "openToPublic": "Y",
                            "locationType": "micro",
                            "locationTypeDisplay": "Micro Brewery",
                            "countryIsoCode": "US",
                            "status": "verified",
                            "statusDisplay": "Verified",
                            "createDate": "2012-01-03 02:42:10",
                            "updateDate": "2014-07-23 19:11:34",
                            "country": {
                                "isoCode": "US",
                                "name": "UNITED STATES",
                                "displayName": "United States",
                                "isoThree": "USA",
                                "numberCode": 840,
                                "createDate": "2012-01-03 02:41:33"
                            }
                        }
                    ]
                }
            ]
        },
        "status": "success"
    };

    document.getElementById("beer_name").innerText = json["data"].name;
    document.getElementById("beer_description").innerText = json["data"].description;
    document.getElementById("beer_image").setAttribute("src", json["data"]["labels"].large);
    document.getElementById("brewery_more").value = json["data"]["breweries"][0].id;
}
