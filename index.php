<?php

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EgyDev</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
</head>
<body>

<header>
    <h3>EgyDevs</h3>
</header>

<div class="container">
    <!--    start randm section -->
    <div id="random" class="random row justify-content-center">
        <div class="col-lg-2 info">
            <h6 id="beer_name"></h6>
            <div class="image">
                <img src="" id="beer_image" alt="" width="100%" height="100%">
            </div>
        </div>
        <div class="col-lg-8 description">
            <p id="beer_description"></p>
        </div>
        <div class="col-lg-2 buttons">
            <button id="btn1_random_beer" class="btn btn-primary">Another Beer</button>
            <button id="btn2_more_beers" class="btn btn-primary">More From This <br>Berwery</button>
        </div>
        <input type="hidden" id="brewery_more" value="">
        <input type="hidden" id="random_beer" value="">
    </div>
    <hr>
    <!--    end random section -->

    <!--    start form section-->
    <div id="form" class="search_form">
        <h3>Search</h3>
        <form action="" name="search_form" class="search_form form-inline row">

            <div class="search_input col-lg-2">
                <input type="text" id="search_input" class="form-control">
            </div>

            <div class="col-lg-7 offset-1">
                <div class="search_options col-lg-4">
                    <input type="radio" id="beer" class="form-control" name="search" value="beer">Beer
                    <input type="radio" id="brewery" class="form-control" name="search" value="brewery">Brewery
                </div>
            </div>

            <div class="search_button col-lg-2">
                <input type="button" id="search_button" class="btn btn-primary" value="Search">
            </div>

        </form>
    </div>
    <hr>
    <!--end form section-->

    <!--    start search results-->
    <h3 class="results_head">Search Results: </h3>
    <div id="search_results" class="search_results">
        <div id="results" class="results">

        </div>
    </div>
    <!--    end search results-->
</div>

<script src="js/jquery.js"></script>
<script src="ajax/controller.js"></script>

</body>
</html>
